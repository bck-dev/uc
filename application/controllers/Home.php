<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require 'vendor/autoload.php';

class Home extends CI_Controller {

	public function index()
	{
    $pageName="Home";
    $data = $this->common_model->loadTemplateData($pageName); 

    $locations = $this->location_model->homeLocation();
    foreach ($locations as $l){
      $ls = $this->location_model->locationWithShedule($l->id);
      $l->schedule=$ls;
      foreach($ls as $s){
        if($s->day =="Monday" && $s->fromHour!="00:00:00"){
          $l->mon="on";
        }elseif($s->day =="Tuesday" && $s->fromHour!="00:00:00"){
          $l->tue="on";
        }elseif($s->day =="Wednesday" && $s->fromHour!="00:00:00"){
          $l->wed="on";
        }elseif($s->day =="Thursday" && $s->fromHour!="00:00:00"){
          $l->thu="on";
        }elseif($s->day =="Friday" && $s->fromHour!="00:00:00"){
          $l->fri="on";
        }
      }
    }
    //echo '<pre>' . var_export($locations, true) . '</pre>';

    // $emergency=$this->common_model->getByFeild('pointers', 'section', "Emergency Room");
    // $uc=$this->common_model->getByFeild('pointers', 'section', "Urgent Care");
    // $data['emergency']= $emergency;
    // $data['uc']= $uc;

    //slider data
    $SliderPageData = $this->common_model->getByFeild('pages', 'name', 'Slider');
    foreach ($SliderPageData as $page){
      $pageId=$page->id;
    }
    
    $sliderData = $this->common_model->getByFeild('accordion', 'pageId', $pageId);

    $data['locations']= $locations;
    $comparisonData=$this->comparison_model->getPageComparison('comparison','category','Home Section','ASC');
    $data['comparisonData'] = $comparisonData;
    $data['sliders'] = $sliderData;

		$this->load->view('home',$data);
  }
  
  public function locations($locationName="Null")
	{
    $pageName="Locations";
    $pageData = $this->common_model->getByFeild('pages', 'name', $pageName);
    foreach ($pageData as $page){
      $pageId=$page->id;
    }
    $bannerData = $this->common_model->getByFeild('banner', 'pageId', $pageId);
    $locations = $this->location_model->homeLocation();    
    foreach($locations as $location){
      $id=$location->id;
      $location->locationSchedule = $this->location_model->locationWithShedule($id);
    }
    
    $data=['pageName'=>$pageName, 
          'locations'=> $locations, 
          'banner' => $bannerData,
          'locationName' => $locationName
    ];

		$this->load->view('locations', $data);
  }


  public function doctors()
	{
    $pageName="Doctors";
    $data = $this->common_model->loadTemplateData($pageName);
  
    $doctorData = $this->common_model->getAllData('doctors');
    
    foreach($doctorData as $doctor){
      $id =$doctor->id;
      $doctor->doclocations = $this->doctor_model->doctorLocationWithShedule($id);
    }

    $data['doctorData']=$doctorData ;

    $this->load->view('doctors', $data);

  }

  public function viewDoctor($name)
	{  
    $name = urldecode($name);
    $doctorData = $this->doctor_model->getDoctorByName($name);
    $docLocations = $this->doctor_model->doctorLocationWithShedule($doctorData->id);

    $data=['pageName'=>"Doctors",
    'doctorData'=>$doctorData,
    'docLocations'=>$docLocations
     ];

    // $data['doctorData']=$doctorData ;

    $this->load->view('viewDoctor',$data);

  }

  public function contact()
	{
    $pageName="Contact";
    $data = $this->common_model->loadTemplateData($pageName);

		$this->load->view('contact', $data);
  }

  public function insurance()
	{
    $pageName="Insurance";
    $data = $this->common_model->loadTemplateData($pageName);

		$this->load->view('insurance', $data);
  }

  public function lapi(){
    $locations = $this->location_model->homeLocation(); 
    $lat=$_GET['lat'];
    $lng=$_GET['lng'];
    $i=0;
    $destination="";

    foreach($locations as $location){
      $des=$location->latitude.'%2C'.$location->longitude;
      $destination=$destination.'%7C'.$des;
      $i++;
    }
    
    $ch = curl_init("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".$lat.",".$lng."&destinations=".$destination."&key=AIzaSyBhtediqxxyPQG-jDY17TE_9RZqw0l2qdg");
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    $output = curl_exec($ch);      
    curl_close($ch);

    $distanceFull = json_decode($output, true);

    
    foreach($distanceFull['rows'][0] as $array){
      $x=1;
      foreach($array as $a){
        $distanceFormatted[$x]['id']="#distance".$x;
        $distanceFormatted[$x]['distance']=$a['distance']['text'];
        $x++;
      } 
    }

    if(!$distanceFormatted[1]['distance']){
      
      $n=1;
      foreach($locations as $location){
        $latitudeTo= $location->latitude;
        $longitudeTo = $location->longitude;

        $distance=$this->location_model->haversineGreatCircleDistance($lat, $lng, $latitudeTo, $longitudeTo);
        
        $distanceFormatted[$n]['id']="#distance".$n;
        $distanceFormatted[$n]['distance']=$distance;
        $n++;
      }
      
      $output = json_encode($distanceFormatted);
    }
    else{
      $output = json_encode($distanceFormatted);
    }
    
    echo $output;
    
  }

  public function appointment()
	{
    $pageName="Appointment";
    $data = $this->common_model->loadTemplateData($pageName);
    $data['locations'] = $this->location_model->homeLocation();

    date_default_timezone_set("America/Los_Angeles");

    $hour = date("H");
    $min = date("i");
    $totMins = $hour*60+$min;
    $count = ceil($totMins/7);

    $data['count']=$count;

		$this->load->view('appointment', $data);
  }

  public function book()
	{

    if($_POST['fname']){
      //Load email library
      $this->load->library('email');

      //SMTP & mail configuration
      $config = array(
          'protocol'  => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'bckl.dev@gmail.com',
          'smtp_pass' => 'bcklimited',
          'mailtype'  => 'html',
          'charset'   => 'utf-8'
      );

      $this->email->initialize($config);
      $this->email->set_mailtype("html");
      $this->email->set_newline("\r\n");

      //Email content
      $htmlContent = '<h1>New Appointment Request</h1>';
      $htmlContent .= '<p>This email has sent via Urgent Care online appointment application.</p>';
      $htmlContent .= '<p>Patient Details:</p>';    
      $htmlContent .= '<p>First Name: '.$_POST['fname'].'</p>';
      $htmlContent .= '<p>Last Name: '.$_POST['lname'].'</p>';
      $htmlContent .= '<p>Date of Birth: '.$_POST['dob'].'</p>';
      $htmlContent .= '<p>Reason: '.$_POST['reason'].'</p>';
      $htmlContent .= '<p>Cell Number: '.$_POST['cellNum'].'</p>';
      $htmlContent .= '<p>Email: '.$_POST['email'].'</p>';
      $htmlContent .= '<br/>';
      $htmlContent .= '<p>Appointment Details:</p>';    
      $htmlContent .= '<p>Prefered Method: '.$_POST['location'].'</p>';
      $htmlContent .= '<p>Prefered Date: '.$_POST['date'].'</p>';
      $htmlContent .= '<p>Prefered Time: '.$_POST['time'].'</p>';
      $htmlContent .= '<br/>';
      $htmlContent .= '<p>Additional Comment: '.$_POST['comment'].'</p>';

      $toEmails= ['customerservice@ktdoctor.com',
      'info@ktdoctor.com',
      'shenelle@bcinterbrand.com',
      'anojan.t@bckonnect.com'];

      foreach($toEmails as $to){
        
        $this->email->from('bckl.dev@gmail.com','Appointment Email');
        $this->email->subject('Urgent Care Online Appointment');
        $this->email->message($htmlContent);

        $this->email->to($to);
        $this->email->send();
      }
      $_SESSION['success']="yes";
      redirect('/mailredirect');
    }else{
      redirect('/appointment');
    }

  }

  public function mailredirect()
  {
    $pageName="Appointment";
    $data = $this->common_model->loadTemplateData($pageName);
    if($_SESSION['success']=="yes"){
      $data['status'] = "Success";
      unset($_SESSION['success']);
    }
    
    
    $data['locations'] = $this->location_model->homeLocation();
    //var_dump($data);
		$this->load->view('appointment', $data);
  }

  public function teleapi()
	{
    date_default_timezone_set("America/Los_Angeles");

    $hour = date("H");
    $min = date("i");
    $totMins = $hour*60+$min;
    $count = ceil($totMins/7);
    $output = json_encode($count);

    echo $output;
  }
 
}