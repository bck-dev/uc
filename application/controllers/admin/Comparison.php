<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Comparison extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $comparisonData = $this->common_model->getAllData('comparison');

  $data=['pageName'=>"Comparison",
         'action'  => 'add',
         'comparisonData' => $comparisonData ];

  $this->load->view('dashboard/comparison',$data);
}

public function add()
{
  $data = array
  (
    'section'   => $_POST['section'],
    'category'  => $_POST['category'],
    'urgentCare' => $_POST['urgentCare'],
    'emergencyRoom' => $_POST['emergencyRoom']
  );  

  $this->db->insert('comparison', $data);

  $comparisonData = $this->common_model->getAllData('comparison');

  $data=['pageName'=>"Comparison",
         'action'  => 'add',
         'comparisonData' => $comparisonData ];

  $this->load->view('dashboard/comparison',$data);
}

public function delete($id)
{
  $this->common_model->delete('comparison', $id);

  $comparisonData = $this->common_model->getAllData('comparison');

  $data=['pageName'=>"Comparison",
         'comparisonData' => $comparisonData,
         'action'  => 'add' ];
      
  $this->load->view('dashboard/comparison', $data); 
}

public function loadUpdate($id)
{
  $updateData = $this->common_model->getById('comparison',$id);

  $comparisonData = $this->common_model->getAllData('comparison');
 
  $data=['pageName'=>"Comparison",
         'comparisonData' => $comparisonData,
         'updateData'  => $updateData,
         'action'  => 'update'];

  $this->load->view('dashboard/comparison', $data);
}

public function update($id)
{
  $data = array
  (
    'section'   => $_POST['section'],
    'category'  => $_POST['category'],
    'urgentCare' => $_POST['urgentCare'],
    'emergencyRoom' => $_POST['emergencyRoom']
  ); 

  $this->common_model->update('comparison', $id, $data);
  $comparisonData = $this->common_model->getAllData('comparison');

  $data=['pageName'=>"Comparison",
         'comparisonData' => $comparisonData,
         'action'  => 'add'
        ];

  $this->load->view('dashboard/comparison', $data);
}

public function api()
{
  $id = $_POST['id'];
  $data = $this->common_model->getById('comparison',$id);

  $output = json_encode($data);
  echo $output;
}

} 

?>