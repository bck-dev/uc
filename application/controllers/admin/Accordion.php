<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Accordion extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
  $pages = $this->common_model->getAllData('pages');

  $accordionData = $this->accordion_model->accordionFull();
  
  $data=['pageName'=>"Accordion",
          'accordionData' => $accordionData, 
          'options' => $pages,
          'action'  => 'add',
        ];

  $this->load->view('dashboard/accordion',$data);

}

public function add()
{
    $data = array
    (
        'title' => $_POST['title'],
        'content' =>$_POST['content'],
        'pageId' => $_POST['page']
    );  
  
      $this->db->insert('accordion', $data);

      $pages = $this->common_model->getAllData('pages');

      $accordionData = $this->accordion_model->accordionFull();

      $data=['pageName'=>"Accordian",
      'accordionData' => $accordionData, 
      'options' => $pages,
      'check' => 'success',
      'action'  => 'add'
    ];

    $this->load->view('dashboard/accordion',$data);

}

public function delete($id){

  $this->common_model->delete('accordion', $id);

  $pages = $this->common_model->getAllData('pages');
  $accordionData = $this->accordion_model->accordionFull();
  $data=['pageName'=>"Accordion",
          'accordionData' => $accordionData,
          'options' => $pages,
          'action'  => 'add'
      ];
  $this->load->view('dashboard/accordion', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('accordion',$id);
  $selectedPageData = $this->common_model->getById('pages',$updateData->pageId);
  $pages = $this->common_model->getAllData('pages');

  $accordionData = $this->accordion_model->accordionFull();

  $tableData=['pageName'=>"Accordion",
              'accordionData' => $accordionData ,
              'updateData'  => $updateData,
              'action'  => 'update',
              'options' => $pages,
              'selectedPage' => $selectedPageData
            ];

  $this->load->view('dashboard/accordion', $tableData);



}

public function update($id){

  $data = array
          (
            'title' => $_POST['title'],
            'content' =>$_POST['content'],
            'pageId' => $_POST['page']
          ); 

    $this->common_model->update('accordion', $id, $data);

    $accordionData = $this->accordion_model->accordionFull();
    $pages = $this->common_model->getAllData('pages');
  
    $tableData=['pageName'=>"Accordion",
                'accordionData' => $accordionData ,
                'action'  => 'add',
                'options' => $pages
                ];
  
    $this->load->view('dashboard/accordion', $tableData);
  


}

public function api(){

  $id = $_POST['id'];
  $data = $this->common_model->getById('accordion',$id);
  $pages = $this->accordion_model->getpage($id);

  $data->pages= $pages;
  
  $output = json_encode($data);
  echo $output;
}



} 

?>