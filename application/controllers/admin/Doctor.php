<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Doctor extends BaseController {
  public function __construct()
  {
      parent::__construct();
      // $this->isLoggedIn();   
  }

public function index()
{

    $locations = $this->common_model->getAllData('locations');

    $doctorData = $this->common_model->getAllData('doctors');

    $data=[    'pageName'=>"Doctor",
                'doctorData'=> $doctorData,
                'locations'=> $locations,
                'action'  => 'add'
          ];


  $this->load->view('dashboard/doctor',$data);

}

public function newDoctor()
{
    $locations = $this->common_model->getAllData('locations');

    $data=['pageName'=>"Doctor",
            'action'=>"add",
            'locations'=> $locations
          ];
    $this->load->view('dashboard/sections/doctorForm', $data);
}


public function add()
{
    $config['upload_path']          = './uploads/';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['max_size']             = 5000;
    $config['encrypt_name']         = TRUE;
   

    $this->load->library('upload', $config);
    $this->upload->do_upload('doctorImage');
  
    $upload_data = $this->upload->data();
    $image_name = $upload_data['file_name'];

    $data = array
    (
        'name' => $_POST['name'],
        'qualification' =>$_POST['qualification'],
        'certification' => $_POST['certification'],
        'specialty' =>$_POST['specialty'],
        'medicalSchool' => $_POST['medicalSchool'],
        'hospitalAffiliation' => $_POST['hospitalAffiliation'],
        'residency_fellowship' => $_POST['residency_fellowship'],
        'biography' => $_POST['biography'],
        'image' => $image_name
    ); 
    
    $this->db->insert('doctors', $data);

    $this->db->select_max('id');
    $query = $this->db->get('doctors'); 

    foreach ($query->result() as $row)
    {
        $docId = $row->id;
    }

    foreach($_POST['locations'] as $selectedlocation){
      $locationData = array(
        'locationId' => $selectedlocation,
        'doctorId' => $docId
      );
  
      $this->db->insert('location_has_doctors', $locationData);
    }

      $doctorData = $this->common_model->getAllData('doctors');

      $data=[   'pageName'=>"Doctor",
                'doctorData'=> $doctorData,
                'action'  => 'add',
                'check' => 'success'
            ];

      $this->load->view('dashboard/doctor',$data);

}

public function delete($id){

  $this->common_model->deleteByFeild('location_has_doctors', 'doctorId', $id);
  
  $this->common_model->delete('doctors', $id);

  $doctorData = $this->common_model->getAllData('doctors');
  $data=[   'pageName'=>"Doctor",
            'doctorData' => $doctorData,
            'action'  => 'add'
      ];
   $this->load->view('dashboard/doctor',$data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('doctors',$id);

  $locations = $this->common_model->getAllData('locations');
 
  $selectedLocations = $this->doctor_model->doctorLocation($id);
  $nonSelectedLocations = $this->doctor_model->doctorLocationNotSelected($id);

  $data=[  'pageName'=>"Doctor",
            'updateData'  => $updateData,
            'locations'=> $locations,
            'selectedLocations' => $selectedLocations,
            'nonSelectedLocations' => $nonSelectedLocations,
            'action'  => 'update'
    
         ];

  $this->load->view('dashboard/sections/doctorForm', $data);

}

public function update($id){

  $config['upload_path']          = './uploads/';
  $config['allowed_types']        = 'jpg|png|jpeg';
  $config['max_size']             = 5000;
  $config['encrypt_name']         = TRUE;

  $this->load->library('upload', $config);
  $this->upload->do_upload('doctorImage');
 
  $uploadData = $this->upload->data();
 
  if($uploadData['file_path']!=$uploadData['full_path']){
    
    $data = array
    (
      'name' => $_POST['name'],
      'qualification' =>$_POST['qualification'],
      'certification' => $_POST['certification'],
      'specialty' =>$_POST['specialty'],
      'medicalSchool' => $_POST['medicalSchool'],
      'hospitalAffiliation' => $_POST['hospitalAffiliation'],
      'residency_fellowship' => $_POST['residency_fellowship'],
      'biography' => $_POST['biography'],
      'image' =>$uploadData['file_name']
    ); 

    
  }
  else{
    $data = array
    (
      'name' => $_POST['name'],
      'qualification' =>$_POST['qualification'],
      'certification' => $_POST['certification'],
      'specialty' =>$_POST['specialty'],
      'medicalSchool' => $_POST['medicalSchool'],
      'hospitalAffiliation' => $_POST['hospitalAffiliation'],
      'residency_fellowship' => $_POST['residency_fellowship'],
      'biography' => $_POST['biography'],
    ); 
  }

    $this->common_model->update('doctors', $id, $data);

    $this->common_model->deleteByFeild('location_has_doctors', 'doctorId', $id);

    foreach($_POST['locations'] as $selectedlocation){
      $locationData = array(
        'locationId' => $selectedlocation,
        'doctorId' => $id
      );
  
      $this->db->insert('location_has_doctors', $locationData);
    }

    $doctorData = $this->common_model->getAllData('doctors');
  
    $data=[ 'pageName'=>"Doctor",
            'doctorData'=> $doctorData,
            'action'  => 'add'
          ];
  
    $this->load->view('dashboard/doctor', $data);

}

  public function api(){


    $id = $_POST['id'];
    $doctor = $this->common_model->getById('doctors',$id);
    $locations = $this->doctor_model->doctorLocation($id);

    $doctor->locations= $locations;
    
    $output = json_encode($doctor);
    echo $output;
  }


} 

?>

