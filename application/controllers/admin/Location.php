<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Location extends BaseController
{
    public function __construct()
    {
        parent::__construct();
        $this->isLoggedIn();   
    }

	public function index()
	{
        $locations = $this->common_model->getAllData('locations');
        $data=['pageName'=>"Locations",
                'locations' => $locations
            ];

        $this->load->view('dashboard/location/list', $data);
    }

    public function newLocation()
	{
        $data=['pageName'=>"Locations",
                'action'=>"save"];
        $this->load->view('dashboard/location/new', $data);
    }

    public function save()
	{
        $data = array(
            'name' => $_POST['name'],
            'address' => $_POST['address'],
            'remarks' =>$_POST['remarks'],
            'telephone' => $_POST['telephone'],
            'fax' => $_POST['fax'],
            'longitude' => $_POST['longitude'],
            'latitude' =>$_POST['latitude'],
            'afterHour' => $_POST['afterHour'],
            'saturday' => $_POST['saturdayHighlight'],
            'sunday' =>$_POST['sundayHighlight']
        ); 
        
        $this->db->insert('locations', $data);

        $locationId = $this->db->insert_id();

        $mondayData = array(
            'locationId' => $locationId,
            'day' => $_POST['monday'],
            'status' => $_POST['mondayStatus'],
            'fromHour' =>$_POST['mondayFromHour'],
            'toHour' => $_POST['mondayToHour']
        );
        $tuesdayData = array(
            'locationId' => $locationId,
            'day' => $_POST['tuesday'],
            'status' => $_POST['tuesdayStatus'],
            'fromHour' =>$_POST['tuesdayFromHour'],
            'toHour' => $_POST['tuesdayToHour']
        );
        $wednesdayData = array(
            'locationId' => $locationId,
            'day' => $_POST['wednesday'],
            'status' => $_POST['wednesdayStatus'],
            'fromHour' =>$_POST['wednesdayFromHour'],
            'toHour' => $_POST['wednesdayToHour']
        );
        $thursdayData = array(
            'locationId' => $locationId,
            'day' => $_POST['thursday'],
            'status' => $_POST['thursdayStatus'],
            'fromHour' =>$_POST['thursdayFromHour'],
            'toHour' => $_POST['thursdayToHour']
        );
        $fridayData = array(
            'locationId' => $locationId,
            'day' => $_POST['friday'],
            'status' => $_POST['fridayStatus'],
            'fromHour' =>$_POST['fridayFromHour'],
            'toHour' => $_POST['fridayToHour']
        );
        $saturdayData = array(
            'locationId' => $locationId,
            'day' => $_POST['saturday'],
            'status' => $_POST['saturdayStatus'],
            'fromHour' =>$_POST['saturdayFromHour'],
            'toHour' => $_POST['saturdayToHour']
        );
        $sundayData = array(
            'locationId' => $locationId,
            'day' => $_POST['sunday'],
            'status' => $_POST['sundayStatus'],
            'fromHour' =>$_POST['sundayFromHour'],
            'toHour' => $_POST['sundayToHour']
        );
    
        $this->db->insert('location_schedule', $mondayData);
        $this->db->insert('location_schedule', $tuesdayData);
        $this->db->insert('location_schedule', $wednesdayData);
        $this->db->insert('location_schedule', $thursdayData);
        $this->db->insert('location_schedule', $fridayData);
        $this->db->insert('location_schedule', $saturdayData);
        $this->db->insert('location_schedule', $sundayData);


        $locations = $this->common_model->getAllData('locations');
        $data=['pageName'=>"Locations",
                'locations' => $locations
            ];

        $this->load->view('dashboard/location/list', $data);
    }

    public function delete($id)
	{
        $this->common_model->delete('locations', $id);
        $this->common_model->deleteByFeild('location_schedule', 'locationId', $id);
        $locations = $this->common_model->getAllData('locations');
        $data=['pageName'=>"Locations",
                'locations' => $locations
            ];
        $this->load->view('dashboard/location/list', $data);
    }

    public function view($id)
	{
        $location = $this->common_model->getById('locations', $id);
        $locationSchedule=$this->common_model->getByFeild('location_schedule', 'locationId', $id);
        $data=['pageName'=>"Locations",
                'location' => $location,
                'locationSchedule' => $locationSchedule
            ];
        $this->load->view('dashboard/location/view', $data);
    }

    public function edit($id)
	{
        $location = $this->common_model->getById('locations', $id);
        $locationSchedule=$this->common_model->getByFeild('location_schedule', 'locationId', $id);
        $data=['pageName'=>"Locations",
                'location' => $location,
                'locationSchedule' => $locationSchedule,
                'action'=>"update"
            ];
        $this->load->view('dashboard/location/new', $data);
    }

    public function update()
	{
        $id=$_POST['id'];
        $data = array(
            'name' => $_POST['name'],
            'address' => $_POST['address'],
            'remarks' =>$_POST['remarks'],
            'telephone' => $_POST['telephone'],
            'fax' => $_POST['fax'],
            'longitude' => $_POST['longitude'],
            'latitude' =>$_POST['latitude'],
            'afterHour' => $_POST['afterHour'],
            'saturday' => $_POST['saturdayHighlight'],
            'sunday' =>$_POST['sundayHighlight']
        ); 



        // if($_POST['mondayStatus']==""){
        //     $data ['monday'] = "on"; 
        // }
        // if($_POST['mondayStatus']){
        //     $data ['monday'] = "on"; 
        // }
        
        $this->common_model->update('locations', $id, $data);

        $mondayData = array( 'status' => $_POST['mondayStatus'], 'fromHour' =>$_POST['mondayFromHour'], 'toHour' => $_POST['mondayToHour'] );
        $mondayConditions = array( 'locationId' => $id, 'day' => $_POST['monday'] );
        $tuesdayData = array( 'status' => $_POST['tuesdayStatus'], 'fromHour' =>$_POST['tuesdayFromHour'], 'toHour' => $_POST['tuesdayToHour'] );
        $tuesdayConditions = array( 'locationId' => $id, 'day' => $_POST['tuesday'] );
        $wednesdayData = array( 'status' => $_POST['wednesdayStatus'], 'fromHour' =>$_POST['wednesdayFromHour'], 'toHour' => $_POST['wednesdayToHour'] );
        $wednesdayConditions = array( 'locationId' => $id, 'day' => $_POST['wednesday'] );
        $thursdayData = array( 'status' => $_POST['thursdayStatus'], 'fromHour' =>$_POST['thursdayFromHour'], 'toHour' => $_POST['thursdayToHour'] );
        $thursdayConditions = array( 'locationId' => $id, 'day' => $_POST['thursday'] );
        $fridayData = array( 'status' => $_POST['fridayStatus'], 'fromHour' =>$_POST['fridayFromHour'], 'toHour' => $_POST['fridayToHour'] );
        $fridayConditions = array( 'locationId' => $id, 'day' => $_POST['friday'] );
        $saturdayData = array( 'status' => $_POST['saturdayStatus'], 'fromHour' =>$_POST['saturdayFromHour'], 'toHour' => $_POST['saturdayToHour'] );
        $saturdayConditions = array( 'locationId' => $id, 'day' => $_POST['saturday'] );
        $sundayData = array( 'status' => $_POST['sundayStatus'], 'fromHour' =>$_POST['sundayFromHour'], 'toHour' => $_POST['sundayToHour'] );
        $sundayConditions = array( 'locationId' => $id, 'day' => $_POST['sunday'] );
        
        $this->common_model->updateMultipleCondition('location_schedule', $mondayConditions, $mondayData);
        $this->db->last_query();
        $this->common_model->updateMultipleCondition('location_schedule', $tuesdayConditions, $tuesdayData);
        $this->db->last_query();
        $this->common_model->updateMultipleCondition('location_schedule', $wednesdayConditions, $wednesdayData);
        $this->db->last_query();
        $this->common_model->updateMultipleCondition('location_schedule', $thursdayConditions, $thursdayData);
        $this->db->last_query();
        $this->common_model->updateMultipleCondition('location_schedule', $fridayConditions, $fridayData);
        $this->common_model->updateMultipleCondition('location_schedule', $saturdayConditions, $saturdayData);   
        $this->common_model->updateMultipleCondition('location_schedule', $sundayConditions, $sundayData);
        $this->db->last_query();

        echo "success";
    }
  
}