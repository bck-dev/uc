<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/BaseController.php';

class Pointers extends BaseController {


public function __construct()
{
    parent::__construct();
    $this->isLoggedIn();   
}

public function index()
{
    $pointersData = $this->common_model->getAllData('pointers');

    $data=['pageName'=>"Pointers",
            'action'  => 'add',
            'pointersData' => $pointersData, 
           ];

  $this->load->view('dashboard/pointers',$data);

}

public function add()
{
    $data = array
    (
        'section' => $_POST['section'],
        'point' => $_POST['point']
    );  
  
      $this->db->insert('pointers', $data);

      $pointersData = $this->common_model->getAllData('pointers');

      $pageData=['pageName'=>"Pointers",
                 'action'  => 'add',
                 'pointersData' => $pointersData, 
                ];

      $this->load->view('dashboard/pointers',$pageData);

}

public function delete($id){

  $this->common_model->delete('pointers', $id);

  $pointersData = $this->common_model->getAllData('pointers');

  $data=['pageName'=>"Pointers",
          'pointersData' => $pointersData,
          'action'  => 'add'
      ];
      
  $this->load->view('dashboard/pointers', $data); 

}

public function loadUpdate($id){

  $updateData = $this->common_model->getById('pointers',$id);

  $pointersData = $this->common_model->getAllData('pointers');
 
  $tableData=['pageName'=>"Pointers",
              'pointersData' => $pointersData,
              'updateData'  => $updateData,
              'action'  => 'update'
              ];

  $this->load->view('dashboard/pointers', $tableData);

}

public function update($id){
    
    $data = array
    (
        'section' => $_POST['section'],
        'point' => $_POST['point']
    ); 

  $this->common_model->update('pointers', $id, $data);
  $pointersData = $this->common_model->getAllData('pointers');

  $tableData=['pageName'=>"Pointers",
              'pointersData' => $pointersData,
              'action'  => 'add'
              ];

  $this->load->view('dashboard/pointers', $tableData);

}

} 

?>