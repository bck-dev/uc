<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

require 'vendor/autoload.php';

class Api extends CI_Controller {

	public function index()
	{
        
    }

    public function update()
	{
        $id=$_GET['id'];
        $data = array(
            'name' => $_GET['name'],
            'address' => $_GET['address'],
            'remarks' =>$_GET['remarks'],
            'telephone' => $_GET['telephone'],
            'fax' => $_GET['fax'],
            'longitude' => $_GET['longitude'],
            'latitude' =>$_GET['latitude'],
            'afterHour' => $_GET['afterHour'],
            'saturday' => $_GET['saturdayHighlight'],
            'sunday' =>$_GET['sundayHighlight']
        ); 
        
        $this->common_model->update('locations', $id, $data);

        $mondayData = array( 'status' => $_GET['mondayStatus'], 'fromHour' =>$_GET['mondayFromHour'], 'toHour' => $_GET['mondayToHour'] );
        $mondayConditions = array( 'locationId' => $id, 'day' => $_GET['monday'] );
        $tuesdayData = array( 'status' => $_GET['tuesdayStatus'], 'fromHour' =>$_GET['tuesdayFromHour'], 'toHour' => $_GET['tuesdayToHour'] );
        $tuesdayConditions = array( 'locationId' => $id, 'day' => $_GET['tuesday'] );
        $wednesdayData = array( 'status' => $_GET['wednesdayStatus'], 'fromHour' =>$_GET['wednesdayFromHour'], 'toHour' => $_GET['wednesdayToHour'] );
        $wednesdayConditions = array( 'locationId' => $id, 'day' => $_GET['wednesday'] );
        $thursdayData = array( 'status' => $_GET['thursdayStatus'], 'fromHour' =>$_GET['thursdayFromHour'], 'toHour' => $_GET['thursdayToHour'] );
        $thursdayConditions = array( 'locationId' => $id, 'day' => $_GET['thursday'] );
        $fridayData = array( 'status' => $_GET['fridayStatus'], 'fromHour' =>$_GET['fridayFromHour'], 'toHour' => $_GET['fridayToHour'] );
        $fridayConditions = array( 'locationId' => $id, 'day' => $_GET['friday'] );
        $saturdayData = array( 'status' => $_GET['saturdayStatus'], 'fromHour' =>$_GET['saturdayFromHour'], 'toHour' => $_GET['saturdayToHour'] );
        $saturdayConditions = array( 'locationId' => $id, 'day' => $_GET['saturday'] );
        $sundayData = array( 'status' => $_GET['sundayStatus'], 'fromHour' =>$_GET['sundayFromHour'], 'toHour' => $_GET['sundayToHour'] );
        $sundayConditions = array( 'locationId' => $id, 'day' => $_GET['sunday'] );
        
        $this->common_model->updateMultipleCondition('location_schedule', $mondayConditions, $mondayData);
        $this->common_model->updateMultipleCondition('location_schedule', $tuesdayConditions, $tuesdayData);
        $this->common_model->updateMultipleCondition('location_schedule', $wednesdayConditions, $wednesdayData);
        $this->common_model->updateMultipleCondition('location_schedule', $thursdayConditions, $thursdayData);
        $this->common_model->updateMultipleCondition('location_schedule', $fridayConditions, $fridayData);
        $this->common_model->updateMultipleCondition('location_schedule', $saturdayConditions, $saturdayData);
        $this->common_model->updateMultipleCondition('location_schedule', $sundayConditions, $sundayData);
        
        $output = json_encode('success');
        echo $output;
    }
  
}