<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'home';
$route['home'] = 'home';
$route['doctors'] = 'home/doctors';
$route['viewDoctor/(:any)'] = 'home/viewDoctor/$1';
$route['locations'] = 'home/locations';
$route['locations/(:any)'] = 'home/locations/$1';
$route['insurance'] = 'home/insurance';
$route['contact'] = 'home/contact';
$route['appointment'] = 'home/appointment';
$route['appointment/book'] = 'home/book';
$route['mailredirect'] = 'home/mailredirect';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['login'] = 'admin/login';
$route['admin'] = 'admin/login';
$route['login/check'] = 'admin/login/checkLogin';
$route['logout'] = 'admin/user/logout';

$route['admin/location'] = 'admin/location';
$route['admin/location/new'] = 'admin/location/new';
$route['admin/location/save'] = 'admin/location/save';
$route['admin/location/update'] = 'admin/api/update';
$route['admin/location/delete/(:any)'] = 'admin/location/delete/$1';

$route['admin/accordion/delete/(:any)'] = 'admin/accordion/delete/$1';
$route['admin/accordion/loadupdate/(:any)'] = 'admin/accordion/loadUpdate/$1';
$route['admin/accordion/update/(:any)'] = 'admin/accordion/update/$1';

$route['admin/banner/delete/(:any)'] = 'admin/banner/delete/$1';
$route['admin/banner/loadupdate/(:any)'] = 'admin/banner/loadUpdate/$1';
$route['admin/banner/update/(:any)'] = 'admin/banner/update/$1';

$route['admin/descriptionbox'] = 'admin/descriptionBox';
$route['admin/descriptionbox/delete/(:any)'] = 'admin/descriptionBox/delete/$1';
$route['admin/descriptionbox/loadupdate/(:any)'] = 'admin/descriptionBox/loadUpdate/$1';
$route['admin/descriptionbox/update/(:any)'] = 'admin/descriptionBox/update/$1';

$route['admin/doctor/delete/(:any)'] = 'admin/doctor/delete/$1';
$route['admin/doctor/loadupdate/(:any)'] = 'admin/doctor/loadUpdate/$1';
$route['admin/doctor/update/(:any)'] = 'admin/doctor/update/$1';

$route['admin/pages/delete/(:any)'] = 'admin/pages/delete/$1';
$route['admin/pages/loadupdate/(:any)'] = 'admin/pages/loadUpdate/$1';
$route['admin/pages/update/(:any)'] = 'admin/pages/update/$1';

$route['admin/featurebox'] = 'admin/featureBox';
$route['admin/featurebox/delete/(:any)'] = 'admin/featureBox/delete/$1';
$route['admin/featurebox/loadupdate/(:any)'] = 'admin/featureBox/loadUpdate/$1';
$route['admin/featurebox/update/(:any)'] = 'admin/featureBox/update/$1';

//pointers
$route['admin/pointers'] = 'admin/pointers';
$route['admin/pointers/delete/(:num)'] = 'admin/pointers/delete/$1';
$route['admin/pointers/loadupdate/(:num)'] = 'admin/pointers/loadUpdate/$1';
$route['admin/pointers/update/(:num)'] = 'admin/pointers/update/$1';

//comparison
$route['admin/comparison'] = 'admin/comparison';
$route['admin/comparison/delete/(:num)'] = 'admin/comparison/delete/$1';
$route['admin/comparison/loadupdate/(:num)'] = 'admin/comparison/loadUpdate/$1';
$route['admin/comparison/update/(:num)'] = 'admin/comparison/update/$1';

//api
$route['api/doctor'] = 'admin/doctor/api';
$route['api/banner'] = 'admin/banner/api';
$route['api/accordion'] = 'admin/accordion/api';
$route['api/descriptionbox'] = 'admin/descriptionBox/api';
$route['api/featurebox'] = 'admin/featureBox/api';
$route['api/comparison'] = 'admin/comparison/api';


$route['web/api/location'] = 'home/lapi';
$route['web/api/telehealth'] = 'home/teleapi';