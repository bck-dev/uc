<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/breadcrumb'); ?>

<div class="container mt-5" id="locations">
  <div class="row">
    <div class="col-lg-4">
      <?php $i=1; foreach($locations as $dataRow): ?>
        <div id="<?php echo $i;?>" class="<?php echo str_replace(' ', '',strtolower($dataRow->name)); ?>">
          <div class="card">
            <div class="card-header px-0 pb-0">
              <?php if($dataRow->afterHour=="on"): ?>
                <div class="after_label">After Hour Location</div>
              <?php endif; ?>
              <div class="d-flex p-0">
                <div class="flex-fill white purple_background font-s2 b600 location_num mr-2"><?php echo $i; ?></div>
                <div class="flex-fill w-100 font20 light_blue b700 p-0 mb-3">
                  <?php $address = explode(",", $dataRow->address); echo $address[0]; ?><br />
                  <span class="dark_grey b300 font16">
                    <?php $len = strlen($address[0])+2; echo substr($dataRow->address, $len); ?>
                  </span>
                </div>
                <div id="distance<?php echo $i;?>" class='flex-fill text-right align-self-right font12 grey pt-2' style="width: auto; min-width: 50px;"></div>
              </div>
              <p class="font-s2 dark_grey b600 pl-4 ml-2">
                Tel. <a class="font-s1 dark_grey" href="tel:8187068133">(818)-706-8133</a><br />
                Text (E). <a class="font-s1 dark_grey" href="sms:6262987121">(626) 298-7121</a><br />
                Text (S). <a class="font-s1 dark_grey" href="sms:6262697744">(626) 269-7744</a><br />
                <br />
                <a class="light_blue font-s1 b700" href="https://www.ktdoctor.com/doctors">Find a Doctor</a>
              </p>
              
            </div>
            <div class="card-body pt-2" >
              <div class="pl-2 ml-1" style="min-width: 180px; float:left;">
                <?php foreach($dataRow->locationSchedule as $ls): ?>
                  <?php if($ls->status =="Open"): ?>
                    <div class="font-s2 mt-1">
                      <div class="font-s2 b600" style="width: 50px !important; float: left;"><?php echo substr($ls->day, 0, 3); ?> </div>
                      <div class="font-s2 b600">
                        <?php echo date("g:i a", strtotime($ls->fromHour))."-".date("g:i a", strtotime($ls->toHour)); ?>
                      </div>                  
                    </div>
                  <?php endif; ?>
                <?php endforeach; ?>
              </div>
              <div class="text-right">
                <a class=" font-s1 green b600" href="https://maps.google.com/?q=<?php echo $dataRow->latitude; ?>,<?php echo $dataRow->longitude; ?>" target="_blank">Get Direction</a>
              </div>
            </div>
          </div>
        </div>
      <?php $i++; endforeach; ?>
    </div>

    <div class="col-lg-8">
      <div class="sticky" id='map_canvas' style="width: 100%; height: 600px;"></div>
    </div>
  </div>
</div>

  <script>
  $(document).ready(function(){
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(showPosition);
    } else { 
      x.innerHTML = "Geolocation is not supported by this browser.";
    }
  
    // Handler for .ready() called.  
    $('html, body').animate({
      
        scrollTop: $('.<?php echo $locationName; ?>').offset().top- 200
    }, '1000');
  
  });

  function showPosition(position) {
    console.log(position.coords);
    $.ajax({
      url: '<?php echo base_url('web/api/location') ?>', 
      type:'get',
      data: {lat: position.coords.latitude, lng: position.coords.longitude},
      dataType: 'json',
      success: function(results){ 
          console.log(results);
          jQuery.each(results, function( key, val ) {
              $(val['id']).text(val['distance']);
          });
          
      },
  
      error:function(){
          console.log('error');
      }
    });
  }




  jQuery(function($) {
    // Asynchronously Load the map API 
    var script = document.createElement('script');
    script.src = "https://maps.googleapis.com/maps/api/js?v=3.38&callback=initialize&key=AIzaSyB_xKPDja6o9znHZAmvv-GpjFVFhlLwV_Y";
    document.body.appendChild(script);
  });

  function attachClickHandler(marker){
    google.maps.event.addListener(marker, 'click', function() {
      console.log(marker.url);
      var elem = $(marker.url);
      $('html, body').animate({
        scrollTop: elem.offset().top - 200
      }, 1000 );        
    });
  }

  function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
        mapTypeId: 'roadmap'
    };

    map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

    var markers = new Array();
    <?php $i= 1; foreach($locations as $l):  ?>
      var marker = ['<?php echo $i; ?>', <?php echo $l->latitude; ?>, <?php echo $l->longitude; ?>, '#<?php echo $i; ?>'];
      markers.push(marker);
    <?php $i++; endforeach; ?>

    var bounds = new google.maps.LatLngBounds();
    var infowindow = new google.maps.InfoWindow();  
     
    for( i = 0; i < markers.length; i++ ) {
      var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
      
      marker = new google.maps.Marker({
          position: position,
          map: map,
          title: markers[i][0],
          url: markers[i][3],
          label: {color: '#fff', fontSize: '16px', fontWeight: '600', text: markers[i][0]}
      });
      bounds.extend(position);
      
      attachClickHandler(marker);   

        
    }
    map.fitBounds(bounds);
    // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
    var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
        this.setZoom(10);
        google.maps.event.removeListener(boundsListener);
    });

  }



</script>

<?php $this->load->view('components/common/footer'); ?>