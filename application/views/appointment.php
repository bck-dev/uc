<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>

<div class="container pt-5 appointment-container">
    <div class="text-center font26 b400">
        <span class="green_background white px-4 py-1 mr-2 br-5 font26 b600">OPEN NOW</span> Urgent Care Needs
    </div>

    <div class="row mt-5">
        <div class="col-lg-4 text-center p-0 px-2 my-3">
            <div class="p-0 pb-3 option-box orange-border">
                <p class="app-num orange-num">1</p>
                <p class="font24 b600 mt-1 mb-0">Request via Text</p>                
                <p>for In-Office Appointment</p>
                <div class="option-body">
                    <div>
                        <a href="sms:6262987121">
                            <div class="appointment-red-button text-center">
                                English<span class="purple-red-num b600 font18"> (626) 298-7121</span>
                            </div>
                        </a>
                        <a href="sms:6262697744">
                            <div class="appointment-red-button text-center">
                                Spanish<span class="purple-red-num b600 font18"> (626) 269-7744</span>
                            </div>
                        </a>
                        <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F141266270391%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink" target="blank">
                            <div class="appointment-messenger-button text-center">
                                <i class="fab fa-facebook-messenger"></i> Send to Messenger
                            </div>
                        </a>
                    </div>
                </div>
                <p class="font20 m-0 mt-3">Response time : <span class="font20 b600 fast">Fastest</span> </p>
                <p class="font15 m-0">(Usually within 10 min during office hours)</p>
            </div>
        </div>
        
        <div class="col-lg-4 text-center telehealth-button-section p-0 px-2 my-3">
            <div class="p-0 pb-3 option-box purple-border">
                <p class="app-num purple-num">2</p>
                <p class="font24 b600 mt-1 mb-0">Request Telemedicine</p>
                <p>May Include Video</p>
                <div class="option-body">
                    <div>
                        <a href="sms:6262987121">
                            <div class="appointment-red-button text-center">
                                English<span class="purple-red-num b600 font18"> (626) 298-7121</span>
                            </div>
                        </a>
                        <a href="sms:6262697744">
                            <div class="appointment-red-button text-center">
                                Spanish<span class="purple-red-num b600 font18"> (626) 269-7744</span>
                            </div>
                        </a>
                        <a href="https://www.messenger.com/login.php?next=https%3A%2F%2Fwww.messenger.com%2Ft%2F141266270391%2F%3Fmessaging_source%3Dsource%253Apages%253Amessage_shortlink" target="blank">
                            <div class="appointment-messenger-button text-center">
                                <i class="fab fa-facebook-messenger"></i> Send to Messenger
                            </div>
                        </a>
                    </div>
                </div>
                <p class="font20 m-0 mt-3"><span class="font20 b600 fast">From Home</span> </p>
                <p class="font15 m-0">(Usually within 30 min during office hours)</p>
            </div>
        </div>

        <div class="col-lg-4 text-center p-0 px-2 my-3">
            <div class="p-0 pb-3 option-box yellow-border">
                <p class="app-num yellow-num">3</p>
                <p class="font24 b600 mt-1 mb-3">Request via Online Form</p>
                <br />
                <div class="option-body">
                    <div>
                        <a href="#" id="scrollToForm">
                            <div class="appointment-red-button text-center">
                                <span class="purple-red-num b600 font18">Online Requests</span>
                            </div>
                        </a>
                    </div>
                </div>
                <p class="font20 m-0 mt-3">Response time : <span class="font20 b600 slow">Slow</span> </p>
                <p class="font15 m-0">(Usually within 30 min during office hours)</p>
            </div>
        </div>
    </div>
    
    <div class="row mt-3">
        <div class="col-lg-4 text-center p-0 px-2 my-3">
            <div class="p-0 pb-3 option-box light-blue-border">
                <p class="app-num light-blue-num">4</p>
                <p class="font24 b600 mt-1 mb-3">Request via Call</p>
                <div class="option-body">
                    <div>
                        <a href="tel:8183615437">
                            <div class="appointment-red-button text-center">
                                <span class="purple-red-num b600 font18">(818) 361-5437</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-lg-8 my-3 p-0 px-2">
            <div class="grey_background p-5">
                <p class="font26 b600">All PPO & Most HMO Insurance Accepted</p>
                <p class="font20">We accept most insurance including HMOs, PPOs, and Medi-Cal. If you are a new patient and would like to verify if we accept your insurance, please email our new patient liaison: <a href="mailto:newpatient@ktdoctor.com">newpatient@ktdoctor.com</a>.</p>
                <p class="font25 grey">
                    If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians. 
                    <br /><br />
                    Or contact us on <a href="tel:8183615437">(818) 361-5437</a> and have it changed over the phone.
                </p>
            </div>
        </div>
    </div> 
    <hr class="mt-4"/>
    <div class="row pt-4 pb-1">
        <div class="col-lg-8">
            <p class="font26 b600 appoint-hading">Request an Appointment</p>
            <p class="font20">
                Our pediatricians are dedicated to providing your family with the highest degree of healthcare. We offer you four quick and easy ways to get an appointment to care for you and your loved ones. 
            </p>

            <div class="d-block d-md-none w-100" id="seeMoreBox">
                <hr class="bb-grey mt-5" />
                <div class="p-1 text-center white_background" id="seeMore">See More</div>
            </div>
            <p class="font15 d-none d-md-block" id="moreBox">
                Our team members have the expertise and skills to provide the best care possible for your child. However, if you're experiencing a medical emergency, we recommend calling 911 for emergency aid.
            </p>
        </div>
        <div class="col-lg-4 d-none d-lg-block">
            <img src="<?php echo base_url();?>assets/images/stayhome-new.png" class="w-100"/>         
        </div>
    </div>
    <script>
        $("#seeMore").click(function() {
            $('#seeMoreBox').removeClass('d-block');
            $('#seeMoreBox').addClass('d-none');
            $('#moreBox').removeClass('d-none');
            $('#moreBox').addClass('d-block');
        });
    </script>
    <?php $this->load->view('components/sections/telehealthCount'); ?>
</div>

<div class="yellow_background">
    <div class="container py-5 text_box_container">
        <div class="row">
            <div class="col-lg-12 font30 text-center mb-3 p-0 b500">
                Fastest Ways To Get A Response From Your Doctor
            </div>
            <div class="col-lg-4 text_box">
                <p class="font30 b700 white">
                    01 <span class="font20 b600 black">Patient Portal</span>
                </p>
                <p class="font15">
                    <a class="black b600" href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank">Registering to your patient portal</a> is the fastest way to contact your doctor, store your child’s medical records, refill medication, and make appointments. 
                </p>
            </div>
            <div class="col-lg-4 text_box">
                <p class="font30 b700 white">
                    02 <span class="font20 b600 black">Text Us (24/7)</span>
                </p>
                <p class="font15">
                    Send us a text on <a class="b600" href="sms:6262987121">626-298-7121</a> <span class="black b600">English</span>, <br/><a class="b600" href="sms:6262697744">626-269-7744</a>  <span class="black b600">Spanish</span> <!--(fastest response time, 15 min during office hours) -->
                    to arrange an <br/>appointment instantly, 24/7.
                </p>
            </div>
            <div class="col-lg-4 text_box">
                <p class="font30 b700 white">
                    03 <span class="font20 b600 black">Call Us</span>
                </p>
                <p class="font15">
                    <!-- Call us at <a href="tel:6267958811">626-795-8811</a> slow response time -->
                    Reach us on <a class="b600" href="tel:6267958811">626-795-8811</a>
                    You can use this hotline to easily make an appointment. However, it may take us a few minutes due to the amount of inquiries.
                </p>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('components/sections/appointmentForm'); ?>
<?php $this->load->view('components/common/footer'); ?>