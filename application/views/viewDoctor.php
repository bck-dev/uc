<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>



			<!-- DOCTOR BREADCRUMBS -->
			<section id="doctor-breadcrumbs" class="bg-fixed doctor-details-section division">	
				<div class="container">
					<div class="row">
						<div class="col-md-7 offset-md-5">
			 				<div class="doctor-data white-color">
                             
			 					<!-- Name -->	
			 					<h2 class="h2-xs"><?php echo $doctorData->name; ?></h2>
			 					<h5 class="h5-md"><?php echo $doctorData->specialty; ?></h5>

							</div>
						</div>
					</div>   <!-- End row -->	
				</div>	  <!-- End container -->
			</section>	<!-- END DOCTOR BREADCRUMBS -->




			<!-- DOCTOR-1 DETAILS -->
			<section id="doctor-1-details" class="doctor-details-section division">	
				<div class="container">
					<div class="row">


						<!-- DOCTOR PHOTO -->
						<div class="col-md-5">
			 				<div class="doctor-photo mb-40">

			 					<!-- Photo -->	
			 					<img class="img-fluid" src="<?php echo base_url(); ?>/uploads/<?php echo $doctorData->image; ?>" alt="doctor-foto">

			 					<!-- Doctor Info -->
			 					<div class="doctor-info">
									<table class="table table-striped">
										<tbody>
										    <tr>
										      	<td>Qualificaition</td>
										      	<td><?php echo $doctorData->qualification; ?></td>
											</tr>   
											<tr>
										      	<td>Certification</td>
										      	<td><?php echo $doctorData->certification; ?></td>
										    </tr>   
                                            <tr>
										      	<td>Medical School</td>
										      	<td><?php echo $doctorData->medicalSchool; ?></td>
                                            </tr>
                                            <tr>
										      	<td>Hospital Affiliation</td>
										      	<td><?php echo $doctorData->hospitalAffiliation; ?></td>
											</tr>
											<tr>
												  <td colspan="2">Doctor Locations</td>
												  
											</tr>
											<tr>
												<td colspan="2">
													<?php foreach($docLocations as $location): ?>                    
														<a href="<?php echo base_url('locations'); ?>">
															<p class="font20 b700 light_blue m-0">
																<?php $address = explode(",", $location->address); echo $address[0]; ?> 
															</p>
														</a>
														<span class="dark_grey b300 font16">
															<?php $len = strlen($address[0])+2; echo substr($location->address, $len); ?>
														</span>
														<div class="d-flex">
															<div class="w-50">
															<p class="font-s2">
																<span class="b600 green font-s2">
																<?php 
																	if($location->status == 'Close'): 
																	echo "(Closed)"; 
																	else: ?>
																	<b class="font-s2"><?php echo date("g:i a", strtotime($location->fromHour))."-".date("g:i a", strtotime($location->toHour)); ?></b>
																	<?php
																	ini_set('date.timezone', 'America/Los_Angeles');
																	$time = strtotime(date('g:i'));
																	$toTime = strtotime($location->toHour);
																	$fromTime = strtotime($location->fromHour);

																	if($time<$toTime AND $time>$fromTime): 
																		echo "(Open)";
																	else:
																		echo "(Closed)";
																	endif;
																	endif; 
																?>
																</span>
															</p>
															</div>
															<div class="w-50 text-right">
															<a class="font-s2 green" href="https://maps.google.com/?q=<?php echo $location->latitude; ?>,<?php echo $location->longitude; ?>" target="_blank">Get Direction</a>
															</div>
														</div>
														
																		
													<?php endforeach; ?>
												</td>
											</tr>   
												
										</tbody>
									</table>
								</div>	<!-- End Doctor Info -->

								<!-- Doctor Contacts -->	
			 					<div class="text-center">
								 	<a href="https://www.ktdoctor.com/appointment" class="btn btn-block btn-blue blue-hover m-0 py-3 font-b3">Make an Appointment</a>
        					
									<div class="d-flex m-0 mt-4">
										<div class="w-50">
											<span class="p-sm mt-15 steel-blue">ENGLISH TEXT</span>
											<a href="sms:6262987121" >
												<h5 class="h5-xl blue-color">(626) 298-7121</h5>
											</a>
										</div>
										<div class="w-50">
											<span class="p-sm mt-15 steel-blue">SPANISH TEXT</span>
											<a href="sms:6262697744" >
												<h5 class="h5-xl blue-color">(626) 269-7744</h5>
											</a>
										</div>

									</div> 
									
								</div>

								<!-- Buttons -->


			 				</div>
			 			</div>	<!-- END DOCTOR PHOTO -->


			 			<!-- DOCTOR'S BIO -->
						<div class="col-md-7">
							<div class="doctor-bio">
			 					<!-- Text -->	
                                 <?php echo $doctorData->biography; ?>							
							</div>
						</div>	<!-- END DOCTOR BIO -->

					</div>    <!-- End row -->	
				</div>	   <!-- End container -->
			</section>  <!-- END DOCTOR-1 DETAILS -->

<?php $this->load->view('components/common/footer'); ?>