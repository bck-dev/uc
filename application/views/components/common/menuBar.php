	<!-- PRELOADER SPINNER
		============================================= -->	
		<!-- <div id="loader-wrapper">
			<div id="loader"><div class="loader-inner"></div></div>
		</div> -->
		<!-- <button id="test" style="z-index: 9999; margin-top: 100px;">Test</button> -->
		
		<!-- PAGE CONTENT
		============================================= -->	
		<div id="page" class="page">


			<!-- HEADER
			============================================= -->
			<header id="header" class="header">


				<!-- MOBILE HEADER -->
			    <div class="wsmobileheader clearfix">
					<div class="mobile-top-bar">		     			
						<div class="address text-center">
							<a href="sms:6262987121" class="mr-2 font-weight-bold">
								<i class="fas fa-comments"></i>Text Us (E) 626-298-7121
							</a>
							<a href="sms:6262697744" class="font-weight-bold">
								(S) 626-269-7744
							</a>
							<br />
							<a href="tel:8183615437" class="font-weight-bold"><i class="fas fa-phone header-top-call-i"></i>Call Us 818-361-5437</a>
						</div>
						<hr class="m-0"/>
						<div class="address">
							<a class="pay-bil-section mr-3" href="https://pay.instamed.com/Form/PaymentPortal/Default?id=PEDAFFILIATES" target="_blank"><i class="fas fa-credit-card"></i> <span class="pay-bill-text">PAY YOUR BILL</span></a>
							<a class="pay-bil-section ml-3" href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank"><i class="fas fa-medkit"></i> <span class="pay-bill-text">PATIENT PORTAL</span></a>
						</div>
					</div>

			    	<a id="wsnavtoggle" class="wsanimated-arrow"><span></span></a>
			    	<span class="smllogo"><img src="<?php echo base_url('assets/images/uc-nw.png'); ?>" width="180"  alt="mobile-logo"/></span>
			    	<!-- <a href="tel:8183615437" class="callusbtn"><i class="fas fa-phone"></i></a> -->
			 	</div>


			 	<!-- HEADER STRIP -->
			 	<div class="headtoppart bg-blue clearfix">
			    	<div class="headerwp clearfix">

			    		<!-- Address -->
			     		<div class="headertop_left headertop-contact-area">			     			
			        		<div class="address clearfix">
								 
								
								<a href="sms:6262987121" class="callusbtn mr-2 font-weight-bold">
									<i class="fas fa-comments"></i>Text Us (E) 626-298-7121
								</a>
								<a href="sms:6262697744" class="callusbtn font-weight-bold">
									(S) 626-269-7744
								</a>
								<a href="tel:8183615437" class="callusbtn mr-2 font-weight-bold"><i class="fas fa-phone header-top-call-i"></i>Call Us 818-361-5437</a>
			            	</div>
			     		</div>

			     		<!-- Social Links -->
					    <div class="headertop_right">
					    	<a class="googleicon" title="Google" href="https://www.youtube.com/channel/UC5pMXGZ_F2OZUFdfy6YbIew"><i class="fab fa-youtube"></i> <span class="mobiletext02">Google</span></a>
					        <a class="linkedinicon" title="Instagram" href="https://www.instagram.com/napediatricurgentcare/" target="_blank"><i class="fab fa-instagram"></i> <span class="mobiletext02">Linkedin</span></a>
					        <a class="facebookicon" title="Facebook" href="https://www.facebook.com/pediatricafterhoursusa/" target="_blank"><i class="fab fa-facebook-f"></i> <span class="mobiletext02">Facebook</span></a>
	
							<a class="pay-bil-section pr-2" style="border-right: 1px solid white;" href="https://pay.instamed.com/Form/PaymentPortal/Default?id=PEDAFFILIATES" target="_blank"><i class="fas fa-credit-card"></i> <span class="pay-bill-text">PAY YOUR BILL</span></a>
					        <a class="pay-bil-section pl-1 pr-2" href="https://patientportal.intelichart.com/login/Account/Login?ReturnUrl=%2f" target="_blank"><i class="fas fa-medkit"></i> <span class="pay-bill-text">PATIENT PORTAL</span></a>
				      	</div>

				    </div>
			  	</div>	<!-- END HEADER STRIP -->


			  	<!-- NAVIGATION MENU -->
			  	<div class="wsmainfull menu clearfix">
    				<div class="wsmainwp clearfix">

    					<!-- LOGO IMAGE -->
    					<!-- For Retina Ready displays take a image with double the amount of pixels that your image will be displayed (e.g 360 x 80 pixels) -->
    					<div class="desktoplogo p-0" style="padding-top: 2px !important;"><a href="<?php echo base_url(''); ?>"><img src="<?php echo base_url('assets/images/uc-nw.png'); ?>"  width="300" alt="header-logo"></a></div>
    					<!-- MAIN MENU -->
      					<nav class="wsmenu clearfix">
        					<ul class="wsmenu-list">
								<li aria-haspopup="true"><a href="<?php echo base_url(''); ?>">Home</a></li>
								<li aria-haspopup="true"><a href="https://www.ktdoctor.com/doctors">Our Doctors</a></li>
								<li aria-haspopup="true"><a href="<?php echo base_url('locations'); ?>">Locations</a></li>
								<li aria-haspopup="true"><a href="<?php echo base_url('insurance'); ?>">Insurance</a></li>
								<li aria-haspopup="true"><a href="<?php echo base_url('contact'); ?>">Contact Us</a></li>								    <!-- NAVIGATION MENU BUTTON -->
							    <li class="nl-simple header-btn" aria-haspopup="true"><a href="<?php echo base_url('appointment'); ?>">Make an Appointment</a></li>
        					</ul>
        				</nav>	<!-- END MAIN MENU -->

    				</div>
    			</div>	<!-- END NAVIGATION MENU -->


			</header>	<!-- END HEADER -->

			