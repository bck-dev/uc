<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
	<head>

    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="author" content="BCKonnect"/>	
		<meta name="description" content="We are considered as one of the most visionary Pediatric Practices in the country. We believe in delivering the best, most up to date care for your children. As children of the future, they deserve it."/>
		<meta name="robots" content="index, follow"> 
  		<meta name="og:title" property="og:title" content="Pediatric Urgent Care"> 
		<meta name="keywords" content="Pediatric, Urgent Care, After Hour, Kids, Medical, Health, Healthcare, Doctor, Clinic, Care, Hospital">	
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<meta name="google-site-verification" content="p0DDjNwAMwcaVyzlrjUmyU6MniI82-gLPpXKdW0bwNg" />
			
  		<!-- SITE TITLE -->
		<title>Urgent Care | <?php echo $pageName; ?></title>
		<link rel="canonical" href="https://www.pediatricafterhour.com<?php if($pageName=="Home"): echo ""; else: echo "/".strtolower($pageName); endif;?>">					
		<!-- FAVICON AND TOUCH ICONS  -->
		<link rel="shortcut icon" href="<?php echo base_url('assets/images/fav.png'); ?>" type="image/x-icon">
		<link rel="icon" href="<?php echo base_url('assets/images/fav.png'); ?>" type="image/x-icon">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url('assets/images/fav.png'); ?>">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url('assets/images/fav.png'); ?>">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url('assets/images/fav.png'); ?>">
		<link rel="apple-touch-icon" href="<?php echo base_url('assets/images/fav.png'); ?>">

		<!-- GOOGLE FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet"> 	
		<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet"> 

		<!-- BOOTSTRAP CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css'); ?>">
				
		<!-- FONT ICONS -->
		<link href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" rel="stylesheet" crossorigin="anonymous">		
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/flaticon.css'); ?>">

		<!-- PLUGINS STYLESHEET -->
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/menu.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/magnific-popup.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/owl.carousel.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/owl.theme.default.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/animate.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/jquery.datetimepicker.min.css'); ?>">
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/dropdown-effects/fade-down.css'); ?>" media="all" rel="stylesheet">
				
		<!-- TEMPLATE CSS -->
		<link rel="stylesheet" href="<?php echo base_url('assets/uc/css/style.css'); ?>">	
		
		<!-- RESPONSIVE CSS -->
        <link rel="stylesheet" href="<?php echo base_url('assets/uc/css/responsive.css'); ?>">
		<!-- BCK Typography -->
		<link rel="stylesheet" href="<?php echo base_url('assets/bck/css/type.css'); ?>">
		<!-- BCK spacing -->
		<link rel="stylesheet" href="<?php echo base_url('assets/bck/css/spacing.css'); ?>">
		<!-- BCK color -->
		<link rel="stylesheet" href="<?php echo base_url('assets/bck/css/color.css'); ?>">

	
		<link rel="stylesheet" href="<?php echo base_url('assets/uc/css/custom.css'); ?>">
		<script src="<?php echo base_url('assets/common/js/jquery.min.js'); ?>"></script>
		<script src="https://code.jquery.com/jquery-migrate-3.1.0.min.js"></script>
		
		<script type='text/javascript' src='<?php echo base_url('assets/marquee/jquery.marquee.min.js'); ?>'></script>
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-KT3SK6C');</script>
		<!-- End Google Tag Manager -->
	
  </head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KT3SK6C"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142053013-3"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-142053013-3');
</script>
