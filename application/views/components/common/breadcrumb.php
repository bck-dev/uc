<div id="breadcrumb" class="division">
    <div class="container">
        <div class="row">						
            <div class="col">
                <div class=" breadcrumb-holder">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
                            <li class="breadcrumb-item active" aria-current="page"><?php echo $pageName; ?></li>
                        </ol>
                    </nav>
                    <h4 class="h4-sm steelblue-color"><?php echo $pageName; ?></h4>
                </div>
            </div>
        </div>  <!-- End row -->	
    </div>	<!-- End container -->		
</div>