<?php if(count($descriptionBox)>0): ?>
    <?php $i=1; foreach($descriptionBox as $d): ?>
        <section id="about-5" class="pt-100 about-section division">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-lg-6">
                        <div class="about-img text-center wow fadeInUp" data-wow-delay="0.6s">
                            <img class="img-fluid" src="<?php echo base_url();?>uploads/<?php echo $d->image?>" alt="about-image">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="txt-block pc-30 wow fadeInUp" data-wow-delay="0.4s">
                            <h3 class="h3-md steelblue-color"><?php echo $d->title ?></h3>
                            <p><?php echo $d->content ?></p>
                            <?php if($d->button_text!=" "):?>
                                <div class="w-100 mt-4">
                                <a href="<?php echo base_url(); ?><?php echo $d->link; ?>" class="btn btn-blue blue-hover"><?php echo $d->button_text?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php $i++; endforeach; ?>  
<?php endif; ?>       