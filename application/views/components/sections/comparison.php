<!-- smaller screens -->
<!-- <div class="container my-5 py-5 d-sm-block d-lg-none">
    <div class="row px-5">
        <div class="col-lg-12 text-center">    
            <h1 class="h3-md steelblue-color">Our Pediatric Urgent Care or Hospital Emergency Room?</h1>
            <h3 class="steelblue-color">Choosing the right location</h3>
        </div>
        <div class="col-lg-5 pt-5">
            <?php $this->load->view('components/sections/uc'); ?>
        </div>
        <div class="col-lg-2 pt-4"><br>
        <h4 class="text-danger text-center">Vs.</h4>
            <ul>
                <?php foreach($comparisonData as $c): ?>
                    <li class="mt-2 pt-2 text-center font-weight-bold text-danger"><?php echo $c->category; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-lg-5 pt-5">
            <?php $this->load->view('components/sections/emergency'); ?>
        </div>
    </div>
</div> -->

<div class="container my-5 py-5 d-sm-block d-lg-none">
    <div class="row px-5">
        <div class="col-lg-12 text-center">    
            <h1 class="h3-md steelblue-color">Our Pediatric Urgent Care or Hospital Emergency Room?</h1>
            <h3 class="steelblue-color">Choosing the right location</h3>
        </div>
    </div>
        <?php foreach($comparisonData as $cd): ?>  
            <div class="row"> 
                <div class="col-lg-2 pt-2 pb-2">    
                    <li class="mt-2 pb-1 text-center font-weight-bold text-danger"><?php echo $cd->category; ?></li>
                </div>    
                <div class="col-lg-5 pt-2">
                <h4 class="text-lg-right text-sm-left">Urgent Care</h4>
                    <li class="text-lg-right text-sm-left mt-2"><?php echo $cd->urgentCare; ?></li>
                </div>
                <div class="col-lg-5 pt-2">
                <h4 class="text-left">Emergency Room</h4>
                    <li class="mt-1 text-left mt-2"><?php echo $cd->emergencyRoom; ?></li> 
                </div>
            </div>
        <?php endforeach; ?>
</div>

<!-- larger screens -->
<div class="container my-5 py-5 d-none d-lg-block">
    <div class="row px-5">
        <div class="col-lg-12 text-center">    
            <h1 class="h3-md steelblue-color">Our Pediatric Urgent Care or Hospital Emergency Room?</h1>
            <h3 class="steelblue-color">Choosing the right location</h3>
        </div>
    
        <div class="col-lg-5 pt-5">
            <h4 class="text-lg-right text-sm-left">Urgent Care</h4>
        </div>
        <div class="col-lg-2 pt-5"> 
        <h4 class="text-center text-danger">Vs.</h4>
        </div>
        <div class="col-lg-5 pt-5">
            <h4 class="text-left">Emergency Room</h4>
        </div>
    </div>

    <ul>
        <?php foreach($comparisonData as $cd): ?>  
            <div class="row">     
                <div class="col-lg-5 pt-2">
                    <li class="text-lg-right text-sm-left mt-2"><?php echo $cd->urgentCare; ?></li>
                </div>
                <div class="col-lg-2 pt-2">    
                    <li class="mt-2 pb-1 text-center font-weight-bold text-danger"><?php echo $cd->category; ?></li>
                </div>
                <div class="col-lg-5 pt-2">
                    <li class="mt-1 text-left mt-2"><?php echo $cd->emergencyRoom; ?></li> 
                </div>
            </div>
        <?php endforeach; ?>
    </ul>
</div>


