
<div class="container">
    <?php if($status=="Success") :?>
        <div class="row" id="message">
            <div class="col-lg-12 light_blue_background white p-3 mb-4 md-3">
                <p class="font20 b600">Thank you for reaching us. </p>
                <p>We will get back to you shortly in order to process your appointment request. Please call us on <a class="white" href="tel:8183615437">(818) 361-5437</a> or text us on <a href="sms:6262987121" class="white">(626) 298-7121</a>(English) and <a href="sms:6262697744" class="white"> (626) 269-7744</a> (Spanish) for information. </p>
            </div>
        </div>

        <script>
            $(document).ready(function(){
                $('html, body').animate({
                    scrollTop: $("#message").offset().top-200
                }, 1000);
            });
        </script>
    <?php endif; ?>

    <?php if($locationName): ?>
        <script>
            $(document).ready(function(){
                $('html, body').animate({
                    scrollTop: $("#form").offset().top-200
                }, 1000);
            });
        </script>
    <?php endif; ?>

        
    <h3 class="mt-5">Request an appointment</h3><br>
    Complete the form below to request an appointment at After Hours Pediatric Urgent Care.<br><br>
    For non life threatening emergencies contact us on <a href="tel:8183615437" class="callusbtn mr-2 blue-color font-weight-bold">(818) 361-5437.</a>If you are having a medical emergency, call 911 for emergency medical help. The team members who will care for your child have the expertise and skills to provide the best care possible.<br>

    <form action="<?php echo base_url();?>appointment/book" id="form" method="POST">
        <?php $date = new DateTime(); $today = $date->format('Y-m-d'); ?>
        <div class="row" style="padding-top: 50px;padding-bottom: 50px;">
            <div class="col-lg-12 font15 b700 mb-3">Patient Details:</div>

            <div class="col-lg-4 pb-3">
                <input type="text" name="fname" class="form-control custom_feild" placeholder="Patient's First Name" required/>
            </div>
            <div class="col-lg-4 pb-3">
                <input type="text" name="lname" class="form-control custom_feild" placeholder="Patient's Last Name" required/>
            </div>
            <div class="col-lg-4 pb-3">
                <input type="text" name="dob" onfocus="(this.type='date')" max="<?php echo $today ?>" class="form-control custom_feild" placeholder="Patient's Date of Birth" required/>
            </div>

            <div class="col-lg-6 pb-3">
                <input type="tel" pattern="[\(]\d{3}[\)][\ ]\d{3}[\-]\d{4}" name="cellNum" id="cellNum" minlength="12" class="form-control custom_feild" placeholder="Cell Phone Number" required/>
            </div>
            <div class="col-lg-6 pb-3">
                <input type="email" class="form-control custom_feild" name="email" placeholder="Email Address" required/>
            </div>

            <div class="col-lg-12 font15 b700 mb-3">Appointment Details:</div>

            <div class="col-lg-6 pb-3">
                <input type="text" onfocus="(this.type='date')" min="<?php echo $today ?>" class="form-control custom_feild" name="date" placeholder="Preferred Date" required/>
            </div>
            <div class="col-lg-6 pb-3">
                <input type="text" onfocus="(this.type='time')" class="form-control custom_feild" name="time" placeholder="Preferred Time" required/>
            </div>

            <div class="col-lg-6 pb-3">
                <select class="form-control custom_feild" name="reason" required>
                    <option value="" disabled="disabled" selected="selected">Reason for visit</option>
                    <option>Sickness</option>
                    <option>Injury</option>
                    <option>Physical</option>
                    <option>Other</option>
                </select>
            </div>
            <div class="col-lg-6 pb-3">
                <select class="form-control custom_feild" name="location" required>
                    <?php if($locationName): ?>
                        <option value="<?php echo $locationName; ?>"><?php echo $locationName; ?></option>
                    <?php else: ?>
                        <option value="" disabled="disabled" selected="selected">Preferred Method</option>
                    <?php endif; ?>
                    <option value="" disabled="disabled">---Method---</option>
                    <option value="Telehealth">Telehealth</option>
                    <option value="" disabled="disabled">---Visit Us---</option>
                    <?php foreach($locations as $location): ?>
                        <option value="<?php echo $location->name; ?>"><?php echo $location->name; ?></option>
                    <?php endforeach; ?>        
                </select>
            </div>

            <div class="col-lg-12 font15 b700 mb-3">Additional Comments:</div>

            <div class="col-lg-12 pb-3">
                <textarea name="comment" class="form-control mb-4 custom_feild"></textarea>
            </div>

            <div class="col-lg-3 pt-0 pb-7">
                <button type="submit" class="btn btn-block appointment_btn" id="btn">SUBMIT</button>
                <div  class="btn btn-block appointment_btn d-none" id="btnreplacement">SUBMITTING</div>
            </div>
        </div>
    </form>
</div>

<script>
    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());

    document.getElementById('cellNum').addEventListener('input', function (e) {
        var x = e.target.value.replace(/\D/g, '').match(/(\d{0,3})(\d{0,3})(\d{0,4})/);
        e.target.value = !x[2] ? x[1] : '(' + x[1] + ') ' + x[2] + (x[3] ? '-' + x[3] : '');
    });
</script>

<script>
	// $.validator.setDefaults({
	// 	submitHandler: function() {
    //         $('#appointment').submit();
	// 	}
	// });

	// // $().ready(function() {
	// // 	$("#appointment").validate({
	// // 		rules: {
    // //             cellNum: {
	// // 				required: true,
    // //                 minlength: 12
	// // 			}
	// // 		},
	// // 		messages: {
	// // 			cellNum: {
	// // 				required: "Please enter a cell number.",
    // //                 minlength: "Please enter a valid cell number."
	// // 			}
	// // 		}
	// // 	});

	// // });

    $('#btn').click(function() {
		$('#btn').css('display', 'none');
        $('#btnreplacement').removeClass('d-none');
        $('#btnreplacement').css('display', 'block');
	});
</script>

<script>
    $("#scrollToForm").click(function() {
        $('html, body').animate({
            scrollTop: $("#form").offset().top - 300
        }, 2000);
    });
</script>