<div id='map_canvas' style="width: 100%; height: 600px;"></div>

<script>

    jQuery(function($) {
        // Asynchronously Load the map API 
        var script = document.createElement('script');
        script.src = "https://maps.googleapis.com/maps/api/js?v=3.38&callback=initialize&key=AIzaSyB_xKPDja6o9znHZAmvv-GpjFVFhlLwV_Y";
        document.body.appendChild(script);
    });

  
    function initialize() {
        var map;
        var bounds = new google.maps.LatLngBounds();
        var mapOptions = {
            mapTypeId: 'roadmap'
        };

        map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        var markers = new Array();
        <?php $i= 1; foreach($locations as $l):  ?>
        var marker = ['<?php echo $i; ?>', <?php echo $l->latitude; ?>, <?php echo $l->longitude; ?>, '#<?php echo $i; ?>'];
        markers.push(marker);
        <?php $i++; endforeach; ?>

        var bounds = new google.maps.LatLngBounds();
        var infowindow = new google.maps.InfoWindow();  
        
        for( i = 0; i < markers.length; i++ ) {
        var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0],
            url: markers[i][3],
            label: {color: '#fff', fontSize: '16px', fontWeight: '600', text: markers[i][0]}
        });
        bounds.extend(position);

            
        }
        map.fitBounds(bounds);
        // Override our map zoom level once our fitBounds function runs (Make sure it only runs once)
        var boundsListener = google.maps.event.addListener((map), 'bounds_changed', function(event) {
            this.setZoom(10);
            google.maps.event.removeListener(boundsListener);
        });

    }



</script>