<div class="container my-5 py-5">
    <div class="row px-5">
        <!-- <div class="col-lg-12 text-center"> -->
        <div class="col-lg-12 text-center">    
            <h1 class="h3-md steelblue-color">Our Pediatric Urgent Care or Hospital Emergency Room?</h1>
            <h3 class="steelblue-color">Choosing the right location</h3>
        </div>
        <div class="col-lg-6 pt-5 pr-5">
            <?php $this->load->view('components/sections/uc'); ?>
        </div>
        <div class="col-lg-6 pt-5">
            <?php $this->load->view('components/sections/emergency'); ?>
        </div>
    </div>
</div>
