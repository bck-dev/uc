<section id="services-2" class="wide-70 services-section division">
    <div class="container">
        <div class="row">	
            <div class="col-lg-10 offset-lg-1 section-title">		

                <!-- Title 	-->	
                <h3 class="h3-md steelblue-color">Our Signature Services</h3>	

                <!-- Text -->
                <p>Ease of access, prompt courteous service and responsiveness to our patient’s concerns will always be our primary goal. We strive to treat them professionally with compassion, respect and dignity. 
                </p>
                            
            </div> 
        </div>

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/rx.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Asthma & Allergies</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/medical-report.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Accepting most insurances</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/pharmacist.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Same day sick visits</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/thermometer.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Healthcare for kids & teens</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/soccer.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Sports injuries</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/vaccine.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Immunization & Vaccination</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/heart.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">ADHD & Behavioral Issues</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/health.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Extended Hour Pediatric Care</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/gender.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">OB/GYN Services</h5>
                </div>							
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="sbox-2 wow fadeInUp" data-wow-delay="0.4s">
                    <div class="sbox-2-icon icon-xl">
                        <img src="<?php echo base_url('assets/images/mother.png') ?>" class="service_img"/>
                    </div>
                    <h5 class="h5-sm sbox-2-title steelblue-color">Expectant Mothers</h5>
                </div>							
            </div>
        </div>
    </div>	
</section>
