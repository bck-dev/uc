<section id="faqs-page" class="faqs-section division">
    <div class="container pb-5 mb-5">
        <div class="row">
            <div class="col-lg-12">
                <div class="questions-holder">	
                    <div id="accordion" role="tablist">
                        <?php $i=1; foreach($accordions as $accordion): ?>
                            <div class="card">
                                <div class="card-header" role="tab" id="heading<?php echo $accordion->id ?>">
                                    <h5 class="h5-xs">
                                        <a data-toggle="collapse" href="#collapse<?php echo $accordion->id ?>" role="button" aria-expanded="true" aria-controls="collapse<?php echo $accordion->id ?>">
                                            <?php echo $accordion->title ?>
                                        </a>
                                    </h5>
                                </div>
                                <div id="collapse<?php echo $accordion->id ?>" class="collapse <?php if ($i==1): ?>show<?php endif; ?>" role="tabpanel" aria-labelledby="heading<?php echo $accordion->id ?>" data-parent="#accordion">
                                    <div class="card-body">
                                        <p><?php echo $accordion->content ?></p>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endforeach; ?>
                    </div>
                </div>
            </div>
        </div>	
    </div>
</section>