<section id="about-1" class="about-section division">
    <div class="container">
        <div class="row d-flex row-eq-height align-items-center">
            <!-- ABOUT BOX #1 -->
            <div id="abox-1" class="col-md-8 col-lg-8">
                <div class="abox-1 white-color">

                    <div class="row align-self-bottom">
                        <div class="col-12 col-lg-3">
                            <h5 class="white pb-3" style="min-width: 135px;">Our Locations</h5>
                        </div>
                        <div class="col-12 col-lg-9 white p-0">
                            <p class="m-0"><span class="font12 b700">SORT BY:</span>
                                <div class="align-text-bottom text-left p-0">
                                    <label for="opt4" class="radio">
                                        <input type="radio" name="sort" id="opt4" class="hidden" checked/>
                                        <span class="label"></span>After Hours
                                    </label>
                                    <label for="opt1" class="radio">
                                        <input type="radio" name="sort" id="opt1" class="hidden"/>
                                        <span class="label"></span>Mon
                                    </label>

                                    <label for="opt5" class="radio">
                                        <input type="radio" name="sort" id="opt5" class="hidden"/>
                                        <span class="label"></span>Tue
                                    </label>

                                    <label for="opt6" class="radio">
                                        <input type="radio" name="sort" id="opt6" class="hidden"/>
                                        <span class="label"></span>Wed
                                    </label>

                                    <label for="opt7" class="radio">
                                        <input type="radio" name="sort" id="opt7" class="hidden"/>
                                        <span class="label"></span>Thu
                                    </label>

                                    <label for="opt8" class="radio">
                                        <input type="radio" name="sort" id="opt8" class="hidden"/>
                                        <span class="label"></span>Fri
                                    </label>
                                    
                                    <label for="opt2" class="radio">
                                        <input type="radio" name="sort" id="opt2" class="hidden"/>
                                        <span class="label"></span>Sat
                                    </label>
                                    
                                    <label for="opt3" class="radio">
                                        <input type="radio" name="sort" id="opt3" class="hidden"/>
                                        <span class="label"></span>Sun
                                    </label>
                                </div>
                            </p>
                        </div>
                    </div>
                    <div class="d-none d-lg-block">
                        <div class="row row-eq-height mt-3 p-0">
                            <?php $i=1; foreach($locations as $location): ?>
                                <div class="col-lg-6 pl-0 <?php if($i%2==0 && $i!=10):?> pr-0 <?php endif; ?> <?php if($i==10):?>mtm<?php endif; ?> <?php if($i==10): echo"order-3"; else: echo"order-1"; endif;?> home_location <?php if($x==5): echo"float-left"; endif;?>">
                                    <div class="row yellow <?php if($location->afterHour=="on"): ?>afterHour <?php else: ?> notAfterHour <?php endif; ?><?php if($location->saturday!="on"): ?>notSaturday <?php endif; ?><?php if($location->sunday!="on"): ?>notSunday <?php endif; ?><?php if($location->mon!="on"): ?>notMon <?php endif; ?><?php if($location->tue!="on"): ?>notTue <?php endif; ?><?php if($location->wed!="on"): ?>notWed <?php endif; ?><?php if($location->thu!="on"): ?>notThu <?php endif; ?><?php if($location->fri!="on"): ?>notFri <?php endif; ?> common" >
                                        <div class="col-5 location_cell p-0 align-self-center font-s1 b600" >
                                            <a href="<?php echo base_url('locations/').str_replace(' ', '',strtolower($location->name));?>" class="lname <?php if($location->afterHour=="on"): ?>yellow<?php endif; ?>"><?php echo $location->name ?></a>
                                            <?php if($location->afterHour=="on"): ?>&nbsp;
                                                <i class="far fa-clock font-s1 yellow <?php if($location->afterHour=="on"): ?>afterHour <?php else: ?> notAfterHour <?php endif; ?><?php if($location->saturday!="on"): ?>notSaturday <?php endif; ?><?php if($location->sunday!="on"): ?>notSunday <?php endif; ?><?php if($location->mon!="on"): ?>notMon <?php endif; ?><?php if($location->tue!="on"): ?>notTue <?php endif; ?><?php if($location->wed!="on"): ?>notWed <?php endif; ?><?php if($location->thu!="on"): ?>notThu <?php endif; ?><?php if($location->fri!="on"): ?>notFri <?php endif; ?> common"></i>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-3 location_cell p-0">
                                            <a href="sms:6262987121" class="align-self-center lt_btn" >
                                                    <i class="far fa-comments font-s3"></i> Text Us
                                            </a>
                                        </div>
                                        <div class="col-4 location_cell p-0 time b600 text-right align-self-center font-s3 currentDay" >
                                            <?php if($location->fromHour!="00:00:00"): ?>
                                                <?php $date = DateTime::createFromFormat( 'H:i:s', $location->fromHour); echo $date->format( 'g:i a'); ?> 
                                                - <?php $date = DateTime::createFromFormat( 'H:i:s', $location->toHour); echo $date->format( 'g:i a'); ?>
                                            <?php else: ?> 
                                                <span class="font-s3">Closed</span>
                                            <?php endif; ?> 
                                        </div>
                                        <?php foreach($location->schedule as $ls): ?>
                                            <div class="col-4 location_cell p-0 time b600 text-right align-self-center font-s3 notCurrentDay d-none <?php echo $ls->day;?>">
                                                <?php if($ls->fromHour!="00:00:00"): ?>
                                                    <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->fromHour); echo $date->format( 'g:i a'); ?> 
                                                    - <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->toHour); echo $date->format( 'g:i a'); ?>
                                                <?php else: ?> 
                                                    <span class="font-s3">Closed</span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php $i++; endforeach; ?>
                            <div class="col-lg-6 order-2 p-0">   
                                <div class="row p-0 py-2 no_border" >
                                    <div class="col-6 p-0 font-s1 text-center en_text yellow" >
                                        <a href="sms:6262987121" class="lt_btn_big btn-block mb-1 yellow"><i class="far fa-comments yellow"></i> (626) 298-7121</a>
                                        ENGLISH TEXT
                                    </div>
                                    <div class="col-6 p-0 font-s1 text-center sp_text yellow">
                                        <a href="sms:6262697744" class="lt_btn_big btn-block mb-1 yellow"><i class="far fa-comments yellow"></i> (626) 269-7744</a>
                                        SPANISH TEXT
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                    <div class="d-block d-lg-none">
                        <div class="row row-eq-height mt-4">
                            <?php foreach($locations as $location): ?>
                                <div class="col-lg-6 home_location p-0">
                                    <div class="row yellow <?php if($location->afterHour=="on"): ?>afterHour <?php else: ?> notAfterHour <?php endif; ?><?php if($location->saturday!="on"): ?>notSaturday <?php endif; ?><?php if($location->sunday!="on"): ?>notSunday <?php endif; ?><?php if($location->mon!="on"): ?>notMon <?php endif; ?><?php if($location->tue!="on"): ?>notTue <?php endif; ?><?php if($location->wed!="on"): ?>notWed <?php endif; ?><?php if($location->thu!="on"): ?>notThu <?php endif; ?><?php if($location->fri!="on"): ?>notFri <?php endif; ?> common" >
                                        <div class="col-5 location_cell p-0 align-self-center font-s1 b600" >
                                            <a href="<?php echo base_url('locations/').str_replace(' ', '',strtolower($location->name));?>" class="<?php if($location->afterHour=="on"): ?>yellow<?php endif; ?>"><?php echo $location->name ?></a>
                                            <?php if($location->afterHour=="on"): ?>&nbsp;
                                                <i class="far fa-clock font-s1 yellow <?php if($location->afterHour=="on"): ?>afterHour <?php else: ?> notAfterHour <?php endif; ?><?php if($location->saturday!="on"): ?>notSaturday <?php endif; ?><?php if($location->sunday!="on"): ?>notSunday <?php endif; ?><?php if($location->mon!="on"): ?>notMon <?php endif; ?><?php if($location->tue!="on"): ?>notTue <?php endif; ?><?php if($location->wed!="on"): ?>notWed <?php endif; ?><?php if($location->thu!="on"): ?>notThu <?php endif; ?><?php if($location->fri!="on"): ?>notFri <?php endif; ?> common"></i>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-3 p-0 location_cell lt_btn_box align-self-center" >
                                            <a href="sms:6262987121" class="lt_btn btn-block b700">
                                                <i class="far fa-comments"></i> &nbsp; Text Us
                                            </a>
                                        </div>
                                        <div class="col-4 p-0 location_cell time pt-1 b600 text-right align-self-center font-s2 currentDay" >
                                            <?php if($location->fromHour!="00:00:00"): ?>
                                                <?php $date = DateTime::createFromFormat( 'H:i:s', $location->fromHour); echo $date->format( 'g a'); ?> 
                                                - <?php $date = DateTime::createFromFormat( 'H:i:s', $location->toHour); echo $date->format( 'g a'); ?>
                                            <?php else: ?> 
                                                <span class=" font-s2">Closed</span>
                                            <?php endif; ?> 
                                        </div>
                                        <?php foreach($location->schedule as $ls): ?>
                                            <div class="col-4 p-0 location_cell time b600 text-right align-self-center font-s2 notCurrentDay d-none <?php echo $ls->day;?>">
                                                <?php if($ls->fromHour!="00:00:00"): ?>
                                                    <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->fromHour); echo $date->format( 'g a'); ?> 
                                                    - <?php $date = DateTime::createFromFormat( 'H:i:s', $ls->toHour); echo $date->format( 'g a'); ?>
                                                <?php else: ?> 
                                                    <span class="font-s2">Closed</span>
                                                <?php endif; ?>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                            <div class="col-lg-6 order-2 p-0">   
                                <div class="row p-0 py-3 no_border" >
                                    <div class="col-6 p-0 font13 text-center" >
                                        <a href="sms:6262987121" class="lt_btn_big btn-block b600 font16"><i class="far fa-comments yellow"></i> &nbsp; (626) 298-7121</a>
                                        ENGLISH TEXT
                                    </div>
                                    <div class="col-6 p-0 font13 text-center" >
                                        <a href="sms:6262697744" class="lt_btn_big btn-block b600 font16"><i class="far fa-comments yellow"></i> &nbsp; (626) 269-7744</a>
                                        SPANISH TEXT
                                    </div>
                                </div>
                            </div>  
                        </div>
                    </div>

                    <div class="row home_appoinment">
                        <div class="col-lg-12 white font13 text-center px-0 pt-2">
                            Is your local office closed?
                            <span class="font18 ml-2">Call our hotline <a class="b700" href="tel:8183615437">(818) 361-5437</a></span>
                            <span class="font16 ml-1">or make an <a class="font18 b700" href="https://www.ktdoctor.com/appointment">APPOINTMENT</a></span>
                        </div>

                        <div class="blinking">
                            We are now offering 24/7 Telehealth appointments during COVID -19 pandemic.
                        </div>
                    </div>

                </div>
            </div>


           
            <!-- ABOUT BOX #4 -->
            <div id="abox-4" class="col-md-4 col-lg-4">
                <div class="abox-1 white-color p-4" style="padding-top: 35px !important;">
                    <h3 class="h3-md">Why us?</h3>
                    <div class="d-flex mt-3">
                        <div class="" style="width: 50px;"><i class="fas fa-clock font-b9 steelblue-color"></i></div>
                        <div class="b600 steelblue-color" style="margin-top: 5px;">Less wait time</div>
                    </div>
                    <div class="d-flex mt-3">
                        <div class="" style="width: 50px;"><i class="fas fa-donate font-b9 steelblue-color"></i></div>
                        <div class="b600 steelblue-color" style="margin-top: 5px;">Lower quote</div>
                    </div>
                    <div class="d-flex mt-4">
                        <div class="" style="width: 50px;"><i class="fas fa-user-md font-b9 steelblue-color"></i></div>
                        <div class="b600 steelblue-color" style="margin-top: -7px;">Care for children 0-21 by pediatricians</div>
                    </div>
                    <div class="d-flex mt-3">
                        <div class="" style="width: 50px;"><i class="fas fa-briefcase-medical font-b8 steelblue-color"></i></div>
                        <div class="b600 steelblue-color" style="margin-top: 5px;">We accept most insurances</div>
                    </div>
                    <div class="d-flex mt-4">
                        <div class="" style="width: 50px;"><i class="far fa-calendar-check font-b9"></i></div>
                        <div class="b600" style="margin-top: -7px;">Walk-in and same day appointments</div>
                    </div>
                    <div class="d-flex mt-3">
                        <div class="" style="width: 50px; margin-top:13px;"><i class="fas fa-hourglass-half font-b8"></i></div>
                        <div class="b600" style="margin-top: -7px;">Regular and consistent <br />hours including evening and <br />weekends</div>
                    </div>
                </div>
            </div>


        </div>    <!-- End row -->
    </div>	   <!-- End container -->	
</section>	<!-- END ABOUT-1 -->




<script>
$(document).ready(function(){
    $(".currentDay").removeClass('d-block');
    $(".notCurrentDay").removeClass('d-block'); 
    $(".notCurrentDay").addClass('d-none');
    $(".currentDay").removeClass('d-none')
    $(".currentDay").addClass('d-block');
    $(".common").css('opacity', '1');
    $(".notAfterHour").css('opacity', '0.5');        
    $(".common" ).find( ".lt_btn" ).css( "display", "block" );
    $(".notAfterHour" ).find( ".lt_btn" ).css( "display", "none" );    
    $(".notAfterHour").removeClass('yellow'); 
    $(".notAfterHour").addClass('white');
}); 


$("#opt3").click(function(){
    if ($('#opt3').val() == 'on') {
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none'); 
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');        
        $(".Sunday").removeClass('d-none');
        $(".Sunday").addClass('d-block'); 
        $(".common").css('opacity', '1');
        $(".notSunday").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notSunday" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notSunday").removeClass('yellow'); 
        $(".notSunday").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notSunday").find( ".lname" ).removeClass('yellow'); 
        $(".notSunday").find( ".lname" ).addClass('white');
    }
}); 
$("#opt2").click(function(){
    if ($('#opt2').val() == 'on') {       
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none');
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');
        $(".Saturday").removeClass('d-none');
        $(".Saturday").addClass('d-block'); 
        $(".common").css('opacity', '1');
        $(".notSaturday").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notSaturday" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notSaturday").removeClass('yellow'); 
        $(".notSaturday").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notSaturday").find( ".lname" ).removeClass('yellow'); 
        $(".notSaturday").find( ".lname" ).addClass('white');
    }
}); 
$("#opt4").click(function(){
    if ($('#opt4').val() == 'on') { 
        $(".currentDay").removeClass('d-block');
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');
        $(".currentDay").removeClass('d-none')
        $(".currentDay").addClass('d-block');
        $(".common").css('opacity', '1');
        $(".notAfterHour").css('opacity', '0.5');        
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notAfterHour" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notAfterHour").removeClass('yellow'); 
        $(".notAfterHour").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notAfterHour").find( ".lname" ).removeClass('yellow'); 
        $(".notAfterHour").find( ".lname" ).addClass('white');
    }
}); 
$("#opt1").click(function(){
    if ($('#opt1').val() == 'on') {
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none'); 
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');        
        $(".Monday").removeClass('d-none');
        $(".Monday").addClass('d-block');     
        $(".common").css('opacity', '1');
        $(".notMon").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notMon" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notMon").removeClass('yellow'); 
        $(".notMon").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notMon").find( ".lname" ).removeClass('yellow'); 
        $(".notMon").find( ".lname" ).addClass('white');
    }
}); 
$("#opt5").click(function(){
    if ($('#opt5').val() == 'on') { 
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none'); 
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');        
        $(".Tuesday").removeClass('d-none');
        $(".Tuesday").addClass('d-block');        
        $(".common").css('opacity', '1');
        $(".notTue").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notTue" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notTue").removeClass('yellow'); 
        $(".notTue").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notTue").find( ".lname" ).removeClass('yellow'); 
        $(".notTue").find( ".lname" ).addClass('white');
    }
}); 
$("#opt6").click(function(){
    if ($('#opt6').val() == 'on') { 
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none'); 
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');        
        $(".Wednesday").removeClass('d-none');
        $(".Wednesday").addClass('d-block');        
        $(".common").css('opacity', '1');
        $(".notWed").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notWed" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notWed").removeClass('yellow'); 
        $(".notWed").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notWed").find( ".lname" ).removeClass('yellow'); 
        $(".notWed").find( ".lname" ).addClass('white');
    }
}); 
$("#opt7").click(function(){
    if ($('#opt7').val() == 'on') { 
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none'); 
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');        
        $(".Thursday").removeClass('d-none');
        $(".Thursday").addClass('d-block');        
        $(".common").css('opacity', '1');
        $(".notThu").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notThu" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notThu").removeClass('yellow'); 
        $(".notThu").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notThu").find( ".lname" ).removeClass('yellow'); 
        $(".notThu").find( ".lname" ).addClass('white');
    }
}); 
$("#opt8").click(function(){
    if ($('#opt8').val() == 'on') {
        $(".currentDay").removeClass('d-block');
        $(".currentDay").addClass('d-none'); 
        $(".notCurrentDay").removeClass('d-block'); 
        $(".notCurrentDay").addClass('d-none');        
        $(".Friday").removeClass('d-none');
        $(".Firday").addClass('d-block');         
        $(".common").css('opacity', '1');
        $(".notFri").css('opacity', '0.5');
        $(".common" ).find( ".lt_btn" ).css( "display", "block" );
        $(".notFri" ).find( ".lt_btn" ).css( "display", "none" );
        $(".common").addClass('yellow');
        $(".notFri").removeClass('yellow'); 
        $(".notFri").addClass('white');
        $(".lname" ).removeClass('white'); 
        $(".lname" ).addClass('yellow');
        $(".notFri").find( ".lname" ).removeClass('yellow'); 
        $(".notFri").find( ".lname" ).addClass('white');
    }
}); 
</script>