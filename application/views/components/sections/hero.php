

<?php $i=1; foreach($banner as $b): ?>
	<?php if($i==1):?>
		<section id="hero-1" class="bg-fixed hero-section division" style="background-image: url('<?php echo base_url();?>uploads/<?php echo $b->baseImage?>'); ">
			<div class="container">						
				<div class="row d-flex align-items-center">
					<div class="col-md-8 col-lg-7 col-xl-6">
						<div class="hero-txt mb-40">
							<h2 class="steelblue-color"><?php echo $b->title; ?></h2>
							<p class="steelblue-color p-md"><?php echo $b->content?></p>
							<!-- <a href="<?php echo base_url(); ?><?php echo $b->buttonLink; ?>" class="btn btn-blue blue-hover"><?php echo $b->buttonText?></a>										
							<a href="<?php echo base_url(); ?><?php echo $b->buttonLink2; ?>" class="btn btn-magenda"><?php echo $b->buttonText2?></a> -->								
							
							<div class="marquee" data-duplicated='true' data-direction='left' data-duration="10000">
								<?php $x=1; foreach($sliders as $s): ?>
									<span class="mr-3"><span style="font-weight: 700;"><?php echo $s->title; ?> </span><b><?php $strnosp = str_replace('<p>', '', $s->content);  $strnoep = str_replace('</p>', '', $strnosp); $strnobr = str_replace('<br>', '', $strnoep); echo $strnobr; ?></b></span>
								<?php $x++; endforeach; ?>
							</div>	
							
							<div class="row">
								<div class="col-md-6">
									<div>
                        				<a href="<?php echo base_url('appointment'); ?>">
                            				<div class="request-appoit-btn">Request Appointment<br /><span class="request-appoint-b">Telehealth </span>or<span class="request-appoint-b"> In-Office</span>
                            				</div>
                        				</a>
                        			</div>
								</div>
							</div>	

						</div>
					</div>
					<div class="col-md-4 col-lg-5 col-xl-6">	
						<div class="hero-1-img text-center">				
							<img class="img-fluid" src="<?php echo base_url();?>uploads/<?php echo $b->topImage?>" alt="hero-image">
						</div>
					</div>
				</div>
			</div>
		</section>
    <?php $i++; endif; ?>  
<?php $i++; endforeach; ?> 

<script>
	$(document).ready(function(){
		$('.marquee').marquee({
			direction: 'left'
		});
	});
</script>
