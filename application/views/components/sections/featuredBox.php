<?php if(count($featured)>0): ?>
    <?php $i=1; foreach($featured as $f): ?>
        <section id="info-8" class="info-section division">	
            <div class="container">
                <div class="row d-flex align-items-center">	
                    <div class="col-md-7 col-lg-5">
                        <div class="txt-block pc-30 white-color wow fadeInUp" data-wow-delay="0.4s">
                            <h4 class="h4-sm"><?php echo $f->title ?></h4>
                            <p><?php echo $f->content ?></p>
                            <?php if($f->button_text!=" "):?>
                                <div class="w-100 p-4 text-center mt-5">
                                    <a href="<?php echo base_url(); ?><?php echo $f->link ?>" class="home_white_btn" style="font-size: 16px; padding: 8px 20px;"><?php echo $f->button_text ?></a>
                                </div>
                            <?php endif; ?>
                        </div>	
                    </div>	
                </div>
            </div>
            <div class="featured-img text-center" style="background-image: url('<?php echo base_url();?>uploads/<?php echo $f->image; ?>');"></div>
        </section>

    <?php $i++; endforeach; ?>  
<?php endif; ?>       