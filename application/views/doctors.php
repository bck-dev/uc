<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/breadcrumb'); ?>

			<!-- DOCTORS-3
			============================================= -->
			<section id="doctors-3" class="bg-lightgrey wide-60 doctors-section division">
				<div class="container">
					<div class="row">
					<?php foreach($doctorData as $dataRow): ?>

						<!-- DOCTOR #1 -->
						<div class="col-md-6 col-lg-4">
							<div class="doctor-2">	

								<!-- Doctor Photo -->
								<div class="hover-overlay"> 
									<img class="img-fluid" src="<?php echo base_url(); ?>/uploads/<?php echo $dataRow->image; ?>" alt="doctor-foto">	
								</div>								
														
								<!-- Doctor Meta -->		
								<div class="doctor-meta">

									<h5 class="h5-xs blue-color"><?php echo $dataRow->name; ?></h5>
									<span><?php echo $dataRow->specialty; ?></span>

									<!-- Button -->
									<a class="btn btn-sm btn-blue blue-hover mt-15" href="<?php echo base_url('viewDoctor/');?><?php echo $dataRow->name ?>" >View More Info</a>
									
								</div>	

							</div>								
						</div>	<!-- END DOCTOR #1 -->

						<?php endforeach; ?>

					</div>	    <!-- End row -->
				</div>	   <!-- End container -->
			</section>	<!-- END DOCTORS-3 -->

<?php $this->load->view('components/common/footer'); ?>