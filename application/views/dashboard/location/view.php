<?php $this->load->view('dashboard/common/header.php')?>
<?php $this->load->view('dashboard/common/sidebar.php')?>



<div class="content-wrapper p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                <?php echo $location->name ?> 
                <a class="btn btn-sm btn-warning ml-5" href="<?php echo base_url();?>/admin/location/edit/<?php echo $location->id ?>">Edit</a>
                <a class="btn btn-sm btn-danger" href="<?php echo base_url();?>/admin/location/delete/<?php echo $location->id ?>">Delete</a>
                <a href="<?php echo base_url('admin/location/new');?>" class="btn btn-sm btn-success ml-5">Add new location</a>
                <a href="<?php echo base_url('admin/location');?>" class="btn btn-sm btn-warning">Go Back</a>
            </h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-6">
                    <p><b>Name: </b><?php echo $location->name ?></p>
                    <p><b>Address: </b><?php echo $location->address ?></p>
                    <p><b>Telephone: </b><?php echo $location->telephone ?></p>
                    <p><b>Fax: </b><?php echo $location->fax ?></p>
                    <p><b>Cordinates: </b><?php echo $location->longitude ?>, <?php echo $location->latitude ?></p>
                    <p><b>Remarks: </b><?php echo $location->remarks ?></p>
                </div>
                <div class="col-lg-6">
                    <table id="datatable" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Day</th>
                            <th>Status</th>
                            <th>From Hour</th>
                            <th>To Hour</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php $i=1; foreach($locationSchedule as $schedule): ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $schedule->day; ?></td>
                                    <td><?php echo $schedule->status; ?></td>
                                    <td><?php if($schedule->status=="Open"): echo $schedule->fromHour; endif;?></td>
                                    <td><?php if($schedule->status=="Open"): echo $schedule->toHour; endif;?></td>
                                    <?php  ?>
                                </tr>
                            <?php $i++; endforeach; ?>
                        </tfoot>
                    </table>
                    <?php echo $str ?>
                    <?php echo $status1 ?>
                </div>
            </div>        
        </div>
    </div>
</div>

<?php $this->load->view('dashboard/common/footer.php')?>