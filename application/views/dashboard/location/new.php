<?php $this->load->view('dashboard/common/header.php')?>
<?php $this->load->view('dashboard/common/sidebar.php')?>

<div class="content-wrapper p-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Add new location</h3>
        </div>
        <div class="card-body">            
            <form action="<?php echo base_url('admin/location/');echo $action; ?>" method="POST" id="locationForm" >
                <div class="row">
                    <div class="col-lg-12">
                        <label>Location Name</label>
                        <input class="form-control" type="text" name="name" value="<?php echo $location->name ?>" placeholder="Location Name" required/>
                        <?php if($action=="update"):?><input type="hidden" name="id" value="<?php echo $location->id ?>"/><?php endif; ?>
                    </div>
                    <div class="col-lg-6">
                        <label>Telephone No</label>
                        <input class="form-control" type="text" name="telephone" value="<?php echo $location->telephone ?>" placeholder="Telephone No" required/>
                    </div>
                    <div class="col-lg-6">
                        <label>Fax No</label>
                        <input class="form-control" type="text" name="fax" value="<?php echo $location->fax ?>" placeholder="Fax No" required/>
                    </div>
                    <div class="col-lg-6">
                        <label>Address</label>
                        <textarea class="form-control" name="address" required><?php echo $location->address ?></textarea>
                    </div>
                    <div class="col-lg-6">
                        <label>Remarks</label>
                        <textarea class="form-control" name="remarks"><?php echo $location->remarks ?></textarea>
                    </div>
                    <div class="col-lg-6">
                        <label>Latitude</label>
                        <input class="form-control" type="text" name="latitude" value="<?php echo $location->latitude ?>" placeholder="Latitude" required/>
                    </div>
                    <div class="col-lg-6">
                        <label>Longitude</label>
                        <input class="form-control" type="text" name="longitude" value="<?php echo $location->longitude ?>" placeholder="Longitude" required/>
                    </div>
                    <div class="col-lg-4">
                        <label>Highlights</label>
                        <br />
                        <input class="" type="checkbox" name="afterHour" <?php if ($location->afterHour=="on"): ?>checked<?php endif; ?> /> After Hour
                        <input class="ml-5" type="checkbox" name="saturdayHighlight" <?php if ($location->saturday=="on"): ?>checked<?php endif; ?> /> Saturday
                        <input class="ml-5" type="checkbox" name="sundayHighlight" <?php if ($location->sunday=="on"): ?>checked<?php endif; ?> /> Sunday
                    </div>

                    <hr class="w-100"/> 

                    <div class="col-lg-12"><h4>Opening Hours</h4></div>
                    
                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="monday" Value="Monday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        
                        <select name="mondayStatus" class="form-control" id="mondayStatus" required>
                            <?php if($locationSchedule[0]->status): ?>
                                <option selected><?php echo $locationSchedule[0]->status ?></option>
                            <?php else: ?>
                                <option value="" disabled="disabled" selected="selected">Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="mondayFromHourBox" <?php if($locationSchedule[0]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="mondayFromHour" value="<?php echo $locationSchedule[0]->fromHour ?>" id="mondayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="mondayToHourBox" <?php if($locationSchedule[0]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="mondayToHour" value="<?php echo $locationSchedule[0]->toHour ?>" id="mondayToHour"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="tuesday" Value="Tuesday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        <select name="tuesdayStatus" class="form-control" id="tuesdayStatus" required>
                            <?php if($locationSchedule[1]->status): ?>
                                <option selected><?php echo $locationSchedule[1]->status ?></option>
                            <?php else: ?>
                                <option disabled selected>Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="tuesdayFromHourBox" <?php if($locationSchedule[1]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="tuesdayFromHour" value="<?php echo $locationSchedule[1]->fromHour ?>" id="tuesdayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="tuesdayToHourBox" <?php if($locationSchedule[1]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="tuesdayToHour" value="<?php echo $locationSchedule[1]->toHour ?>" id="tuesdayToHour"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="wednesday" Value="Wednesday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        <select name="wednesdayStatus" class="form-control" id="wednesdayStatus" required>
                            <?php if($locationSchedule[2]->status): ?>
                                <option selected><?php echo $locationSchedule[2]->status ?></option>
                            <?php else: ?>
                                <option disabled selected>Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="wednesdayFromHourBox" <?php if($locationSchedule[2]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="wednesdayFromHour" value="<?php echo $locationSchedule[2]->fromHour ?>" id="wednesdayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="wednesdayToHourBox" <?php if($locationSchedule[2]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="wednesdayToHour" value="<?php echo $locationSchedule[2]->toHour ?>" id="wednesdayToHour"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="thursday" Value="Thursday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        <select name="thursdayStatus" class="form-control" id="thursdayStatus" required>
                            <?php if($locationSchedule[3]->status): ?>
                                <option selected><?php echo $locationSchedule[3]->status ?></option>
                            <?php else: ?>
                                <option disabled selected>Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="thursdayFromHourBox" <?php if($locationSchedule[3]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="thursdayFromHour" value="<?php echo $locationSchedule[3]->fromHour ?>" id="thursdayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="thursdayToHourBox" <?php if($locationSchedule[3]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="thursdayToHour" value="<?php echo $locationSchedule[3]->toHour ?>" id="thursdayToHour"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="friday" Value="Friday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        <select name="fridayStatus" class="form-control" id="fridayStatus" required>
                            <?php if($locationSchedule[4]->status): ?>
                                <option selected><?php echo $locationSchedule[4]->status ?></option>
                            <?php else: ?>
                                <option disabled selected>Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="fridayFromHourBox" <?php if($locationSchedule[4]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="fridayFromHour" value="<?php echo $locationSchedule[4]->fromHour ?>" id="fridayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="fridayToHourBox" <?php if($locationSchedule[4]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="fridayToHour" value="<?php echo $locationSchedule[4]->toHour ?>" id="fridayToHour"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="saturday" Value="Saturday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        <select name="saturdayStatus" class="form-control" id="saturdayStatus" required>
                            <?php if($locationSchedule[5]->status): ?>
                                <option selected><?php echo $locationSchedule[5]->status ?></option>
                            <?php else: ?>
                                <option disabled selected>Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="saturdayFromHourBox" <?php if($locationSchedule[5]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="saturdayFromHour" value="<?php echo $locationSchedule[5]->fromHour ?>" id="saturdayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="saturdayToHourBox" <?php if($locationSchedule[5]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="saturdayToHour" value="<?php echo $locationSchedule[5]->toHour ?>" id="saturdayToHour"/>
                    </div>

                    <div class="col-lg-3">
                        <label>Day</label>
                        <input class="form-control" type="text" name="sunday" Value="Sunday" readonly/>
                    </div>
                    <div class="col-lg-3">
                        <label>Status</label>
                        <select name="sundayStatus" class="form-control" id="sundayStatus" required>
                            <?php if($locationSchedule[6]->status): ?>
                                <option selected><?php echo $locationSchedule[6]->status ?></option>
                            <?php else: ?>
                                <option disabled selected>Select Status</option>
                            <?php endif; ?>
                            <option>Open</option>
                            <option>Close</option>
                        </select>
                    </div>
                    <div class="col-lg-3" id="sundayFromHourBox" <?php if($locationSchedule[6]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>From Hour</label>
                        <input class="form-control" type="time" name="sundayFromHour" value="<?php echo $locationSchedule[6]->fromHour ?>" id="sundayFromHour"/>
                    </div>
                    <div class="col-lg-3" id="sundayToHourBox" <?php if($locationSchedule[6]->status=="Close"): ?>style="visibility: hidden;"<?php endif; ?>>
                        <label>To Hour</label>
                        <input class="form-control" type="time" name="sundayToHour" value="<?php echo $locationSchedule[6]->toHour ?>" id="sundayToHour" />
                    </div>

                    <div class="col-lg-2 pt-3">
                        <input class="btn btn-block btn-success" type="submit" value="Save"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>
$("#mondayStatus").change(function(){
    if ($('#mondayStatus').val() == 'Open') {
        $("#mondayFromHour").attr('required', true);
        $("#mondayToHour").attr('required', true);
        $("#mondayFromHourBox").css('visibility', 'visible');
        $("#mondayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#mondayFromHour").attr('required', false);
        $("#mondayToHour").attr('required', false);
        $("#mondayFromHourBox").css('visibility', 'hidden');
        $("#mondayToHourBox").css('visibility', 'hidden');
    }
}); 

$("#tuesdayStatus").change(function(){
    if ($('#tuesdayStatus').val() == 'Open') {
        $("#tuesdayFromHour").attr('required', true);
        $("#tuesdayToHour").attr('required', true);
        $("#tuesdayFromHourBox").css('visibility', 'visible');
        $("#tuesdayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#tuesdayFromHour").attr('required', false);
        $("#tuesdayToHour").attr('required', false);
        $("#tuesdayFromHourBox").css('visibility', 'hidden');
        $("#tuesdayToHourBox").css('visibility', 'hidden');
    }
}); 

$("#wednesdayStatus").change(function(){
    if ($('#wednesdayStatus').val() == 'Open') {
        $("#wednesdayFromHour").attr('required', true);
        $("#wednesdayToHour").attr('required', true);
        $("#wednesdayFromHourBox").css('visibility', 'visible');
        $("#wednesdayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#wednesdayFromHour").attr('required', false);
        $("#wednesdayToHour").attr('required', false);
        $("#wednesdayFromHourBox").css('visibility', 'hidden');
        $("#wednesdayToHourBox").css('visibility', 'hidden');
    }
}); 

$("#thursdayStatus").change(function(){
    if ($('#thursdayStatus').val() == 'Open') {
        $("#thursdayFromHour").attr('required', true);
        $("#thursdayToHour").attr('required', true);
        $("#thursdayFromHourBox").css('visibility', 'visible');
        $("#thursdayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#thursdayFromHour").attr('required', false);
        $("#thursdayToHour").attr('required', false);
        $("#thursdayFromHourBox").css('visibility', 'hidden');
        $("#thursdayToHourBox").css('visibility', 'hidden');
    }
}); 

$("#fridayStatus").change(function(){
    if ($('#fridayStatus').val() == 'Open') {
        $("#fridayFromHour").attr('required', true);
        $("#fridayToHour").attr('required', true);
        $("#fridayFromHourBox").css('visibility', 'visible');
        $("#fridayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#fridayFromHour").attr('required', false);
        $("#fridayToHour").attr('required', false);
        $("#fridayFromHourBox").css('visibility', 'hidden');
        $("#fridayToHourBox").css('visibility', 'hidden');
    }
}); 

$("#saturdayStatus").change(function(){
    if ($('#saturdayStatus').val() == 'Open') {
        $("#saturdayFromHour").attr('required', true);
        $("#saturdayToHour").attr('required', true);
        $("#saturdayFromHourBox").css('visibility', 'visible');
        $("#saturdayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#saturdayFromHour").attr('required', false);
        $("#saturdayToHour").attr('required', false);
        $("#saturdayFromHourBox").css('visibility', 'hidden');
        $("#saturdayToHourBox").css('visibility', 'hidden');
    }
}); 

$("#sundayStatus").change(function(){
    if ($('#sundayStatus').val() == 'Open') {
        $("#sundayFromHour").attr('required', true);
        $("#sundayToHour").attr('required', true);
        $("#sundayFromHourBox").css('visibility', 'visible');
        $("#sundayToHourBox").css('visibility', 'visible');
    }
    else{
        $("#sundayFromHour").attr('required', false);
        $("#sundayToHour").attr('required', false);
        $("#sundayFromHourBox").css('visibility', 'hidden');
        $("#sundayToHourBox").css('visibility', 'hidden');
    }
}); 
    
$(document).ready(function() {
    $( "#locationForm" ).submit(function(e) {
        var formData = $("#locationForm").serialize();
        $.ajax({
            url: '<?php echo base_url('admin/location/update') ?>', 
            type:'GET',
            data: formData,
            success: function(results){ 
                console.log(results);
            },
        
            error:function(){
                console.log('error');
            }
        });

        $.ajax({
            url: 'https://www.ktdoctor.com/admin/location/update', 
            type:'GET',
            data: formData,
            dataType: "jsonp",
            success: function(results){ 
                console.log(results);
                window.location="<?php echo base_url('admin/location/view/').$location->id; ?>";
            },
        
            error:function(){
                console.log('error');
                window.location="<?php echo base_url('admin/location/view/').$location->id; ?>";
            }
        });
        return false;
    });
});
</script>

<?php $this->load->view('dashboard/common/footer.php')?>