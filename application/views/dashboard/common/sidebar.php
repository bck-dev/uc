<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
  </nav>


  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a style="color: white" class="brand-link">
           <img src="<?php echo base_url('assets/adminLte/dist/img/icon-UC-dark-300x300.png')?>"
            alt="AdminLTE Logo"
            class="brand-image img-circle elevation-3"
            style="opacity: .8">
      <span class="brand-text font-weight-light">Urgent Care</span>
    </a>
    
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
                with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url('admin/user') ?>" class="nav-link">
                  <i class="fas fa-user-friends nav-icon"></i>
                  <p>Users</p>
                </a>
              </li>
               <li class="nav-item">
                <a href="<?php echo base_url('admin/location') ?>" class="nav-link">
                  <i class="fas fa-map-marker-alt nav-icon"></i>
                  <p>Locations</p>
                </a>
              </li>
             
              <li class="nav-item">
                <a href="<?php echo base_url('admin/accordion') ?>" class="nav-link">
                  <i class="fas fa-align-justify nav-icon"></i>
                  <p>Accordion</p>
                </a>
              </li>
               
              <li class="nav-item">
                <a href="<?php echo base_url('admin/featurebox') ?>" class="nav-link">
                  <i class="fas fa-box nav-icon"></i>
                  <p>Feature Box</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="<?php echo base_url('admin/banner') ?>" class="nav-link">
                  <i class="fas fa-image nav-icon"></i>
                  <p>Banner</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('admin/descriptionbox') ?>" class="nav-link">
                  <i class="fab fa-buromobelexperte  nav-icon"></i>
                  <p>Description Box</p>
                </a>
              </li>
              <!-- 
              <li class="nav-item">
                <a href="<?php echo base_url('admin/logoslider') ?>" class="nav-link">
                  <i class="fas fa-angle-double-right  nav-icon"></i>
                  <p>Logo Slider</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('admin/textbox') ?>" class="nav-link">
                  <i class="fas fa-text-height nav-icon"></i>
                  <p>Text Box</p>
                </a>
              </li> -->
              <!-- <li class="nav-item">
                <a href="<?php //echo base_url('admin/highlightedbox') ?>" class="nav-link">
                  <i class="fas fa-archive  nav-icon"></i>
                  <p>Highlighted Box</p>
                </a>
              </li> -->
               <li class="nav-item">
                <a href="<?php echo base_url('admin/doctor') ?>" class="nav-link">
                  <i class="fas fa-user-md nav-icon"></i>
                  <p>Doctors</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('admin/pointers') ?>" class="nav-link">
                  <i class="fas fa-hand-point-right nav-icon"></i>
                  <p>Pointers</p>
                </a>
              </li> 
              <li class="nav-item">
                <a href="<?php echo base_url('admin/pages') ?>" class="nav-link">
                  <i class="fas fa-file nav-icon"></i>
                  <p>Pages</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url('admin/comparison') ?>" class="nav-link">
                  <i class="fas fa-columns nav-icon"></i>
                  <p>Comparison</p>
                </a>
              </li> 
             <!-- <li class="nav-item">
                <a href="<?php echo base_url('admin/reviews') ?>" class="nav-link">
                   <i class="fas fa-comments nav-icon"></i>
                  <p>Reviews</p>
                </a>
              </li> -->
              <li class="nav-item">
                <a href="<?php echo base_url('logout') ?>" class="nav-link">
                  <i class="fas fa-sign-out-alt nav-icon"></i>
                  <p>Log out</p>
                </a>
              </li>
            </ul>
          </li>
          
      </nav>
      <!-- /.sidebar-menu -->
    </div>
  </aside>