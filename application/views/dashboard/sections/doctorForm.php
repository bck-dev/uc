<?php $this->load->view('dashboard/common/header.php')?>

<?php $this->load->view('dashboard/common/sidebar.php')?>

<div class="row">
    <div class="col-12" .mt-2>
      <div class="content-wrapper p-5">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
           <!-- left column -->
            <div class="col-lg-12">
             <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title">Doctor Form</h3>
                </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('')?>admin/doctor/<?php echo $action ?>/<?php echo $updateData->id ?>" method="POST" name="doctorForm" enctype='multipart/form-data'>

               <div class="card-body">

                <div class="form-group">
                    <label for="inputFile">Doctor Image</label>
                    <?php  if($action == 'update') { ?>
                      <div class="col-sm-12">
                        <div class="form-group">
                          <img src="<?php echo base_url(); ?>/uploads/<?php echo $updateData->image;?>" width="250"/>
                        </div>
                      </div>
                    <?php }?>  
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" class="custom-file-input" name="doctorImage" size="200"  <?php  if($action != 'update'): echo 'required'; endif;?> >
                        <label class="custom-file-label" for="inputFile">Choose Image</label>
                      </div>
                    </div>
                  </div>

                   <div class="form-group">
                          <label for="inputTitle">Name</label>
                          <input type="text" class="form-control" placeholder="Enter Name" name='name' value="<?php echo $updateData->name; ?>" required>
                    </div>
                    <div class="form-group">
                          <label for="inputTitle">Qualificaion</label>
                          <input type="text" class="form-control" placeholder="Enter Qualificaion" name='qualification' value="<?php echo $updateData->qualification; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="inputTitle">Certification</label>
                          <input type="text" class="form-control" placeholder="Enter Certificaion" name='certification' value="<?php echo $updateData->certification; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="inputTitle">Specialty</label>
                          <input type="text" class="form-control" placeholder="Enter Specialty" name='specialty' value="<?php echo $updateData->specialty; ?>" required>
                    </div>
                    <div class="form-group">
                          <label for="inputTitle">Medical School</label>
                          <input type="text" class="form-control" placeholder="Enter Medical School" name='medicalSchool' value="<?php echo $updateData->medicalSchool; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="inputTitle">Hospital Affiliation</label>
                          <input type="text" class="form-control" placeholder="Enter Hospital Affiliation" name='hospitalAffiliation' value="<?php echo $updateData->hospitalAffiliation; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="inputTitle">Residency & Fellowship</label>
                          <input type="text" class="form-control" placeholder="Enter Residency and Fellowship" name='residency_fellowship' value="<?php echo $updateData->residency_fellowship; ?>" >
                    </div>
                    <div class="form-group">
                          <label for="inputBio">Biography</label>
                          <textarea class="form-control textarea" rows="10" id="inputBio" placeholder="Enter Biography" name='biography' required><?php echo $updateData->biography; ?></textarea>
                        </div>

                      <div class="form-group">
                        <div class="col-sm-12">
                          <label for="selectPage">Locations</label>                  
                          <div class="select2-purple">
                            <select class="select2" name='locations[]' multiple="multiple" style="width:100%;" data-placeholder="Select locations" >
                              <?php if($action=="update"): ?>
                                <?php foreach($selectedLocations as $option): ?>
                                  <option value="<?php echo $option->locationId; ?>" selected=""><?php echo $option->name; ?></option>
                                <?php endforeach; ?>
                                <?php foreach($nonSelectedLocations as $option): ?>
                                  <option value="<?php echo $option->id; ?>"><?php echo $option->name; ?></option>
                                <?php endforeach; ?>  
                              <?php else: ?>      
                                <?php foreach($locations as $option): ?>
                                  <option value="<?php echo $option->id; ?>"><?php echo $option->name; ?></option>
                                <?php endforeach; ?>   
                              <?php endif; ?>     
                            </select> 
                          </div>
                        </div>                         
                     </div>  
                                       
              <?php  if($action == 'update') { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>
              <?php }else { ?>      
              <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                </div>
              <?php } ?>                         
                  </form>
             </div>
          </div>           <!-- /.card -->  <!-- Form Element sizes -->
      </section>
      
    </div>
 </div>

 <?php $this->load->view('dashboard/common/footer.php')?>

