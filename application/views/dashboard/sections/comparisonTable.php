<div class="col-6 p-3">

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Section</th>
              <th>Category</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($comparisonData as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->section; ?></td>
                      <td><?php echo $dataRow->category; ?></td>
                       <td>
                          <button class="btn btn-xs btn-info" onclick="getData(this);" id="btn" data-toggle="modal" data-target="#exampleModalScrollable" value="<?php echo $dataRow->id ?>">View More</button>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/comparison/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/comparison/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>
</div>
</div>

<?php $this->load->view('dashboard/sections/comparisonModal.php')?>
