<div class="col-6 p-3">
<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Name</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($pages as $dataRow): ?>
                  <tr>
                      <td><?php echo $dataRow->name; ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/pages/loadupdate/');?><?php echo $dataRow->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/pages/delete/');?><?php echo $dataRow->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>
</div>
</div>

