<div class="row">
  <div class="col-6" .mt-2>
    <div class="content-wrapper p-3">
      <?php $this->load->view('dashboard/sections/error') ?>
      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <!-- left column -->
            <div class="col-lg-12">
            <!-- general form elements -->
            <div class="card card-primary">
            <div class="card-header">
             <h3 class="card-title">Pointers Form</h3>
            </div>         
            <!-- form start -->
            <form action="<?php echo base_url('admin/pointers/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="pointerForm">
              <div class="card-body">
                <div class="form-group">

                <div class="form-group">
                      <div class="col-sm-12">
                          <label for="section">Section</label>                  
                              <select class="custom-select" name='section' id='section' required>
                                <?php if($action=="update"): ?>
                                  <option value="<?php echo $updateData->section; ?>"><?php echo $updateData->section; ?></option>
                                <?php else: ?>
                                  <option value="" disabled="disabled" selected="selected">Select Section</option>
                                <?php endif; ?>
                                  <option value="Urgent Care">Urgent Care</option>
                                  <option value="Emergency Room">Emergency Room</option>
                                  <!-- <option value="Why Choose Us">Why Choose Us</option> -->
                                 </select> 
                          </div>                         
                </div>

                  <label for="point">Pointer</label>
                  <input type="text" class="form-control" placeholder="Enter Pointer" name='point' id='point' value="<?php echo $updateData->point; ?>" >
                </div>  
              <!-- /.card-body -->       
              <?php  if($action == 'update') { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                </div>
                <?php }else { ?>
                <div class="card-footer">
                  <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                </div>
              <?php } ?>                
            </form>
          </div>
        </div>        
      </section>
    </div>
  </div>

