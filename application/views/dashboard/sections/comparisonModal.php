<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Comparison Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaltext">
                <div class="row">
                    <div class="col-md-4"><b>Section</b></div>
                    <div class="col-md-8 ml-auto"><p id="section"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Category</b></div>
                    <div class="col-md-8 ml-auto"><p id="category"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Urgent Care</b></div>
                    <div class="col-md-8 ml-auto"><p id="urgentCare"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Emergency Room</b></div>
                    <div class="col-md-8 ml-auto"><p id="emergencyRoom"></p></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 

<script>
    function getData(id){
        var boxId = $(id).val();           
        $.ajax({
            url: '<?php echo base_url('api/comparison') ?>', 
            type:'post',
            data: {id: boxId},
            dataType: 'json',
            success: function(results){ 
                $('#section').text(results['section']);
                $('#category').text(results['category']);
                $('#urgentCare').text(results['urgentCare']);
                $('#emergencyRoom').text(results['emergencyRoom']);
            },
        
            error:function(){
                console.log('error');
            }
        });
    }
</script>

     