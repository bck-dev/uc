<div class="row">
    <div class="col-6" .mt-2>
      <div class="content-wrapper p-3">
 
      <?php $this->load->view('dashboard/sections/error') ?>
 
     <!-- Main content -->
     <section class="content">
       <div class="container-fluid">
         <div class="row">
           <!-- left column -->
           <div class="col-lg-12">
             <!-- general form elements -->
             <div class="card card-primary">
               <div class="card-header">
                 <h3 class="card-title">Comparison Form</h3>
               </div>
               <!-- /.card-header -->
               <!-- form start -->
               <form action="<?php echo base_url('admin/comparison/'.$action.'/');?><?php echo $updateData->id ?>" method="POST" name="comparisonForm" >
                 
                    <div class="card-body">
                        
                        <div class="form-group">
                        <div class="col-sm-12">
                            <label for="selectSection">Section</label>                  
                                <select class="custom-select" name='section' id='selectSection' required>
                                    <?php if($action=="update"): ?>
                                        <option value="<?php echo $updateData->section; ?>"><?php echo $updateData->section; ?></option>
                                    <?php else: ?>
                                        <option value="" disabled="disabled" selected="selected">Select Section</option>
                                    <?php endif; ?>
                                        <option value="Home Section">Home Section</option>
                                </select> 
                            </div>                         
                        </div>  

                        <div class="form-group">
                            <label>Category</label>
                            <input type="text" class="form-control" placeholder="Enter Category" name='category' value="<?php echo $updateData->category; ?>" required>
                        </div>

                        <div class="form-group">
                            <label>Urgent Care</label>
                            <input type="text" class="form-control" placeholder="Enter Urgent Care points" name='urgentCare' value="<?php echo $updateData->urgentCare; ?>" required>
                        </div>

                        <div class="form-group">
                            <label>Emergency Room</label>
                            <input type="text" class="form-control" placeholder="Enter Emergency Room points" name='emergencyRoom' value="<?php echo $updateData->emergencyRoom; ?>" required>
                        </div>

                    </div>
                                                                
                    <?php  if($action == 'update') { ?>
                        <div class="card-footer">
                            <button type="submit"  class="btn btn-primary btn-lg btn-block" name="update">Update</button>
                        </div>
                    <?php }else { ?>      
                        <div class="card-footer">
                            <button type="submit"  class="btn btn-primary btn-lg btn-block" name="submit">Add</button>
                        </div>
                    <?php } ?> 

                </form>
             </div>
          </div>           
      </section>
    </div>
 </div>
