<div class="col-6 p-3">

<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Full Name</th>
              <th>Email</th>
              <th>Phone</th>              
              <th>Action</th>
          </tr>
          </thead>
          <tbody>
              <?php foreach($users as $user): ?>
                  <tr>
                      <td><?php echo $user->fullName; ?></td>
                      <td><?php echo $user->email; ?></td>
                      <td><?php echo $user->phone ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/user/loadupdate/');?><?php echo $user->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/user/delete/');?><?php echo $user->id ?>">Delete</a>
                      </td>
                  </tr>
              <?php endforeach; ?>
          </tfoot>
          </table>

</div>
</div>
