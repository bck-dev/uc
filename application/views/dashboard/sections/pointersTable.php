<div class="col-6 p-3">
<table id="datatable" class="table table-bordered table-striped">
          <thead>
          <tr>
              <th>Section</th>
              <th>Point</th>
              <th>Action</th>             
          </tr>
          </thead>
          <tbody>
              <?php foreach($pointersData as $pointer): ?>
                  <tr>
                      <td><?php echo $pointer->section; ?></td>
                      <td><?php echo $pointer->point; ?></td>
                       <td>
                          <a class="btn btn-xs btn-warning" href="<?php echo base_url('admin/pointers/loadupdate/');?><?php echo $pointer->id ?>">Edit</a>
                          <a class="btn btn-xs btn-danger" href="<?php echo base_url('admin/pointers/delete/');?><?php echo $pointer->id ?>">Delete</a>
                      </td>
                  </tr>
                
              <?php endforeach; ?>
          </tfoot>
          </table>
</div>
</div>

