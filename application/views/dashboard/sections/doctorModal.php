<div class="modal fade" id="exampleModalScrollable" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
        <div class="modal-content ">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalScrollableTitle">Doctor Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="modaltext">
                <div class="row">
                    <div class="col-md-4"><b>Doctor Name</b></div>
                    <div class="col-md-8 ml-auto"><p id="name"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Specialty</b></div>
                    <div class="col-md-8 ml-auto"><p id="specialty"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b> School</b></div>
                    <div class="col-md-8 ml-auto"><p id="medicalSchool"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Residency & Fellowship</b></div>
                    <div class="col-md-8 ml-auto"><p id="residency_fellowship"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Hospital Affiliation</b></div>
                    <div class="col-md-8 ml-auto"><p id="hospitalAffiliation"></p></div>
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Biography</b></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><p id="biography" ></p></div>                           
                </div>
                <div class="row">
                    <div class="col-md-4"><b>Locations</b></div>
                </div>
                <div class="row">
                    <div class="col-md-12"><p id="locations" ></p></div>                           
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div> 



<script>
    function getData(id){
        var doctorId = $(id).val();
            
        $.ajax({
            //url:'http://localhost/ktd/api/doctor',           
            url: '<?php echo base_url('api/doctor') ?>', 
            type:'post',
            data: {id: doctorId},
            dataType: 'json',
            success: function(results){ 

                $('#name').text(results['name']);
                $('#specialty').text(results['specialty']);
                $('#medicalSchool').text(results['medicalSchool']);
                $('#residency_fellowship').text(results['residency_fellowship']);
                $('#hospitalAffiliation').text(results['hospitalAffiliation']);
                $('#biography').html(results['biography']);   
                
                var locations =""; 
                jQuery.each(results['locations'], function( key, val ) {
                    locations = locations + "<p>" + val['name'] + "</p>";
                });
                $('#locations').html(locations);
            },
        
            error:function(){
                console.log('error');
            }
        });
    }
</script>

     