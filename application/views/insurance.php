<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/breadcrumb'); ?>

<section id="tabs-1" class="wide-100 tabs-section division">
    <div class="container">	
        <div class="row">
            <div class="col-md-12">
                <h4 class="h4-md steelblue-color text-center">Check Eligibility</h4>
                <p class="mb-4 text-center">Choose Your Insurance Type</p>
            
            
                <div id="tabs-nav" class="list-group text-center">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item icon-xs">
                            <a class="nav-link active" id="tab1-list" data-toggle="pill" href="#tab-1" role="tab" aria-controls="tab-1" aria-selected="true">
                                Medi_cal HMO/IPA
                            </a>
                        </li>
                        <li class="nav-item icon-xs">
                            <a class="nav-link" id="tab2-list" data-toggle="pill" href="#tab-2" role="tab" aria-controls="tab-2" aria-selected="false">
                                Commercial HMO/IPA
                            </a>
                        </li>
                        <li class="nav-item icon-xs">
                            <a class="nav-link" id="tab3-list" data-toggle="pill" href="#tab-3" role="tab" aria-controls="tab-3" aria-selected="false">
                                PPO/POS
                            </a>
                        </li>
                        <li class="nav-item icon-xs">
                            <a class="nav-link" id="tab4-list" data-toggle="pill" href="#tab-4" role="tab" aria-controls="tab-4" aria-selected="false">
                                Regal, Lakeside & Healthcare partners
                            </a>
                        </li>                        
                        <li class="nav-item icon-xs">
                            <a class="nav-link" id="tab4-list" data-toggle="pill" href="#tab-5" role="tab" aria-controls="tab-4" aria-selected="false">
                                Not Sure
                            </a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="tab-1" role="tabpanel" aria-labelledby="tab1-list">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-8">
                                <div class="txt-block pc-30">
                                    <h3 class="h3-md steelblue-color">Medi_cal HMO/IPA</h3>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Serendib-Molina IPA + our physicians (Preferred)</p>							
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">La care Exceptional Care Serendib IPA + our physicians (Preferred)</p>				
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Healthnet Exceptional care – Serendib + our physicians (Preferred)</p>				
                                            </div>
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Anthem Blue cross- Exceptional care – Serendib + our physicians (Preferred)</p>							
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                	
                            </div>
                            
                            <div class="col-lg-4 light_blue_background p-4">
                                <p class="font-b2 white b600">
                                    If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians.
                                    <br /><br />Or contact us on <a href="tel:8183615437" class="white">(818) 361-5437</a> and have it changed over the phone.                
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab-2" role="tabpanel" aria-labelledby="tab1-list">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-8">
                                <div class="txt-block pc-30">
                                    <h3 class="h3-md steelblue-color">Commercial HMO/IPA</h3>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Exceptional care Blue Sheild commercial(Preferred)</p>							
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Exceptional care Blue Sheild Plus (Preferred)</p>				
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Exceptional care Cigna Commercial (Preferred)</p>				
                                            </div>
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Exceptional care Healthnet Salud con Commercial (Preferred)</p>							
                                            </div>
                                        </div>
                                    </div>	
                                </div>
                                
                            </div>
                            <div class="col-lg-4 light_blue_background p-4">
                                <p class="font-b2 white b600">
                                    If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians.
                                    <br /><br />Or contact us on <a href="tel:8183615437" class="white">(818) 361-5437</a> and have it changed over the phone.                
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab-3" role="tabpanel" aria-labelledby="tab1-list">
                        <div class="row d-flex align-items-center">
                        <div class="col-lg-8">
                                <div class="txt-block pc-30">
                                    <h3 class="h3-md steelblue-color">PPO/POS insurance</h3>
                                    <div class="row">
                                        <div class="col-xl-6">
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Aetna PPO/POS and Managed Choice</p>							
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Anthem Blue Cross of CA PPO/POS</p>				
                                            </div>
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Blue Shield of CA PPO/POS</p>							
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">BlueCard Plans/POS</p>				
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Health Net PPO/POS</p>				
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Cigna PPO Global and Open Access Plus and POS</p>				
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">United Health Care PPO/POS</p>				
                                            </div>
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">OSCAR PPO</p>							
                                            </div>
                                            <div class="box-list">							
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Keenan-Prime Healthcare</p>							
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">Tri Care</p>				
                                            </div>
                                            <div class="box-list">	
                                                <div class="box-list-icon blue-color"><i class="fas fa-angle-double-right"></i></div>
                                                <p class="p-sm">And most all PPO insurance programs</p>				
                                            </div>
                                        </div>
                                    </div>	
                                </div>
                                
                            </div>
                            <div class="col-lg-4 light_blue_background p-4">
                                <p class="font-b2 white b600">
                                    If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians.
                                    <br /><br />Or contact us on <a href="tel:8183615437" class="white">(818) 361-5437</a> and have it changed over the phone.                
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab-4" role="tabpanel" aria-labelledby="tab1-list">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-8">
                                <div class="txt-block pc-30">
                                    <h3 class="h3-md steelblue-color">Regal, Lakeside & Healthcare Partners</h3>
                                    <p class="mb-30">We Welcome all Commercial and Medi-cal HMO Members with Regal, Lakeside, Health Care Partners and Eastland Members at Kids and Teens
                                    </p>
                                </div>	
                            </div>
                            <div class="col-lg-4 light_blue_background p-4">
                                <p class="font-b2 white b600">
                                    If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians.
                                    <br /><br />Or contact us on <a href="tel:8183615437" class="white">(818) 361-5437</a> and have it changed over the phone.                
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="tab-5" role="tabpanel" aria-labelledby="tab1-list">
                        <div class="row d-flex align-items-center">
                            <div class="col-lg-8">
                                <div class="txt-block pc-30">
                                    <h3 class="h3-md steelblue-color">Insurance Instructions</h3>
                                    <p class="mb-30">We accept most insurance, including PPO, HMO, MediCal and CHDP. So, if you’re 
                                        not sure what type of insurance you have, or didn’t see your health plan in our list of options, 
                                        chances are we can still help.       
                                    </p>
                                </div>	
                            </div>
                            <div class="col-lg-4 light_blue_background p-4">
                                <p class="font-b2 white b600">
                                    If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians.
                                    <br /><br />Or contact us on <a href="tel:8183615437" class="white">(818) 361-5437</a> and have it changed over the phone.                
                                </p>
                            </div>
                        </div>
                    </div>


                </div>	<!-- END TABS CONTENT -->


            </div>	
        </div>     <!-- End row -->	
    </div>     <!-- End container -->	
</section>	<!-- END TABS-1 -->



<div class="container">
    <div class="row">
        <div class="col-lg-12 pr-5 pl-0 py-6" id="billing_policy">
            <h3 class="h3-md steelblue-color">Billing Policy</h3>
            <p class="font18 mb-4">Our billing policy has recently changed. Please review prior to your appointment.</p>
            <?php $this->load->view('components/sections/accordion'); ?>                  
        </div>
    </div>
</div>

<?php $this->load->view('components/common/footer'); ?>