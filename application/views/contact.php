<?php $this->load->view('components/common/header'); ?>
<?php $this->load->view('components/common/menuBar'); ?>
<?php $this->load->view('components/common/breadcrumb'); ?>

			<!-- CONTACTS-1
			============================================= -->
			<section id="contacts-1" class="wide-60 contacts-section division">				
				<div class="container">


					<!-- SECTION TITLE -->	
					<div class="row">	
						<div class="col-lg-10 offset-lg-1 section-title">	

							<!-- Title 	-->	
							<h3 class="h3-md steelblue-color">Your perfect choice for the best pediatricians</h3>	

							<!-- Text -->
							<p>Have a question? Want to book an appointment for your child? Give us a call or send an email to contact us. 
							</p>
								
						</div>
					</div>

						
					<div class="row">	
							<!-- CONTACTS INFO -->
						<div class="col-md-4">
							<!-- General Information -->
							<div class="contact-box mb-40">
								<h5 class="h5-sm steelblue-color">General Contact Information</h5>
								<p>504 S. Sierra Madre Blvd</p> 
								<p>Pasadena, CA 91107</p>
								<br>
								<p class="grey font15">
								Phone - <a href="tel:8183615437" class="blue-color">(818) 361-5437</a>
								</p>
								<br>
								<p>Text Us at (EN) <a href="sms:6262987121" class="blue-color">(626) 298-7121 </a></p>			
								<p>Text Us at (SP) <a href="sms:6262697744" class="blue-color">(626) 269-7744 </a></p>	
								<br>
								<p>Email: <a href="mailto:info@ktdoctor.com" class="blue-color">info@ktdoctor.com</a></p>
								</div>
						</div>	 

						<div class="col-md-4">
							<!-- Patient Experience -->
							<div class="contact-box mb-40">
								<h5 class="h5-sm steelblue-color">Urgent Care Information  </h5>
								<p>Call 911 if you or your child experiences a life threatening emergency.
									For non life threatening urgencies contact us on <a href="tel:8183615437"><b>(818) 361-5437</b></a> to reach our extended hour pediatric care. 
								</p>
							</div>
						</div>

						<div class="col-md-4">
							<!-- Working Hours -->
							<div class="contact-box mb-40">
								<h5 class="h5-sm steelblue-color">Extended-Hours Pediatric Care</h5>
								<p>Mon-Fri : 9am - 8pm</p> 
								<br>
								<p>Sat & Sun : 12pm - 8pm</p>
							</div>
						</div>	

					</div>	<!-- END CONTACTS INFO -->

				 	</div>	<!-- End row -->			  
 
				</div>	   <!-- End container -->		
			</section>	<!-- END CONTACTS-1 -->
			

<?php $this->load->view('components/common/footer'); ?>

		