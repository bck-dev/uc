-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 08:55 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `location_schedule`
--

CREATE TABLE `location_schedule` (
  `id` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `fromHour` time NOT NULL,
  `toHour` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_schedule`
--

INSERT INTO `location_schedule` (`id`, `locationId`, `day`, `status`, `fromHour`, `toHour`) VALUES
(5, 3, 'Monday', 'Open', '09:00:00', '18:00:00'),
(6, 3, 'Tuesday', 'Open', '21:00:00', '18:00:00'),
(7, 3, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(8, 3, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(9, 3, 'Friday', 'Open', '09:00:00', '18:00:00'),
(10, 3, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(11, 3, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(12, 4, 'Monday', 'Open', '08:30:00', '17:30:00'),
(13, 4, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(14, 5, 'Monday', 'Open', '08:30:00', '17:30:00'),
(15, 5, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(16, 5, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(17, 5, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(18, 5, 'Friday', 'Open', '09:00:00', '18:00:00'),
(19, 5, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(20, 5, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(21, 6, 'Monday', 'Open', '08:30:00', '17:30:00'),
(22, 6, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(23, 6, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(24, 6, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(25, 6, 'Friday', 'Open', '09:00:00', '18:00:00'),
(26, 6, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(27, 6, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(28, 7, 'Monday', 'Open', '08:30:00', '17:30:00'),
(29, 7, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(30, 7, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(31, 7, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(32, 7, 'Friday', 'Open', '09:00:00', '18:00:00'),
(33, 7, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(34, 7, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(35, 10, 'Monday', 'Open', '09:00:00', '18:00:00'),
(36, 10, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(37, 10, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(38, 10, 'Thursday', 'Open', '10:00:00', '17:00:00'),
(39, 10, 'Friday', 'Open', '09:00:00', '18:00:00'),
(40, 10, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(41, 10, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(42, 11, 'Monday', 'Open', '09:00:00', '18:00:00'),
(43, 11, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(44, 11, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(45, 11, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(46, 11, 'Friday', 'Open', '09:00:00', '18:00:00'),
(47, 11, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(48, 11, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(49, 12, 'Monday', 'Open', '09:00:00', '18:00:00'),
(50, 12, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(51, 12, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(52, 12, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(53, 12, 'Friday', 'Open', '09:00:00', '18:00:00'),
(54, 12, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(55, 12, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(91, 18, 'Monday', 'Open', '09:00:00', '20:00:00'),
(92, 18, 'Tuesday', 'Open', '09:00:00', '20:00:00'),
(93, 18, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(94, 18, 'Thursday', 'Open', '09:00:00', '20:00:00'),
(95, 18, 'Friday', 'Open', '09:00:00', '20:00:00'),
(96, 18, 'Saturday', 'Open', '12:00:00', '20:00:00'),
(97, 18, 'Sunday', 'Open', '12:00:00', '20:00:00'),
(98, 19, 'Monday', 'Open', '09:00:00', '20:00:00'),
(99, 19, 'Tuesday', 'Open', '09:00:00', '20:00:00'),
(100, 19, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(101, 19, 'Thursday', 'Open', '09:00:00', '20:00:00'),
(102, 19, 'Friday', 'Open', '09:00:00', '20:00:00'),
(103, 19, 'Saturday', 'Open', '12:00:00', '20:00:00'),
(104, 19, 'Sunday', 'Open', '12:00:00', '20:00:00'),
(112, 21, 'Monday', 'Open', '09:00:00', '18:00:00'),
(113, 21, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(114, 21, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(115, 21, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(116, 21, 'Friday', 'Open', '09:00:00', '18:00:00'),
(117, 21, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(118, 21, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(119, 22, 'Monday', 'Open', '09:00:00', '19:00:00'),
(120, 22, 'Tuesday', 'Open', '09:00:00', '19:00:00'),
(121, 22, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(122, 22, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(123, 22, 'Friday', 'Open', '09:00:00', '19:00:00'),
(124, 22, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(125, 22, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(126, 23, 'Monday', 'Open', '09:00:00', '20:00:00'),
(127, 23, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(128, 23, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(129, 23, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(130, 23, 'Friday', 'Open', '09:00:00', '18:00:00'),
(131, 23, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(132, 23, 'Sunday', 'Close', '00:00:00', '00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `location_schedule`
--
ALTER TABLE `location_schedule`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `location_schedule`
--
ALTER TABLE `location_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
