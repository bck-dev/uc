-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 13, 2020 at 11:58 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `description_box`
--

CREATE TABLE `description_box` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `button_text` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `style` varchar(10) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `description_box`
--

INSERT INTO `description_box` (`id`, `title`, `content`, `image`, `button_text`, `link`, `style`, `pageId`) VALUES
(20, 'Title 3', '<p>sdf</p>', '54a9d83a63eed64582840d454d782a26.jpg', 'Button 1', 'Link 3', 'Right', 6),
(21, 'sadas', '<p>axcasc</p>', '4545194116eeca4488d6e7e774f187d7.jpg', 'f', 'Link 1 ', 'Left', 2),
(22, 'asdas updated', '<p>dv</p>', '9889fdd9e833a1c79dac0fb32d263feb.jpg', 'dfv', 'dvd', 'Right', 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `description_box`
--
ALTER TABLE `description_box`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `description_box`
--
ALTER TABLE `description_box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
