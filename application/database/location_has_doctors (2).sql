-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 08:55 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `location_has_doctors`
--

CREATE TABLE `location_has_doctors` (
  `id` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `doctorId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `location_has_doctors`
--

INSERT INTO `location_has_doctors` (`id`, `locationId`, `doctorId`) VALUES
(70, 11, 34),
(71, 18, 34),
(72, 22, 34),
(73, 23, 34),
(102, 5, 26),
(103, 10, 26),
(104, 12, 26),
(105, 19, 26),
(107, 5, 28),
(108, 19, 28),
(115, 18, 31),
(116, 23, 31),
(121, 11, 33),
(122, 18, 33),
(123, 22, 33),
(124, 23, 33),
(125, 5, 35),
(126, 12, 35),
(127, 19, 35),
(128, 5, 236),
(129, 12, 236),
(130, 19, 236),
(131, 21, 236),
(132, 3, 237),
(133, 10, 237),
(134, 11, 237),
(135, 19, 237),
(136, 23, 237),
(141, 18, 240),
(142, 19, 240),
(143, 22, 240),
(147, 21, 242),
(148, 23, 242),
(149, 5, 243),
(150, 19, 243),
(151, 21, 243),
(152, 19, 244),
(153, 22, 244),
(157, 3, 238),
(158, 23, 238),
(162, 21, 27),
(163, 18, 29),
(164, 19, 29),
(165, 21, 29),
(166, 22, 29),
(167, 23, 29),
(168, 12, 30),
(169, 5, 241),
(170, 12, 241),
(171, 19, 241),
(172, 3, 239),
(173, 23, 239),
(174, 10, 245),
(175, 12, 245),
(176, 23, 245),
(179, 11, 24),
(180, 18, 25),
(181, 23, 25);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `location_has_doctors`
--
ALTER TABLE `location_has_doctors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `location_has_doctors`
--
ALTER TABLE `location_has_doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
