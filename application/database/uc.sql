-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 30, 2020 at 07:01 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `accordion`
--

CREATE TABLE `accordion` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `pageId` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accordion`
--

INSERT INTO `accordion` (`id`, `title`, `content`, `pageId`) VALUES
(41, 'Title 2 Updated', 'This is the content of title 2.', '1'),
(51, 'Title 3', '<p>Content of title 3</p>', '5');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL,
  `fullName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `fullName`, `password`, `email`, `phone`) VALUES
(1, 'Test Case 1', '$2y$12$REgPOJpQtuGP.0lEaqIC/Omuc0oXsDWIYpkCYLeapVbKLyoQpatuC', 'test1@gmail.com', '07712345678');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `baseImage` varchar(300) NOT NULL,
  `topImage` varchar(300) NOT NULL,
  `buttonText` varchar(200) NOT NULL,
  `buttonLink` varchar(200) NOT NULL,
  `buttonText2` varchar(200) NOT NULL,
  `buttonLink2` varchar(200) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `content`, `baseImage`, `topImage`, `buttonText`, `buttonLink`, `buttonText2`, `buttonLink2`, `pageId`) VALUES
(10, 'WE ARE HERE WHEN YOUR DOCTOR IS AWAY', '<p><span style=\"color: rgb(51, 51, 51); font-family: Roboto, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif;\">When you\'re unable to get an appointment with your regular doctor during the day or after hours we are there to take care of your child. We accept SAME DAY APPOINTMENTS, MOST INSURANCES and WALK-INS ARE WELCOMED.</span><br></p>', 'd9fea124c641b197b7dbae1a37cadd20.jpg', 'e870fc4c5793eb6ce7a78e8312b2a654.png', 'Discover Our Locations', 'locations', 'Contact Us', 'contact', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comparison`
--

CREATE TABLE `comparison` (
  `id` int(11) NOT NULL,
  `section` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `urgentCare` varchar(200) NOT NULL,
  `emergencyRoom` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comparison`
--

INSERT INTO `comparison` (`id`, `section`, `category`, `urgentCare`, `emergencyRoom`) VALUES
(1, 'Home Section', 'WAITING AREAS', 'Treats newborn babies less than three months old with a temperature up to 100.4 degrees', 'Treatments for arm/leg weaknesses'),
(2, 'Home Section', 'AWAITING AREAS', 'Minor trauma such as a common sprain or shallow cut', ' Medications for chest pain'),
(3, 'Home Section', 'BWAITING AREAS', 'Treatments for fever without rash', ' Treatments for patients who faces difficulty in breathing or wheezing'),
(4, 'Home Section', 'CWAITING AREAS', 'Medication for painful urination', 'Immediate attention for loss of consciousness or lethargy');

-- --------------------------------------------------------

--
-- Table structure for table `description_box`
--

CREATE TABLE `description_box` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `button_text` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `style` varchar(10) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `description_box`
--

INSERT INTO `description_box` (`id`, `title`, `content`, `image`, `button_text`, `link`, `style`, `pageId`) VALUES
(20, 'Title 3', '<p>sdf</p>', '54a9d83a63eed64582840d454d782a26.jpg', 'Button 1', 'Link 3', 'Right', 6),
(21, 'sadas', '<p>axcasc</p>', '4545194116eeca4488d6e7e774f187d7.jpg', 'f', 'Link 1 ', 'Left', 2),
(22, 'asdas updated', '<p>dv</p>', '9889fdd9e833a1c79dac0fb32d263feb.jpg', 'dfv', 'dvd', 'Right', 6);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `qualification` varchar(200) NOT NULL,
  `certification` varchar(200) NOT NULL,
  `specialty` varchar(200) NOT NULL,
  `medicalSchool` varchar(200) NOT NULL,
  `hospitalAffiliation` varchar(200) NOT NULL,
  `residency_fellowship` varchar(200) NOT NULL,
  `biography` text NOT NULL,
  `image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `qualification`, `certification`, `specialty`, `medicalSchool`, `hospitalAffiliation`, `residency_fellowship`, `biography`, `image`) VALUES
(24, 'Dr. Debra Barach', 'N/A', 'Board Certified', 'General Pediatrics and Child Development', 'UCLA School of Medicine', 'N/A', 'LA County-USC', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Barach was born and raised in Southern California until moving to Israel at the age of 14.  She received her Bachelor of Science in Biology, Magna Cum Laude from The Hebrew University of Jerusalem.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\"><span style=\"font-size: 1rem;\">Dr. Barach attended the UCLA School of Medicine and received her MD in 1985.  She completed her pediatric residency training at LA County-USC in 1988, and worked in private practice until 2006, after which she took time to raise her twin daughters.  She and her husband live in Sherman Oaks with their daughters, Sophia and Eliana.</span><br></p>', 'c662d23ededaf5f0670d1b3250b80c46.png'),
(25, 'Amy Wagner', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Wake Forest University School of Medicine', 'N/A', 'Children\'s Hospital of Orange County', '<div style=\"margin: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Dr. <span id=\"m_68670575145436630270.17085697843506265\" class=\"m_6867057514543663027highlight\" style=\"margin: 0px; padding: 0px; border: 0px;\">Wagner</span> is a board-certified pediatrician. She has been practicing for over seventeen years now. She earned her medical degree at Wake Forest University School of Medicine and received her pediatrics training at Children’s Hospital of Orange County.</div><div style=\"margin: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\"></div><div style=\"margin: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Dr. <span id=\"m_68670575145436630270.6949641937385154\" class=\"m_6867057514543663027highlight\" style=\"margin: 0px; padding: 0px; border: 0px;\">Wagner</span> loves spending time with her two children. She jogs regularly and keeps herself busy with home renovation projects. She is an avid poker player, who loves movies of all genres, and reading real books made out of paper.</div>', 'ea7795784eaa2a97910d9e4b0119f4d6.png'),
(26, 'Janesri De Silva', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'American University of the Caribbean', 'Pediatric Department at Northridge Hospital Medical Center Valley Presbyterian Hospital Tarzana Medical Center Huntington Memorial Hospital', 'USC Yale Affiliated Hospitals, MD, IL Kaiser Permanente, Sunset Children\'s Hospital Los Angeles', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. De Silva graduated from UCLA with a Bachelor’s degree in Neuroscience and then went on to the American University of the Caribbean to complete her medical degree.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Listen to Dr. De Silva’s interview with KFWB talk radio, regarding medication dosage for children.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">She did part of her medical training at USC, Yale affiliated hospitals and other hospitals in Maryland and Chicago. She then completed a 3 year pediatric residency at Kaiser Permanente, Sunset. While at Kaiser, she wrote a manual for pediatric interns on how to take care of patients.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">After residency, she did a fellowship at Children’s Hospital Los Angeles in Adolescent Medicine and worked at the Los Angeles Free Clinic. At CHLA, Dr. De Silva worked on research involving transitioning chronically ill children to adult health care.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">She is the 2014 to 2016 chair of the Pediatric department at Northridge Hospital Medical Center in Northridge, and is on staff at Valley Presbyterian Hospital in Van Nuys, Tarzana Medical CenterTarzana and Huntington Memorial Hospital in Pasadena, CA.</p>', '31a4747ed2e9daaf93678dca1d5c28f2.png'),
(27, 'Jose Vargas', 'P.A.', 'Board Certified', 'General Pediatrics and Child Development', 'Charles Drew University of Medicine and Science.', 'N/A', 'N/A', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Jose Vargas is a Physician Assistant that joined Kids and Teens in  January 2018. Jose comes to us with over 30 years of experience in Pediatric and Family Medicine. His past practices include the West Hollywood, Highland Park and Pacoima communities.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Jose enjoys learning and meeting new challenges.  His priority is to provide high quality Pediatric care to  young children and teens in the community.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Jose is married with two grown children.  He is very involved with many neighborhood organizations  that effect his community.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">In his free time Jose enjoys many outdoor activities such as camping, hiking and cycling. He enjoys keeping fit and attends the gym regularly.  He likes to encourage his patients and their families to get out there and exercise and to promote a healthy lifestyle</p>', '4da14e91134e33fee817bac85fb34e83.png'),
(28, 'Dr. Gaya Selvakkumaran', 'MD FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'University of Zambia, School of Medicine', 'Methodist Hospital', 'White Memorial Medical Center', '<p><span style=\"font-family: OpenSans-Semibold;\">Dr Gaya (Gayathri Selvakkumaran MD FAAP), did her Medical education/training in Zambia where she treated many infectious diseases no longer prevalent in the first world countries. She then moved to UK for Neonatal fellowship training, but moved to California in the middle of it. She completed her Pediatric residency at White Memorial Medical Center, and since then been an esteemed Pediatrician working in the San Gabriel Valley area for the past 8 years. With her vast international experience in Pediatrics she provides excellent Pediatric care to her patients. In her free time she enjoys classical dancing, cooking and best of all spending time with her children; hence her maternal instincts makes her a lovable Pediatrician.</span><br></p>', 'b60b02be525f19ad91298524500e1f6e.png'),
(29, 'Kaberi Mozumder', 'PNP', 'Board Certified', 'General Pediatrics and Child Development', 'Yale School of Nursing', 'Stanford Children\'s Hospital, Children\'s Hospital of Los Angeles, and Cedars-Sinai Medical Center', 'N/A', '<p><span style=\"margin: 0px; padding: 0px; font-weight: 700; border: 0px; font-family: OpenSans-Semibold;\">Kaberi has been a pediatric nurse practitioner for over 10 years graduating from Yale School of Nursing in 2008. She has extensive experience in both the inpatient and outpatient setting. She previously worked in specialty care for pediatric cardiology and cardio-thoracic surgery at a number of large academic centers including Stanford Children’s Hospital, Children’s Hospital of Los Angeles and Cedars-Sinai Medical Center before moving into the primary and urgent care setting. She is passionate about helping children and loves working with kids of all ages. Some of her special interests include adolescent medicine, mental health, asthma, and nutrition. She is a native Southern Californian and loves serving her home community. She is fluent in Spanish and spends her free time travelling, baking and spending time with her shih tzu Timmy.</span><br></p>', '84f1834395d7e2a25d79020f5d634e44.png'),
(30, 'Dr. Margaret Zdarzyl', 'MD FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Nicolaus Copernicus University in Poland', 'USC Verdugo Hills Hospital', 'N/A', '<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin: 0px; padding: 0px; border-spacing: 0px; border: 0px; font-family: OpenSans-Regular;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px;\"><tr style=\"margin: 0px; padding: 0px; border: 0px;\"><td class=\"tdbottomborder\" colspan=\"3\" style=\"margin: 0px; padding: 0px; border: 0px;\">Margaret Zdarzyl, MD, is board certified in pediatrics. Dr. Zdarzyl earned her medical degree from Nicolaus Copernicus University in Poland. She completed her internship and residency at White Memorial Medical Center in Los Angeles. Dr. Zdarzyl is passionate about helping children and believes deeply in patient education. She loves to ski and garden. In addition to English, Dr. Zdarzyl speaks Polish and Spanish</td></tr></tbody></table>', 'b780527911c03b717f92f21a3eb44654.png'),
(31, 'Hilma Benjamin', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Penn State College of Medicine', 'Northridge Hospital Medical Center Providence Tarzana Medical Center Medical Director of After-Hours Pediatric Urgent Care', 'Children\'s Hospital, St. Petersburg, FL Tampa General Hospital, Tampa, FL', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. Hilma Benjamin graduated from the University of the Virgin Islands with a Bachelor of Science in Biology and later earned a Master’s of Science in Molecular and Cellular Biology at the University of Maryland, Baltimore County.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">She received her medical degree in 2004 from Penn State College of Medicine and completed her residency in pediatrics at All Children’s Hospital in St. Petersburg, Florida, and Tampa General Hospital in Tampa, Florida.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Before joining Kids and Teens in 2009, she worked as locums tenens in private practice in the San Francisco Bay Area and at the Monterey County Health Department in outpatient and inpatient settings at the Natividad Medical Center, affiliated with UCSF.</p>', '9146ddf0b5392a45b8f414a44bad061d.png'),
(33, 'Victoria Millet', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Infectious Disease', 'USC', 'Providence Tarzana Medical Center Northridge Hospital and Medical Center', 'University Hospital, San Diego University of California Los Angeles', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. Millet completed her undergraduate work as a Zoology Major at the University of California at Los Angeles in 1969. She earned a Master of Science Degree at the University of Southern California which was awarded in 1972. She completed a three year residency in pediatrics at University Hospital in San Diego in 1975.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">She completed a Pediatric Infectious Diseases/Ambulatory Pediatrics fellowship at the Center for the Health Sciences, University of California at Los Angeles in 1980.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Dr. Millet has been in private pediatric practice since 1984. She joined Kids and Teens Medical Group in January of 2011. She continues to do consulting work in pediatric infectious diseases for the Kids and Teens group, as well as for her colleagues in the local pediatric community. She also enjoys teaching and is a member of the faculty of the Family Practice Residency Program at Northridge Hospital.</p>', '1b66b70e3a6d888c33684d151c092c6a.png'),
(35, 'Priya Harder', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Drexel University College of Medicine', 'N/A', 'Dupont Hospital/TJUH, DE', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. Harder was born and raised in La Canada and graduated cum laude from USC with a degree in English Literature and Language. She attended medical school at Drexel University College of Medicine <em style=\"margin: 0px; padding: 0px; border: 0px;\">(formerly MCP Hahnemann)</em>. She completed her internship and residency at Dupont Hospital for Children/TJUH in Wilmington, Delaware.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Dr. Harder has worked in private practice in Pasadena since 2010 and presently sees newborn babies at Huntington Memorial Hospital. Dr. Harder lives in La Canada with her husband and three children, Arianna, Deven & Gregory</p>', '5ebf70ecdf9a39cb66060fbbcc5011b0.png'),
(236, 'Barbara Rodriguez', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Creighton University Medical School', 'N/A', 'LAC-USC', '<p><span style=\"font-family: OpenSans-Semibold;\">She attended the University of Southern California & received her Bachelor of Science degree in Biological Sciences. She then attended Creighton University Medical School & received her MD in 1985. She completed her Pediatric Training at LAC-USC in 1988 & has been involved in Pediatric Primary Care & Pediatric Urgent Care since then. She has been an active member of Creighton’s Medical Dean’s Alumni Advisory Board since 1998. She is a Fellow of the American Academy of Pediatrics & a member of the American Board of Pediatrics.</span><br></p>', 'd32f7a88e21f74c87ba7961afa25bb96.png'),
(237, 'Martin Fineberg', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'The University of Melbourne, Royal Children’s Hospital', 'N/A', 'Emory University', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Martin Fineberg has been with Pediatric Affiliates Medical Group, Inc. since 1988.  A native of Melbourne, Australia he studied medicine at The University of Melbourne.  He then trained in pediatrics at the Royal Children’s Hospital in Melbourne followed by a pediatric residency at Emory University in Atlanta. Following this, he undertook additional training in neonatology at Children’s Hospital Los Angeles. In addition, Dr. Fineberg worked in the area of pediatric hematology/oncology at the Prince of Wales Hospital in Sydney, Australia. Dr. Fineberg is licensed to practice medicine in California and Australia, and was board certified by the American Board of Pediatrics in 1985. He was re-certified in 2009 and is current with Maintenance of Certification with the American Board of Pediatrics.  He is a Fellow of the American Academy of Pediatrics, a member of the California Medical Association, the American Medical Association and an American Academy member of the Canadian Paediatric Society.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Dr. Fineberg was previously the director of the Neonatal Intensive Care Unit (NICU) at West Hills Hospital and Medical Center.  He is now full time in general pediatrics with a special expertise and interest in the care of the high-risk NICU graduate and children with pulmonary diseases.    He is also the Pediatric Affiliates vaccine expert</p>', '8fffe1825b26a5ec45215ab652282070.png'),
(238, 'Mark Snyder', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Kansas University in Kansas City', 'N/A', 'Cedars Sinai Medical Center', '<p><span style=\"font-family: OpenSans-Semibold;\">Mark Snyder is a Woodland hills native and an alum of El Camino Real High School.  He went to college at U. C. San Diego and majored in Biomedical Engineering, and he spent his junior year abroad studying at Hebrew University in Jerusalem.  He then attended medical school at Kansas University in Kansas City, but returned to L.A. for his pediatric residency at Cedars Sinai Medical Center. He was board certified in pediatrics originally in 1992, and then recertified in 1998, 2006, and in 2015, and he is a fellow of the American Academy of Pediatrics.  He loves to spend time with his family, and he also enjoys mountain biking, exercising at the gym, hiking with the family dog, following politics and world events, and reading about all of the sciences, with a special interest in astronomy and evolution.  And of course, he loves and enjoys his work as a pediatrician at Pediatric Affiliates, working in the community where he grew up and currently lives.</span><br></p>', 'c370c05a8598376daf222d5d185466ea.png'),
(239, 'Beth Melin-Perel', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'University of Arkansas Medical School', 'N/A', 'St. Christopher’s Hospital for Children in Philadelphia', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. Beth Melin-Perel completed her undergraduate education at Washington University in St. Louis while on a National Merit scholarship. She graduated summa cum laude with a bachelor’s degree in biology and a minor in literature.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">She attended the University of Arkansas Medical School on a full scholarship, and went on to do her pediatric residency at St. Christopher’s Hospital for Children in Philadelphia. She was on the teaching staff at Yale University in New Haven, CT and active in private practice. She is Board Certified by the American Board of Pediatrics, and is a member of the American Academy of Pediatrics and the Los Angeles Pediatric Society.  She and her wonderful husband, Dr. Issie Perel, raised three sons who are now in graduate programs. Dr.Beth volunteers at the Westminster Free Clinic in Thousand Oaks, and enjoys gardening, movies, and healthy cooking in her leisure time.</p>', '80fb35072623cfe84188f5d47180e97b.png'),
(240, 'Marianne Woods', 'MD, MSPH', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Jagiellonian University Medical College, Krakow, Poland', 'N/A', 'Kaiser Permanente, Los Angeles', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. Woods received her MD degree from the Jagiellonian University Medical College in Krakow, Poland in 1984 and an MSPH degree from the UCLA School of Public Health in 1979. She completed her residency program in Pediatrics at Kaiser Permanente Medical Center in Los Angeles, CA in 1993 and worked as a pediatrician in the outpatient clinics of the Kaiser Permanente system for over 20 years.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">At the Kids and Teens clinics, she continues to provide high quality acute and preventative care with a special interest in nutritional guidance, weight control and anticipatory guidance</p>', '6a5601307ae6c01566400ed4963f4ba2.png'),
(241, 'Peter Jackson', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'U.C., Irvine', 'N/A', 'Cedars Sinai Medical Center, Beverly Hills', '<p><span style=\"font-family: OpenSans-Semibold;\">Dr. Peter Jackson has practiced pediatrics for nearly 31 years, all in the same location in Pasadena at the Children’s Clinic now Kids & Teens Medical Group. Dr. Jackson is a Southern California native who went to medical school at U.C. Irvine and had an internship and residency at Cedar Sinai Medical Center in Beverly Hills.</span><br></p>', '48fd4f9c680db85e26e6edb92b2fbfac.png'),
(242, 'Padma Bala', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Madras Medical College, Madras, India', 'N/A', 'Rushy Presbyterian Medical Center, IL', '<p><span style=\"font-family: OpenSans-Semibold;\">Dr. Padma Bala, graduated from the prestigious Madras Medical College at Madras, India. She completed her Residency in Pediatrics at Rush Presbyterian Medical Center in Chicago, after which she served in US Army Medical Corps, as a Pediatrician with the Rank of Lt Colonel. She then joined Baylor Medical Group in Dallas while consistently achieving “Best Pediatrician Award” from the local Communities for many years. She moved to Los Angeles to be close to her children and grandchildren</span><br></p>', '9c5e5f53be3dfdeb0b9b3f69805fe75f.png'),
(243, 'Lourdes Mosqueda', 'MD', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'University of Illinois College of Medicine, Rockford', 'N/A', 'California Pacific Medical Center, San Francisco', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Semibold;\">Dr. Mosqueda attended the University of California, Irvine where she received her Bachelors degree in Biological Sciences.  She attended the University of Illinois College of Medicine, Rockford where she received her medical degree.  Dr. Mosqueda completed her Pediatric Residency training at California Pacific Medical Center in San Francisco, California.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px; font-family: OpenSans-Regular;\">Dr. Mosqueda is a Pediatrician who has practiced medicine in San Francisco since 1997.  In 2010, Dr. Mosqueda, her husband and 3 children moved to South Pasadena where she continues her commitment to children’s health.  She sees children from birth until their 18th birthday.  She looks forward to meeting you and your family.</p>', '13a5b311816997c88714d7829e8e3e07.png'),
(244, 'Orlando Ayala', ' PA-C', 'Board Certified', 'Physician Assistant', 'Physician’s Assistant at the University of Southern California', 'N/A', 'N/A', '<p><span style=\"font-family: OpenSans-Semibold;\">Orlando Ayala grew up in East Los Angeles and completed his training as physician’s assistant at the University of Southern California. He worked at the Clinica Medica San Miguel for 8 years prior to starting full time at Kids and Teens Medical Group. Mr Ayala works at the Van Nuys and Pasadena locations of Kids and Teens. He lives in Eagle Rock with his wife and daughter, Isabella.</span><br></p>', 'f00a5d54badbf56b05d78f85f0a8c433.png'),
(245, 'Grace Dasovich', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'University of Minnesota Medical School', 'N/A', 'University of California Irvine / Children\'s Hospital of Orange County', '<p><span style=\"font-family: OpenSans-Semibold;\">Dr. Grace Dasovich is a board certified Pediatrician who is excited to start a career at Kids & Teens Medical Group. She attended Northwestern University and obtained a Bachelor of Science degree in Biomedical Engineering. She received her medical degree from the University of Minnesota Medical School and completed her pediatrics residency at the University of California Irvine / Children’s Hospital of Orange County program. Outside of medicine, she enjoys spending time with family, traveling, and running marathons</span><br></p>', '2b90d116072838d8ce55a1442fc8386f.png'),
(248, 'Cze-Ja Tam', 'CPNP', 'Board Certified', 'Pediatric Nurse Practitioner', '-', '-', '-', '<p>-</p>', 'a4ddf3eb9f932b4371c1f121af97ca05.png');

-- --------------------------------------------------------

--
-- Table structure for table `feature_box`
--

CREATE TABLE `feature_box` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `button_text` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `image` varchar(300) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feature_box`
--

INSERT INTO `feature_box` (`id`, `title`, `content`, `button_text`, `link`, `image`, `pageId`) VALUES
(2, 'We accept most insurances', 'We accept most insurance including HMOs, PPOs, and Medi-Cal. If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians. Or contact us on&nbsp;<a href=\"tel:(818) 361-5437\" target=\"_blank\" style=\"\">(818) 361-5437</a>&nbsp;and have it changed over the phone.<br>', 'Check Eligibility', 'Link 2 ', '99975d17c361bbcf411151add75ecdeb.jpg', 1),
(12, 'Title 4 new', '<p>Content of title 4.</p>', 'Text 4', 'Link 4', '5ab33cad578fe11ec82a62fafba52db7.jpg', 6);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `remarks` text NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `afterHour` varchar(10) NOT NULL,
  `saturday` varchar(10) NOT NULL,
  `sunday` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `address`, `telephone`, `fax`, `remarks`, `latitude`, `longitude`, `afterHour`, `saturday`, `sunday`) VALUES
(3, 'Agoura Hills', '29525 Canwood Street #111\r\nAgoura Hills, CA 91301', '818-706-8133', '818-659-3895', '', '34.148164', '-118.7694706', '', '', ''),
(5, 'Arcadia', '75 North Santa Anita Ave, Suite 206\r\nArcadia, CA 91006', '626-795-8811', '626-231-0501', 'Now Open Monday-Friday!', '34.1409735', '-118.0339027', '', '0', '0'),
(10, 'Beverly Hills', '240 S. La Cienega Boulevard, Suite 350\r\nBeverly Hills, CA 90211', '323-664-1977', '424-512-1700', 'Free Parking at Gregory & La Cienega!', '34.0639201', '-118.3780179', '', '0', '0'),
(11, 'Encino', '17609 Ventura Blvd., Ste. 106\r\nEncino, CA 91316', '818-361-5437', '818-658-9363', '', '34.1622902', '-118.5187972', '', '0', '0'),
(12, 'La Canada', '1442 Foothill Blvd.\r\nLa Canada Flintridge, CA 91011', '818-361-5437', '626-406-1801', 'Now Open Monday-Friday!!', '34.2085283', '-118.2121095', '', '', ''),
(18, 'Northridge', '8940 Reseda Blvd., Suite 101\r\nNorthridge, CA 91324', '818-361-5437', '818-659-3879', '', '34.2330844', '-118.5378921', 'on', 'on', 'on'),
(19, 'Pasadena', '504 S Sierra Madre Blvd.\r\nPasadena, CA 91107', ' 818-361-5437', ' 818-361-5437', '', '34.1376025', '-118.1033559', 'on', 'on', 'on'),
(21, 'San Fernando', '777 Truman Street Suite 105\r\nSan Fernando, CA 91340', '818-361-5437', '818-639-9827', '', '34.2814428', '-118.4408147', '', '0', '0'),
(22, 'Van Nuys', '14426 Gilmore Street\r\nVan Nuys, CA 91401', '818-361-5437', '818-646-9568', 'Open until 7 pm on Monday, Tuesday and Fridays and Saturday until 5 pm!', '34.1874945', '-118.44969', '', 'on', ''),
(23, 'West Hills', '7345 Medical Center Drive #400\r\nWest Hills, CA 91307', '818-883-0460', '818-538-8804', 'Now Open Until 8 pm on Monday and Wednesday Evenings!', '34.2031499', '-118.6337355', 'on', 'on', '');

-- --------------------------------------------------------

--
-- Table structure for table `location_has_doctors`
--

CREATE TABLE `location_has_doctors` (
  `id` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `doctorId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `location_has_doctors`
--

INSERT INTO `location_has_doctors` (`id`, `locationId`, `doctorId`) VALUES
(70, 11, 34),
(71, 18, 34),
(72, 22, 34),
(73, 23, 34),
(102, 5, 26),
(103, 10, 26),
(104, 12, 26),
(105, 19, 26),
(107, 5, 28),
(108, 19, 28),
(115, 18, 31),
(116, 23, 31),
(121, 11, 33),
(122, 18, 33),
(123, 22, 33),
(124, 23, 33),
(125, 5, 35),
(126, 12, 35),
(127, 19, 35),
(128, 5, 236),
(129, 12, 236),
(130, 19, 236),
(131, 21, 236),
(132, 3, 237),
(133, 10, 237),
(134, 11, 237),
(135, 19, 237),
(136, 23, 237),
(141, 18, 240),
(142, 19, 240),
(143, 22, 240),
(147, 21, 242),
(148, 23, 242),
(149, 5, 243),
(150, 19, 243),
(151, 21, 243),
(152, 19, 244),
(153, 22, 244),
(157, 3, 238),
(158, 23, 238),
(162, 21, 27),
(163, 18, 29),
(164, 19, 29),
(165, 21, 29),
(166, 22, 29),
(167, 23, 29),
(168, 12, 30),
(169, 5, 241),
(170, 12, 241),
(171, 19, 241),
(172, 3, 239),
(173, 23, 239),
(174, 10, 245),
(175, 12, 245),
(176, 23, 245),
(179, 11, 24),
(180, 18, 25),
(181, 23, 25);

-- --------------------------------------------------------

--
-- Table structure for table `location_schedule`
--

CREATE TABLE `location_schedule` (
  `id` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `fromHour` time NOT NULL,
  `toHour` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_schedule`
--

INSERT INTO `location_schedule` (`id`, `locationId`, `day`, `status`, `fromHour`, `toHour`) VALUES
(5, 3, 'Monday', 'Open', '09:00:00', '18:00:00'),
(6, 3, 'Tuesday', 'Open', '21:00:00', '18:00:00'),
(7, 3, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(8, 3, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(9, 3, 'Friday', 'Open', '09:00:00', '18:00:00'),
(10, 3, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(11, 3, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(12, 4, 'Monday', 'Open', '08:30:00', '17:30:00'),
(13, 4, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(14, 5, 'Monday', 'Open', '08:30:00', '17:30:00'),
(15, 5, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(16, 5, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(17, 5, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(18, 5, 'Friday', 'Open', '09:00:00', '18:00:00'),
(19, 5, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(20, 5, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(21, 6, 'Monday', 'Open', '08:30:00', '17:30:00'),
(22, 6, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(23, 6, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(24, 6, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(25, 6, 'Friday', 'Open', '09:00:00', '18:00:00'),
(26, 6, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(27, 6, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(28, 7, 'Monday', 'Open', '08:30:00', '17:30:00'),
(29, 7, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(30, 7, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(31, 7, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(32, 7, 'Friday', 'Open', '09:00:00', '18:00:00'),
(33, 7, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(34, 7, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(35, 10, 'Monday', 'Open', '09:00:00', '18:00:00'),
(36, 10, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(37, 10, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(38, 10, 'Thursday', 'Open', '10:00:00', '17:00:00'),
(39, 10, 'Friday', 'Open', '09:00:00', '18:00:00'),
(40, 10, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(41, 10, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(42, 11, 'Monday', 'Open', '09:00:00', '18:00:00'),
(43, 11, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(44, 11, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(45, 11, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(46, 11, 'Friday', 'Open', '09:00:00', '18:00:00'),
(47, 11, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(48, 11, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(49, 12, 'Monday', 'Open', '09:00:00', '18:00:00'),
(50, 12, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(51, 12, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(52, 12, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(53, 12, 'Friday', 'Open', '09:00:00', '18:00:00'),
(54, 12, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(55, 12, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(91, 18, 'Monday', 'Open', '09:00:00', '20:00:00'),
(92, 18, 'Tuesday', 'Open', '09:00:00', '20:00:00'),
(93, 18, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(94, 18, 'Thursday', 'Open', '09:00:00', '20:00:00'),
(95, 18, 'Friday', 'Open', '09:00:00', '20:00:00'),
(96, 18, 'Saturday', 'Open', '12:00:00', '20:00:00'),
(97, 18, 'Sunday', 'Open', '12:00:00', '20:00:00'),
(98, 19, 'Monday', 'Open', '09:00:00', '20:00:00'),
(99, 19, 'Tuesday', 'Open', '09:00:00', '20:00:00'),
(100, 19, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(101, 19, 'Thursday', 'Open', '09:00:00', '20:00:00'),
(102, 19, 'Friday', 'Open', '09:00:00', '20:00:00'),
(103, 19, 'Saturday', 'Open', '12:00:00', '20:00:00'),
(104, 19, 'Sunday', 'Open', '12:00:00', '20:00:00'),
(112, 21, 'Monday', 'Open', '09:00:00', '18:00:00'),
(113, 21, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(114, 21, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(115, 21, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(116, 21, 'Friday', 'Open', '09:00:00', '18:00:00'),
(117, 21, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(118, 21, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(119, 22, 'Monday', 'Open', '09:00:00', '19:00:00'),
(120, 22, 'Tuesday', 'Open', '09:00:00', '19:00:00'),
(121, 22, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(122, 22, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(123, 22, 'Friday', 'Open', '09:00:00', '19:00:00'),
(124, 22, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(125, 22, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(126, 23, 'Monday', 'Open', '09:00:00', '20:00:00'),
(127, 23, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(128, 23, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(129, 23, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(130, 23, 'Friday', 'Open', '09:00:00', '18:00:00'),
(131, 23, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(132, 23, 'Sunday', 'Close', '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`) VALUES
(1, 'Home'),
(2, 'About'),
(5, 'Common'),
(6, 'Locations'),
(7, 'Doctors');

-- --------------------------------------------------------

--
-- Table structure for table `pointers`
--

CREATE TABLE `pointers` (
  `id` int(11) NOT NULL,
  `section` varchar(200) NOT NULL,
  `point` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accordion`
--
ALTER TABLE `accordion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comparison`
--
ALTER TABLE `comparison`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `description_box`
--
ALTER TABLE `description_box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_box`
--
ALTER TABLE `feature_box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_has_doctors`
--
ALTER TABLE `location_has_doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_schedule`
--
ALTER TABLE `location_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pointers`
--
ALTER TABLE `pointers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accordion`
--
ALTER TABLE `accordion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `comparison`
--
ALTER TABLE `comparison`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `description_box`
--
ALTER TABLE `description_box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `feature_box`
--
ALTER TABLE `feature_box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `location_has_doctors`
--
ALTER TABLE `location_has_doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=182;

--
-- AUTO_INCREMENT for table `location_schedule`
--
ALTER TABLE `location_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=133;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pointers`
--
ALTER TABLE `pointers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
