-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2020 at 08:55 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `remarks` text NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `afterHour` varchar(10) NOT NULL,
  `saturday` varchar(10) NOT NULL,
  `sunday` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `address`, `telephone`, `fax`, `remarks`, `latitude`, `longitude`, `afterHour`, `saturday`, `sunday`) VALUES
(3, 'Agoura Hills', '29525 Canwood Street #111\r\nAgoura Hills, CA 91301', '818-706-8133', '818-659-3895', '', '34.148164', '-118.7694706', '', '', ''),
(5, 'Arcadia', '75 North Santa Anita Ave, Suite 206\r\nArcadia, CA 91006', '626-795-8811', '626-231-0501', 'Now Open Monday-Friday!', '34.1409735', '-118.0339027', '', '0', '0'),
(10, 'Beverly Hills', '240 S. La Cienega Boulevard, Suite 350\r\nBeverly Hills, CA 90211', '323-664-1977', '424-512-1700', 'Free Parking at Gregory & La Cienega!', '34.0639201', '-118.3780179', '', '0', '0'),
(11, 'Encino', '17609 Ventura Blvd., Ste. 106\r\nEncino, CA 91316', '818-361-5437', '818-658-9363', '', '34.1622902', '-118.5187972', '', '0', '0'),
(12, 'La Canada Flintridge', '1442 Foothill Blvd.\r\nLa Canada Flintridge, CA 91011', '818-361-5437', '626-406-1801', 'Now Open Monday-Friday!!', '34.2085283', '-118.2121095', '', '0', '0'),
(18, 'Northridge', '8940 Reseda Blvd., Suite 101\r\nNorthridge, CA 91324', '818-361-5437', '818-659-3879', '', '34.2330844', '-118.5378921', 'on', 'on', 'on'),
(19, 'Pasadena', '504 S Sierra Madre Blvd.\r\nPasadena, CA 91107', ' 818-361-5437', ' 818-361-5437', '', '34.1376025', '-118.1033559', 'on', 'on', 'on'),
(21, 'San Fernando', '777 Truman Street Suite 105\r\nSan Fernando, CA 91340', '818-361-5437', '818-639-9827', '', '34.2814428', '-118.4408147', '', '0', '0'),
(22, 'Van Nuys', '14426 Gilmore Street\r\nVan Nuys, CA 91401', '818-361-5437', '818-646-9568', 'Open until 7 pm on Monday, Tuesday and Fridays and Saturday until 5 pm!', '34.1874945', '-118.44969', '', 'on', ''),
(23, 'West Hills', '7345 Medical Center Drive #400\r\nWest Hills, CA 91307', '818-883-0460', '818-538-8804', 'Now Open Until 8 pm on Monday and Wednesday Evenings!', '34.2031499', '-118.6337355', 'on', 'on', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
