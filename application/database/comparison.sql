-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 11:21 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `comparison`
--

CREATE TABLE `comparison` (
  `id` int(11) NOT NULL,
  `section` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `urgentCare` varchar(200) NOT NULL,
  `emergencyRoom` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comparison`
--

INSERT INTO `comparison` (`id`, `section`, `category`, `urgentCare`, `emergencyRoom`) VALUES
(1, 'sectionOne', 'WAITING AREAS', 'Treats newborn babies less than three months old with a temperature up to 100.4 degrees', 'Treatments for arm/leg weaknesses'),
(2, 'sectionTwo', 'AWAITING AREAS', 'Minor trauma such as a common sprain or shallow cut', ' Medications for chest pain'),
(3, 'sectionOne', 'BWAITING AREAS', 'Treatments for fever without rash', ' Treatments for patients who faces difficulty in breathing or wheezing'),
(4, 'sectionTwo', 'CWAITING AREAS', 'Medication for painful urination', 'Immediate attention for loss of consciousness or lethargy');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comparison`
--
ALTER TABLE `comparison`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comparison`
--
ALTER TABLE `comparison`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
