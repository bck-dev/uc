<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Doctor_model extends CI_Model
{
    public function getDoctorByName($name) {
        $this->db->select('*');
        $this->db->from('doctors');
        $this->db->where('name', $name);
        return $this->db->get()->row();
    } 
    public function doctorsLocations() {
        $this->db->select('location_has_doctors.*, locations.name');
        $this->db->from('location_has_doctors');
        $this->db->join('locations', "location_has_doctors.locationId = locations.id");
        return $this->db->get()->result();
    }   
    public function doctorLocation($id) {
        $this->db->select('location_has_doctors.*, locations.name');
        $this->db->from('location_has_doctors');
        $this->db->where('doctorId', $id);
        $this->db->join('locations', "location_has_doctors.locationId = locations.id");
        return $this->db->get()->result();
    }

    public function doctorLocationNotSelected($id) {
        $this->db->select('*');
        $this->db->from('location_has_doctors');
        $this->db->where('doctorId', $id);
        $data = $this->db->get()->result();

        foreach($data as $d){
            $locationIds[] = $d->locationId;
        }

        $this->db->select('locations.id, locations.name');
        $this->db->from('locations');
        $this->db->where_not_in('id', $locationIds);
        return $this->db->get()->result();
    }

    public function doctorLocationWithShedule($id) {
        $date = new DateTime();
        $day = $date->format('l');
        
        $this->db->select('location_has_doctors.*, locations.*, location_schedule.fromHour, location_schedule.toHour, location_schedule.status');
        $this->db->from('location_has_doctors');
        $this->db->where('doctorId', $id);
        $this->db->join('locations', "location_has_doctors.locationId = locations.id");
        $this->db->join('location_schedule', "location_schedule.locationId = locations.id AND location_schedule.day = '$day'");
        
        return $this->db->get()->result();
    }

}