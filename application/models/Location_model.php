<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Location_model extends CI_Model
{
    public function homeLocation(){
        $date = new DateTime();
        $day = $date->format('l');

        $this->db->select('locations.*, location_schedule.fromHour, location_schedule.toHour');
        $this->db->from('locations');
        $this->db->join('location_schedule', "location_schedule.locationId = locations.id AND location_schedule.day = '$day'");
        $this->db->order_by('afterHour', 'DESC');
        return $this->db->get()->result();
    }

    public function afterhour(){
        $date = new DateTime();
        $day = $date->format('l');

        $this->db->select('locations.*, location_schedule.fromHour, location_schedule.toHour');
        $this->db->from('locations');
        $this->db->join('location_schedule', "location_schedule.locationId = locations.id AND location_schedule.day = '$day'");
        $this->db->where('locations.afterHour', "on");
        return $this->db->get()->result();
    }

    
    public function locationWithShedule($id) {
        // $date = new DateTime();
        // $day = $date->format('l');
        
        $this->db->select(' locations.*, location_schedule.fromHour, location_schedule.toHour, location_schedule.status,location_schedule.day');
        $this->db->from('locations');
        $this->db->where('locations.id', $id);
        $this->db->join('location_schedule', "location_schedule.locationId = locations.id");
        
        return $this->db->get()->result();
    }

    function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo)
    {
    $earthRadius = 6371000;
    // convert from degrees to radians
    $latFrom = deg2rad($latitudeFrom);
    $lonFrom = deg2rad($longitudeFrom);
    $latTo = deg2rad($latitudeTo);
    $lonTo = deg2rad($longitudeTo);
    
    $latDelta = $latTo - $latFrom;
    $lonDelta = $lonTo - $lonFrom;
    
    $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) + cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
    return number_format((($angle * $earthRadius)/1000)* 0.621, 1)." mi";
    }

    public function search($key){
        $this->db->select('*');
        $this->db->from('locations');
        $this->db->like('name', $key);
        $this->db->or_like('address', $key);
        $this->db->or_like('telephone', $key);
        $this->db->or_like('fax', $key);
        $this->db->or_like('remarks', $key);
        return $this->db->get()->result();
    }

}