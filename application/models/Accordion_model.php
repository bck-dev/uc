<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Accordion_model extends CI_Model
{
    
    public function accordionFull() {
        $this->db->select('accordion.id, accordion.title, accordion.content, accordion.pageId, pages.name');
        $this->db->from('accordion');
        $this->db->join('pages', "accordion.pageId = pages.id");
        $this->db->order_by('accordion.pageId', 'ASC');
        
        return $this->db->get()->result();
    }

    public function getPage($id) {
        $this->db->select('accordion.*, pages.name');
        $this->db->from('accordion');
        $this->db->where('accordion.id', $id);
        $this->db->join('pages', "accordion.pageId = pages.id");
        return $this->db->get()->result();
    }

}