<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class FeatureBox_model extends CI_Model
{
    
    public function featureBoxFull() {
        $this->db->select('feature_box.*, pages.name');
        $this->db->from('feature_box');
        $this->db->join('pages', "feature_box.pageId = pages.id");
        return $this->db->get()->result();
    }

    public function getPage($id) {
        $this->db->select('feature_box.*, pages.name');
        $this->db->from('feature_box');
        $this->db->where('feature_box.id', $id);
        $this->db->join('pages', "feature_box.pageId = pages.id");
        return $this->db->get()->result();
    }

}