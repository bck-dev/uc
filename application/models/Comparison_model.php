<?php if(!defined('BASEPATH')) exit('No direct script access allowed');

class Comparison_model extends CI_Model
{
    
    public function getPageComparison($table,$feild,$value,$direction) {
        $this->db->from($table);
        $this->db->where('section', $value);
        $this->db->order_by($feild, $direction);
        return $this->db->get()->result();
    }


}