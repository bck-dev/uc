-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2020 at 05:29 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uc`
--

-- --------------------------------------------------------

--
-- Table structure for table `accordion`
--

CREATE TABLE `accordion` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `pageId` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `accordion`
--

INSERT INTO `accordion` (`id`, `title`, `content`, `pageId`) VALUES
(53, 'Affordable Care Act ', '<p>Under the Affordable Care Act, families are having their \r\ninsurance changed to new insurance products, as mandated by federal law.\r\n As a result, traditional insurance products utilizing legacy PPO \r\nnetworks are no longer accessible for many of our patients, though some \r\nwho obtain their insurance through unions or government agencies may \r\nstill have access. The state of California has not adopted the federal \r\nextension allowing individuals and families to keep their plans for an \r\nadditional two years, so in most cases families do not have the choice \r\nto keep their legacy networks or the doctors who are in them.</p>', '8'),
(54, 'Narrow Networks', 'While&nbsp;our contracts with insurers have not changed.&nbsp;The \r\npolicies insurers offer has. Insurers have manufactured new, “narrow” \r\nnetworks that have arbitrarily excluded Rose City Pediatrics. Insurers \r\nadmit that these new, narrow (usually EPO) networks are designed to \r\nfulfill insurance companies’ needs for a minimum number of “providers or\r\n prescribers” and to fulfill the needs of the insurance companies’ \r\nmembers at the lowest cost to the insurance company. Confusingly, many \r\nof these new network plans are marketed and advertised as PPOs, which we\r\n believe they are not. We have only ascertained the truth after families\r\n accessed our services.', '8'),
(55, 'Office Billing Policy', '<p>As a result of the Affordable Care Act, and because of \r\ndiminished access from new limited assurance networks, and although our \r\nexisting relationships with insurers has not changed, we have been \r\nforced to implement new billing policies to remain viable.</p><p>If we cannot quickly determine if we will get paid we will charge you\r\n $125.00 deposit at the time of service and we will refund when if we \r\nget paid.</p><p>We appreciate that our families value the traditional and high \r\nquality services they have come to expect, and that we expect from \r\nourselves. With apologies for any inconvenience this may cause, bat \r\nassuring you of our best attention at all times, it is our intention to \r\ncontinue to provide the caliber of care our patients have enjoyed over \r\nthe years.</p>', '8');

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL,
  `fullName` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `fullName`, `password`, `email`, `phone`) VALUES
(1, 'Test Case 1', '$2y$12$REgPOJpQtuGP.0lEaqIC/Omuc0oXsDWIYpkCYLeapVbKLyoQpatuC', 'test1@gmail.com', '07712345678'),
(2, 'Test Case 2', '$2y$10$0LS1nIWVLoZ3TGG9GwRbY.kTTjFNFlhh2ch567HO10nc5bSEWKj4.', 'test2@gmail.com', '0765594469'),
(3, 'Test Case 3 updated', '$2y$10$OgLwsshE0WdyzfuZE1BMvO9JWQImcg3P3FncfXfLVVEZB9J9DEjdG', 'test3@gmail.com', '0718721540');

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `baseImage` varchar(300) NOT NULL,
  `topImage` varchar(300) NOT NULL,
  `buttonText` varchar(200) NOT NULL,
  `buttonLink` varchar(200) NOT NULL,
  `secondButtonText` varchar(200) NOT NULL,
  `secondButtonLink` varchar(200) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `title`, `content`, `baseImage`, `topImage`, `buttonText`, `buttonLink`, `secondButtonText`, `secondButtonLink`, `pageId`) VALUES
(2, 'Title 2', '<p>Review 2</p>', 'e85ab2fedf039cff7f4b7b24f28c47af.png', 'ae8c3e6f07b0dbc14895aa6ed9441d89.jpg', 'Text 99', 'Link 99', '', '', 2),
(8, 'Title 6', '<p>Content of title 6</p>', '7d1e48f98477eabe303d7be25aa3bbb7.jpeg', '50226433fc6daf786576ec2b7858bf14.jpeg', 'Text 6', 'Link 66', '', '', 2),
(10, 'Giving children the care they deserve', '<p class=\"MsoNormal\">Health problems can come up\r\nanytime and we are here to assist you at your child’s “can’t wait” situation. We operate under a simple, yet effective philosophy: Children\r\nare our future, and the preservation of their health is our duty.  </p>\r\n\r\n', 'bd255baacb3ffd13b14ab352532e22c0.jpg', '2319ec25be7728c0d2a0ab5f8448ec94.png', 'Discover more', 'locations', 'Second Text', 'Second Link', 1),
(11, 'Title 1', '<p>Content of title 1</p>', '70eeb1fd64d26607f522fe4e1cced9a3.jpg', '2a39b73c31626649edc63d733c35650b.jpg', 'Button 1', 'Link 6', 'Second Text', 'Second Link', 7);

-- --------------------------------------------------------

--
-- Table structure for table `comparison`
--

CREATE TABLE `comparison` (
  `id` int(11) NOT NULL,
  `section` varchar(200) NOT NULL,
  `category` varchar(200) NOT NULL,
  `optionOne` varchar(200) NOT NULL,
  `optionTwo` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comparison`
--

INSERT INTO `comparison` (`id`, `section`, `category`, `optionOne`, `optionTwo`) VALUES
(1, 'sectionOne', 'Category 1', 'Fewer Patients and disease types.', 'Waiting areas are full of germs.'),
(2, 'sectionTwo', 'Category 2', 'Fewer Patients and disease types 1', 'Waiting areas are full of germs 2');

-- --------------------------------------------------------

--
-- Table structure for table `description_box`
--

CREATE TABLE `description_box` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `image` varchar(200) NOT NULL,
  `button_text` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `style` varchar(10) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `description_box`
--

INSERT INTO `description_box` (`id`, `title`, `content`, `image`, `button_text`, `link`, `style`, `pageId`) VALUES
(21, 'sadas', '<p>axcasc</p>', '4545194116eeca4488d6e7e774f187d7.jpg', 'f', 'Link 1 ', 'Left', 2),
(23, ' Same Day Appointments ', '<p><!--[if gte mso 9]><xml>\r\n <o:OfficeDocumentSettings>\r\n  <o:AllowPNG/>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves/>\r\n  <w:TrackFormatting/>\r\n  <w:PunctuationKerning/>\r\n  <w:ValidateAgainstSchemas/>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF/>\r\n  <w:LidThemeOther>EN-US</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables/>\r\n   <w:SnapToGridInCell/>\r\n   <w:WrapTextWithPunct/>\r\n   <w:UseAsianBreakRules/>\r\n   <w:DontGrowAutofit/>\r\n   <w:SplitPgBreakAndParaMark/>\r\n   <w:EnableOpenTypeKerning/>\r\n   <w:DontFlipMirrorIndents/>\r\n   <w:OverrideTableStyleHps/>\r\n  </w:Compatibility>\r\n  <w:DoNotOptimizeForBrowser/>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"/>\r\n   <m:brkBin m:val=\"before\"/>\r\n   <m:brkBinSub m:val=\"&#45;-\"/>\r\n   <m:smallFrac m:val=\"off\"/>\r\n   <m:dispDef/>\r\n   <m:lMargin m:val=\"0\"/>\r\n   <m:rMargin m:val=\"0\"/>\r\n   <m:defJc m:val=\"centerGroup\"/>\r\n   <m:wrapIndent m:val=\"1440\"/>\r\n   <m:intLim m:val=\"subSup\"/>\r\n   <m:naryLim m:val=\"undOvr\"/>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"false\"\r\n  DefSemiHidden=\"false\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"376\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" QFormat=\"true\" Name=\"Normal\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 9\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 7\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 8\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 9\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 7\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 8\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 9\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal Indent\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footnote text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"header\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footer\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index heading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"caption\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"table of figures\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"envelope address\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"envelope return\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footnote reference\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation reference\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"line number\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"page number\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"endnote reference\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"endnote text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"table of authorities\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"macro\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"toa heading\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" QFormat=\"true\" Name=\"Title\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Closing\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Signature\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"Default Paragraph Font\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Message Header\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" QFormat=\"true\" Name=\"Subtitle\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Salutation\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Date\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text First Indent\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text First Indent 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Note Heading\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Block Text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Hyperlink\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"FollowedHyperlink\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" QFormat=\"true\" Name=\"Strong\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" QFormat=\"true\" Name=\"Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Document Map\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Plain Text\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"E-mail Signature\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Top of Form\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Bottom of Form\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal (Web)\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Acronym\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Address\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Cite\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Code\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Definition\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Keyboard\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Preformatted\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Sample\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Typewriter\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Variable\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal Table\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation subject\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"No List\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 7\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 8\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 4\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 5\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 7\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 8\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Contemporary\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Elegant\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Professional\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Subtle 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Subtle 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 2\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 3\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Balloon Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"Table Grid\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Theme\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Placeholder Text\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" QFormat=\"true\" Name=\"No Spacing\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Revision\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" QFormat=\"true\"\r\n   Name=\"List Paragraph\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" QFormat=\"true\" Name=\"Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" QFormat=\"true\"\r\n   Name=\"Intense Quote\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" QFormat=\"true\"\r\n   Name=\"Subtle Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" QFormat=\"true\"\r\n   Name=\"Intense Emphasis\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" QFormat=\"true\"\r\n   Name=\"Subtle Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" QFormat=\"true\"\r\n   Name=\"Intense Reference\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" QFormat=\"true\" Name=\"Book Title\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"Bibliography\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"TOC Heading\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"41\" Name=\"Plain Table 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"42\" Name=\"Plain Table 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"43\" Name=\"Plain Table 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"44\" Name=\"Plain Table 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"45\" Name=\"Plain Table 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"40\" Name=\"Grid Table Light\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\" Name=\"Grid Table 1 Light\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\" Name=\"Grid Table 6 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\" Name=\"Grid Table 7 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\" Name=\"List Table 1 Light\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\" Name=\"List Table 6 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\" Name=\"List Table 7 Colorful\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 1\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 2\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 3\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 4\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 5\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 6\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Mention\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Smart Hyperlink\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Hashtag\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Unresolved Mention\"/>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Smart Link\"/>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:\"Table Normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0in 5.4pt 0in 5.4pt;\r\n	mso-para-margin:0in;\r\n	mso-para-margin-bottom:.0001pt;\r\n	mso-pagination:widow-orphan;\r\n	font-size:12.0pt;\r\n	font-family:\"Calibri\",sans-serif;\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:\"Times New Roman\";\r\n	mso-bidi-theme-font:minor-bidi;}\r\n</style>\r\n<![endif]-->\r\n\r\n</p>\r\n\r\n<p class=\"MsoNormal\"><b>The best pediatric\r\nphysicians available </b></p>\r\n\r\n<p class=\"MsoNormal\" style=\"text-align:justify\">We provide quality primary\r\npediatric care to infants, children and adolescents from newborns to 21 years\r\nof age. All of our board-certified pediatricians are also certified through the\r\nNational Committee for Quality Assurance (NCQA). We believe in delivering the\r\nbest, most up to-date care for your children. For our children of the future we\r\ndeliver the best they deserve. </p>\r\n\r\n', '4f82629e9466a2ac0bec4d9fac0bc681.png', 'Our Doctors', 'doctors', 'Left', 1);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `qualification` varchar(200) NOT NULL,
  `certification` varchar(200) NOT NULL,
  `specialty` varchar(200) NOT NULL,
  `medicalSchool` varchar(200) NOT NULL,
  `hospitalAffiliation` varchar(200) NOT NULL,
  `residency_fellowship` varchar(200) NOT NULL,
  `biography` text NOT NULL,
  `image` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `qualification`, `certification`, `specialty`, `medicalSchool`, `hospitalAffiliation`, `residency_fellowship`, `biography`, `image`) VALUES
(24, 'Dr. Debra Barach', 'N/A', 'Board Certified', 'General Pediatrics and Child Development', 'UCLA School of Medicine', 'N/A', 'LA County-USC', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Barach was born and raised in Southern California until moving to Israel at the age of 14.&nbsp; She received her Bachelor of Science in Biology, Magna Cum Laude from The Hebrew University of Jerusalem.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Barach attended the UCLA School of Medicine and received her MD in 1985.&nbsp; She completed her pediatric residency training at LA County-USC in 1988, and worked in private practice until 2006, after which she took time to raise her twin daughters.&nbsp; She and her husband live in Sherman Oaks with their daughters, Sophia and Eliana.<br></p>', '1f7645f878ac6b26483e0a87a74de954.png'),
(25, 'Amy Wagner', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Wake Forest University School of Medicine', 'N/A', 'Children\'s Hospital of Orange County', '<div style=\"margin: 0px; padding: 0px; border: 0px;\">Dr.&nbsp;<span id=\"m_68670575145436630270.17085697843506265\" class=\"m_6867057514543663027highlight\" style=\"margin: 0px; padding: 0px; border: 0px;\">Wagner</span>&nbsp;is a board-certified pediatrician. She has been practicing for over seventeen years now. She earned her medical degree at Wake Forest University School of Medicine and received her pediatrics training at Children’s Hospital of Orange County.</div><div style=\"margin: 0px; padding: 0px; border: 0px;\"></div><div style=\"margin: 0px; padding: 0px; border: 0px;\">Dr.&nbsp;<span id=\"m_68670575145436630270.6949641937385154\" class=\"m_6867057514543663027highlight\" style=\"margin: 0px; padding: 0px; border: 0px;\">Wagner</span>&nbsp;loves spending time with her two children. She jogs regularly and keeps herself busy with home renovation projects. She is an avid poker player, who loves movies of all genres, and reading real books made out of paper.</div>', 'e08f96a04a871fcdafbba87909de4df6.png'),
(26, 'Janesri De Silva', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'American University of the Caribbean', 'Pediatric Department at Northridge Hospital Medical Center Valley Presbyterian Hospital Tarzana Medical Center Huntington Memorial Hospital', 'USC Yale Affiliated Hospitals, MD, IL Kaiser Permanente, Sunset Children\'s Hospital Los Angeles', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. De Silva graduated from UCLA with a Bachelor’s degree in Neuroscience and then went on to the American University of the Caribbean to complete her medical degree.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Listen to Dr. De Silva’s interview with KFWB talk radio, regarding medication dosage for children.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">She did part of her medical training at USC, Yale affiliated hospitals and other hospitals in Maryland and Chicago. She then completed a 3 year pediatric residency at Kaiser Permanente, Sunset. While at Kaiser, she wrote a manual for pediatric interns on how to take care of patients.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">After residency, she did a fellowship at Children’s Hospital Los Angeles in Adolescent Medicine and worked at the Los Angeles Free Clinic. At CHLA, Dr. De Silva worked on research involving transitioning chronically ill children to adult health care.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">She is the 2014 to 2016 chair of the Pediatric department at Northridge Hospital Medical Center in Northridge, and is on staff at Valley Presbyterian Hospital in Van Nuys, Tarzana Medical CenterTarzana and Huntington Memorial Hospital in Pasadena, CA.</p>', '77a6e967d8feb038f3405e06ccd853c8.png'),
(27, 'Jose Vargas', 'P.A.', 'Board Certified', 'General Pediatrics and Child Development', 'Charles Drew University of Medicine and Science.', 'N/A', 'N/A', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Jose Vargas is a Physician Assistant that joined Kids and Teens in&nbsp; January 2018. Jose comes to us with over 30 years of experience in Pediatric and Family Medicine. His past practices include the West Hollywood, Highland Park and Pacoima communities.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Jose enjoys learning and meeting new&nbsp;challenges.&nbsp; His priority is to provide high quality Pediatric care to&nbsp; young children and teens in the community.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Jose is&nbsp;married with two grown children. &nbsp;He is&nbsp;very involved with many neighborhood organizations&nbsp; that effect his community.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">In his&nbsp;free time Jose enjoys many outdoor activities such as camping, hiking and cycling. He&nbsp;enjoys keeping fit and attends the gym regularly. &nbsp;He likes to encourage&nbsp;his&nbsp;patients&nbsp;and their families to get out there and exercise and to&nbsp;promote a healthy lifestyle</p>', '4da14e91134e33fee817bac85fb34e83.png'),
(28, 'Dr. Gaya Selvakkumaran', 'MD FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'University of Zambia, School of Medicine', 'Methodist Hospital', 'White Memorial Medical Center', '<p>Dr Gaya (Gayathri Selvakkumaran MD FAAP), did her Medical education/training in Zambia where she treated many infectious diseases no longer prevalent in the first world countries. She then moved to UK for Neonatal fellowship training, but moved to California in the middle of it. She completed her Pediatric residency at White Memorial Medical Center, and since then been an esteemed Pediatrician working in the San Gabriel Valley area for the past 8 years. With her vast international experience in Pediatrics she provides excellent Pediatric care to her patients. In her free time she enjoys classical dancing, cooking and best of all spending time with her children; hence her maternal instincts makes her a lovable Pediatrician.<br></p>', 'b60b02be525f19ad91298524500e1f6e.png'),
(29, 'Kaberi Mozumder', 'PNP', 'Board Certified', 'General Pediatrics and Child Development', 'Yale School of Nursing', 'Stanford Children\'s Hospital, Children\'s Hospital of Los Angeles, and Cedars-Sinai Medical Center', 'N/A', '<p><span style=\"margin: 0px; padding: 0px; border: 0px;\">Kaberi has been a pediatric nurse practitioner for over 10 years graduating from Yale School of Nursing in 2008. She has extensive experience in both the inpatient and outpatient setting. She previously worked in specialty care for pediatric cardiology and cardio-thoracic&nbsp;surgery at a number of large academic centers including Stanford Children’s Hospital, Children’s Hospital of Los Angeles and Cedars-Sinai Medical Center before moving into the primary and urgent care setting. She is passionate about helping children and loves working with kids of all ages. Some of her special interests include adolescent medicine, mental health, asthma, and nutrition. She is a native Southern Californian and loves serving her home community. She is fluent in Spanish and spends her free time travelling, baking and spending time with her shih tzu Timmy.</span><br></p>', '84f1834395d7e2a25d79020f5d634e44.png'),
(30, 'Dr. Margaret Zdarzyl', 'MD FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Nicolaus Copernicus University in Poland', 'USC Verdugo Hills Hospital', 'N/A', '<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin: 0px; padding: 0px; border-spacing: 0px; border: 0px;\"><tbody style=\"margin: 0px; padding: 0px; border: 0px;\"><tr style=\"margin: 0px; padding: 0px; border: 0px;\"><td class=\"tdbottomborder\" colspan=\"3\" style=\"margin: 0px; padding: 0px; border: 0px;\">Margaret Zdarzyl, MD, is board certified in pediatrics. Dr. Zdarzyl earned her medical degree from Nicolaus Copernicus University in Poland. She completed her internship and residency at White Memorial Medical Center in Los Angeles. Dr. Zdarzyl is passionate about helping children and believes deeply in patient education. She loves to ski and garden. In addition to English, Dr. Zdarzyl speaks Polish and Spanish</td></tr></tbody></table>', 'b780527911c03b717f92f21a3eb44654.png'),
(31, 'Hilma Benjamin', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Penn State College of Medicine', 'Northridge Hospital Medical Center Providence Tarzana Medical Center Medical Director of After-Hours Pediatric Urgent Care', 'Children\'s Hospital, St. Petersburg, FL Tampa General Hospital, Tampa, FL', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Hilma Benjamin graduated from the University of the Virgin Islands with a Bachelor of Science in Biology and later earned a Master’s of Science in Molecular and Cellular Biology at the University of Maryland, Baltimore County.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">She received her medical degree in 2004 from Penn State College of Medicine and completed her residency in pediatrics at All Children’s Hospital in St. Petersburg, Florida, and Tampa General Hospital in Tampa, Florida.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Before joining Kids and Teens in 2009, she worked as locums tenens in private practice in the San Francisco Bay Area and at the Monterey County Health Department in outpatient and inpatient settings at the Natividad Medical Center, affiliated with UCSF.</p>', '9146ddf0b5392a45b8f414a44bad061d.png'),
(33, 'Victoria Millet', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Infectious Disease', 'USC', 'Providence Tarzana Medical Center Northridge Hospital and Medical Center', 'University Hospital, San Diego University of California Los Angeles', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Millet completed her undergraduate work as a Zoology Major at the University of California at Los Angeles in 1969. She earned a Master of Science Degree at the University of Southern California which was awarded in 1972. She completed a three year residency in pediatrics at University Hospital in San Diego in 1975.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">She completed a Pediatric Infectious Diseases/Ambulatory Pediatrics fellowship at the Center for the Health Sciences, University of California at Los Angeles in 1980.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Millet has been in private pediatric practice since 1984. She joined Kids and Teens Medical Group in January of 2011. She continues to do consulting work in pediatric infectious diseases for the Kids and Teens group, as well as for her colleagues in the local pediatric community. She also enjoys teaching and is a member of the faculty of the Family Practice Residency Program at Northridge Hospital.</p>', '1b66b70e3a6d888c33684d151c092c6a.png'),
(35, 'Priya Harder', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Drexel University College of Medicine', 'N/A', 'Dupont Hospital/TJUH, DE', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Harder was born and raised in La Canada and graduated cum laude from USC with a degree in English Literature and Language. She attended medical school at Drexel University College of Medicine&nbsp;<span style=\"border-style: initial; border-color: initial; border-image: initial;\">(formerly MCP Hahnemann)</span>. She completed her internship and residency at Dupont Hospital for Children/TJUH in Wilmington, Delaware.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Harder has worked in private practice in Pasadena since 2010 and presently sees newborn babies at Huntington Memorial Hospital. Dr. Harder lives in La Canada with her husband and three children, Arianna, Deven &amp; Gregory</p>', '5ebf70ecdf9a39cb66060fbbcc5011b0.png'),
(236, 'Barbara Rodriguez', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Creighton University Medical School', 'N/A', 'LAC-USC', '<p>She attended the University of Southern California &amp; received her Bachelor of Science degree in Biological Sciences. She then attended Creighton University Medical School &amp; received her MD in 1985. She completed her Pediatric Training at LAC-USC in 1988 &amp; has been involved in Pediatric Primary Care &amp; Pediatric Urgent Care since then. She has been an active member of Creighton’s Medical Dean’s Alumni Advisory Board since 1998. She is a Fellow of the American Academy of Pediatrics &amp; a member of the American Board of Pediatrics.<br></p>', '096497141fd2f1137f00556e76644f9d.png'),
(237, 'Martin Fineberg', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'The University of Melbourne, Royal Children’s Hospital', 'N/A', 'Emory University', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Martin Fineberg has been with Pediatric Affiliates Medical Group, Inc. since 1988.&nbsp; A native of Melbourne, Australia he studied medicine at The University of Melbourne.&nbsp; He&nbsp;then trained in pediatrics at the Royal Children’s Hospital in Melbourne&nbsp;followed by a pediatric residency at Emory University in Atlanta. Following&nbsp;this, he undertook additional training in neonatology at Children’s Hospital Los Angeles. In addition, Dr. Fineberg worked in the area of pediatric hematology/oncology at the Prince of Wales Hospital in Sydney, Australia. Dr. Fineberg is licensed to practice medicine in California and Australia, and was board certified by the American Board of Pediatrics in 1985.&nbsp;He was re-certified in 2009 and is current with Maintenance of Certification with the American Board of Pediatrics.&nbsp;&nbsp;He is a Fellow of the American Academy of Pediatrics, a member of the California Medical Association, the American Medical Association and an American Academy member of the Canadian Paediatric Society.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Fineberg was previously the director of the Neonatal Intensive Care Unit (NICU) at West Hills Hospital and Medical Center.&nbsp; He is now full time in general pediatrics with a special expertise and interest in the care of the high-risk NICU graduate and children with pulmonary diseases.&nbsp;&nbsp;&nbsp; He is also the Pediatric Affiliates vaccine expert</p>', '8fffe1825b26a5ec45215ab652282070.png'),
(238, 'Mark Snyder', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'Kansas University in Kansas City', 'N/A', 'Cedars Sinai Medical Center', '<p>Mark Snyder is a Woodland hills native and an alum of El Camino Real High School.&nbsp; He went to college at U. C. San Diego and majored in Biomedical Engineering, and he spent his junior year abroad studying at Hebrew University in Jerusalem.&nbsp; He then attended medical school at Kansas University in Kansas City, but returned to L.A. for his pediatric residency at Cedars Sinai Medical Center. He was board certified in pediatrics originally in 1992, and then recertified in 1998, 2006, and in 2015, and he is a fellow of the American Academy of Pediatrics.&nbsp; He loves to spend time with his family, and he also enjoys mountain biking, exercising at the gym, hiking with the family dog, following politics and world events, and reading about all of the sciences, with a special interest in astronomy and evolution.&nbsp; And of course, he loves and enjoys his work as a pediatrician at Pediatric Affiliates, working in the community where he grew up and currently lives.<br></p>', '6fc183fac2b17e08fdfa3aff3fc659e4.png'),
(239, 'Beth Melin-Perel', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'University of Arkansas Medical School', 'N/A', 'St. Christopher’s Hospital for Children in Philadelphia', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Beth Melin-Perel completed her undergraduate education at Washington University in St. Louis while on a National Merit scholarship. She graduated summa cum laude with a bachelor’s degree in biology and a minor in literature.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">She attended the University of Arkansas Medical School on a full scholarship, and went on to do her pediatric residency at St. Christopher’s Hospital for Children in Philadelphia. She was on the teaching staff at Yale University in New Haven, CT and active in private practice. She is Board Certified by the American Board of Pediatrics, and is a member of the American Academy of Pediatrics and the Los Angeles Pediatric Society.&nbsp; She and her wonderful husband, Dr. Issie Perel, raised three sons who are now in graduate programs. Dr.Beth volunteers at the Westminster Free Clinic in Thousand Oaks, and enjoys gardening, movies, and healthy cooking in her leisure time.</p>', '5453b2bcfe99b2a2aad663a95e9ab4fd.png'),
(240, 'Marianne Woods', 'MD, MSPH', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Jagiellonian University Medical College, Krakow, Poland', 'N/A', 'Kaiser Permanente, Los Angeles', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Woods received her MD degree from the Jagiellonian University Medical College in Krakow, Poland in 1984 and an MSPH degree from the UCLA School of Public Health in 1979. She completed her residency program in Pediatrics at Kaiser Permanente Medical Center in Los Angeles, CA in 1993 and worked as a pediatrician in the outpatient clinics of the Kaiser Permanente system for over 20 years.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">At the Kids and Teens clinics, she continues to provide high quality acute and preventative care with a special interest in nutritional guidance, weight control and anticipatory guidance</p>', '457034178d3b427474ec72a6275af22c.png'),
(241, 'Peter Jackson', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'U.C., Irvine', 'N/A', 'Cedars Sinai Medical Center, Beverly Hills', '<p>Dr. Peter Jackson has practiced pediatrics for nearly 31 years, all in the same location in Pasadena at the Children’s Clinic now Kids &amp; Teens Medical Group. Dr. Jackson is a Southern California native who went to medical school at U.C. Irvine and had an internship and residency at Cedar Sinai Medical Center in Beverly Hills.<br></p>', '48fd4f9c680db85e26e6edb92b2fbfac.png'),
(242, 'Padma Bala', 'MD, FAAP', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'Madras Medical College, Madras, India', 'N/A', 'Rushy Presbyterian Medical Center, IL', '<p>Dr. Padma Bala, graduated from the prestigious Madras Medical College at Madras, India. She completed her Residency in Pediatrics at Rush Presbyterian Medical Center in Chicago, after which she served in US Army Medical Corps, as a Pediatrician with the Rank of Lt Colonel. She then joined Baylor Medical Group in Dallas while consistently achieving “Best Pediatrician Award” from the local Communities for many years. She moved to Los Angeles to be close to her children and grandchildren<br></p>', '9c5e5f53be3dfdeb0b9b3f69805fe75f.png'),
(243, 'Lourdes Mosqueda', 'MD', 'Board Certified', 'Pediatrics and Adolescent Medicine', 'University of Illinois College of Medicine, Rockford', 'N/A', 'California Pacific Medical Center, San Francisco', '<p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Mosqueda attended the University of California, Irvine where she received her Bachelors degree in Biological Sciences.  She attended the University of Illinois College of Medicine, Rockford where she received her medical degree.  Dr. Mosqueda completed her Pediatric Residency training at California Pacific Medical Center in San Francisco, California.</p><p style=\"margin-right: 0px; margin-bottom: 20px; margin-left: 0px; padding: 0px; border: 0px;\">Dr. Mosqueda is a Pediatrician who has practiced medicine in San Francisco since 1997.  In 2010, Dr. Mosqueda, her husband and 3 children moved to South Pasadena where she continues her commitment to children’s health.  She sees children from birth until their 18th birthday.  She looks forward to meeting you and your family.</p>', '6fe8680437284ac9e602d0dcce1e85e8.png'),
(244, 'Orlando Ayala', ' PA-C', 'Board Certified', 'Physician Assistant', 'Physician’s Assistant at the University of Southern California', 'N/A', 'N/A', '<p>Orlando Ayala grew up in East Los Angeles and completed his training as physician’s assistant at the University of Southern California.&nbsp;He worked at the Clinica Medica San Miguel for 8 years prior to starting full time at Kids and Teens Medical Group.&nbsp;Mr Ayala works at the Van Nuys and Pasadena locations of Kids and Teens.&nbsp;He lives in Eagle Rock with his wife and daughter, Isabella.<br></p>', 'ee14bf05e62333b39d29b6607b3b0d78.png'),
(245, 'Grace Dasovich', 'MD, FAAP', 'Board Certified', 'General Pediatrics and Child Development', 'University of Minnesota Medical School', 'N/A', 'University of California Irvine / Children\'s Hospital of Orange County', '<p>Dr. Grace Dasovich is a board certified Pediatrician who is excited to start a career at Kids &amp; Teens Medical Group. She attended Northwestern University and obtained a Bachelor of Science degree in Biomedical Engineering. She received her medical degree from the University of Minnesota Medical School and completed her pediatrics residency at the University of California Irvine / Children’s Hospital of Orange County program. Outside of medicine, she enjoys spending time with family, traveling, and running marathons<br></p>', '2b90d116072838d8ce55a1442fc8386f.png'),
(248, 'Cze-Ja Tam', 'CPNP', 'Board Certified', 'Pediatric Nurse Practitioner', '-', '-', '-', '<p>-</p>', '48443bb2a02337f5d126e7df53b8ee83.png');

-- --------------------------------------------------------

--
-- Table structure for table `feature_box`
--

CREATE TABLE `feature_box` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `button_text` varchar(200) NOT NULL,
  `link` varchar(200) NOT NULL,
  `image` varchar(300) NOT NULL,
  `pageId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feature_box`
--

INSERT INTO `feature_box` (`id`, `title`, `content`, `button_text`, `link`, `image`, `pageId`) VALUES
(11, 'Title 3', 'Content of title 3.', 'Text 3', 'Link 3', '88a2bcba53c9c36761beabf107c5ff76.jpeg', 2),
(15, 'We accept over 50 insurances ', '<p>We accept most insurance including HMOs, PPOs, and Medi-Cal. If you belong to another HMO/IPA or have another office listed; you can still come into our office and have it all changed to one of our office and physicians. Or contact us on (818) 361-5437 and have it changed over the phone.<br></p>', 'Check Eligibility', 'insurance', '813eafd9cc5b6c6eaaf4a148f28d891b.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` text NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `fax` varchar(20) NOT NULL,
  `remarks` text NOT NULL,
  `latitude` varchar(20) NOT NULL,
  `longitude` varchar(20) NOT NULL,
  `afterHour` varchar(10) DEFAULT NULL,
  `saturday` varchar(10) DEFAULT NULL,
  `sunday` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `address`, `telephone`, `fax`, `remarks`, `latitude`, `longitude`, `afterHour`, `saturday`, `sunday`) VALUES
(3, 'Agoura Hills', '29525 Canwood Street,\r\n#111 Agoura Hills, CA 91301', '818-706-8133', '818-659-3895', '', '34.148164', '-118.7694706', NULL, NULL, NULL),
(5, 'Arcadia', '75 North Santa Anita Ave, Suite 206\r\nArcadia, CA 91006', '626-795-8811', '626-231-0501', 'Now Open Monday-Friday!', '34.1409735', '-118.0339027', '', '', ''),
(10, 'Beverly Hills', '240 S. La Cienega Boulevard, Suite 350\r\nBeverly Hills, CA 90211', '323-664-1977', '424-512-1700', 'Free Parking at Gregory & La Cienega!', '34.0639201', '-118.3780179', '', '', ''),
(11, 'Encino', '17609 Ventura Blvd, Ste. 106\r\nEncino, CA 91316', '818-361-5437', '818-658-9363', '', '34.1622902', '-118.5187972', '', '', ''),
(12, 'La Canada', '1442 Foothill Blvd,\r\nLa Canada Flintridge, CA 91011', '818-361-5437', '626-406-1801', 'Now Open Monday-Friday!!', '34.2085283', '-118.2121095', '', '', ''),
(18, 'Northridge', '8940 Reseda Blvd, Suite 101\r\nNorthridge, CA 91324', '818-361-5437', '818-659-3879', '', '34.2330844', '-118.5378921', 'on', 'on', 'on'),
(19, 'Pasadena', '504 S Sierra Madre Blvd,\r\nPasadena, CA 91107', ' 818-361-5437', ' 818-361-5437', '', '34.1376025', '-118.1033559', 'on', 'on', 'on'),
(21, 'San Fernando', '777 Truman Street, Suite 105\r\nSan Fernando, CA 91340', '818-361-5437', '818-639-9827', '', '34.2814428', '-118.4408147', '', '', ''),
(22, 'Van Nuys', '14426 Gilmore Street,\r\nSuite B Van Nuys, CA 91401', '818-361-5437', '818-646-9568', 'Open until 7 pm on Monday, Tuesday and Fridays and Saturday until 5 pm!', '34.1874945', '-118.44969', 'on', 'on', NULL),
(24, 'West Hills', '7345 Medical Center Drive, #400 West Hills, CA 91307', '818-883-0460', '818-538-8804', 'Now Open Until 8 pm on Monday and Wednesday Evenings!', '34.2031499', '-118.6337355', 'on', 'on', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `location_has_doctors`
--

CREATE TABLE `location_has_doctors` (
  `id` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `doctorId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `location_has_doctors`
--

INSERT INTO `location_has_doctors` (`id`, `locationId`, `doctorId`) VALUES
(70, 11, 34),
(71, 18, 34),
(72, 22, 34),
(73, 23, 34),
(177, 18, 246),
(178, 22, 246),
(179, 23, 246),
(200, 18, 248),
(201, 22, 248),
(202, 11, 24),
(203, 18, 25),
(204, 5, 26),
(205, 10, 26),
(206, 12, 26),
(207, 19, 26),
(208, 21, 27),
(209, 5, 28),
(210, 19, 28),
(211, 18, 29),
(212, 19, 29),
(213, 21, 29),
(214, 22, 29),
(215, 12, 30),
(216, 18, 31),
(217, 11, 33),
(218, 18, 33),
(219, 22, 33),
(223, 5, 35),
(224, 12, 35),
(225, 19, 35),
(226, 5, 236),
(227, 12, 236),
(228, 19, 236),
(229, 21, 236),
(230, 3, 237),
(231, 10, 237),
(232, 11, 237),
(233, 19, 237),
(234, 3, 238),
(239, 18, 240),
(240, 19, 240),
(241, 22, 240),
(242, 3, 239),
(243, 5, 241),
(244, 12, 241),
(245, 19, 241),
(246, 21, 242),
(250, 5, 243),
(251, 19, 243),
(252, 21, 243),
(253, 19, 244),
(254, 22, 244),
(255, 10, 245),
(256, 12, 245);

-- --------------------------------------------------------

--
-- Table structure for table `location_schedule`
--

CREATE TABLE `location_schedule` (
  `id` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `status` varchar(10) NOT NULL,
  `fromHour` time NOT NULL,
  `toHour` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location_schedule`
--

INSERT INTO `location_schedule` (`id`, `locationId`, `day`, `status`, `fromHour`, `toHour`) VALUES
(5, 3, 'Monday', 'Open', '08:30:00', '17:30:00'),
(6, 3, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(7, 3, 'Wednesday', 'Close', '01:00:00', '03:00:00'),
(8, 3, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(9, 3, 'Friday', 'Open', '09:00:00', '18:00:00'),
(10, 3, 'Saturday', 'Open', '14:00:00', '22:00:00'),
(11, 3, 'Sunday', 'Close', '04:00:00', '10:00:00'),
(12, 4, 'Monday', 'Open', '08:30:00', '17:30:00'),
(13, 4, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(14, 5, 'Monday', 'Open', '08:30:00', '17:30:00'),
(15, 5, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(16, 5, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(17, 5, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(18, 5, 'Friday', 'Open', '09:00:00', '18:00:00'),
(19, 5, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(20, 5, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(21, 6, 'Monday', 'Open', '08:30:00', '17:30:00'),
(22, 6, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(23, 6, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(24, 6, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(25, 6, 'Friday', 'Open', '09:00:00', '18:00:00'),
(26, 6, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(27, 6, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(28, 7, 'Monday', 'Open', '08:30:00', '17:30:00'),
(29, 7, 'Tuesday', 'Open', '08:30:00', '17:30:00'),
(30, 7, 'Wednesday', 'Open', '14:00:00', '18:00:00'),
(31, 7, 'Thursday', 'Open', '08:30:00', '17:30:00'),
(32, 7, 'Friday', 'Open', '09:00:00', '18:00:00'),
(33, 7, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(34, 7, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(35, 10, 'Monday', 'Open', '09:00:00', '18:00:00'),
(36, 10, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(37, 10, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(38, 10, 'Thursday', 'Open', '10:00:00', '17:00:00'),
(39, 10, 'Friday', 'Open', '09:00:00', '18:00:00'),
(40, 10, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(41, 10, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(42, 11, 'Monday', 'Open', '09:00:00', '18:00:00'),
(43, 11, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(44, 11, 'Wednesday', 'Close', '00:00:00', '00:00:00'),
(45, 11, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(46, 11, 'Friday', 'Open', '09:00:00', '18:00:00'),
(47, 11, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(48, 11, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(49, 12, 'Monday', 'Open', '09:00:00', '18:00:00'),
(50, 12, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(51, 12, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(52, 12, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(53, 12, 'Friday', 'Open', '09:00:00', '18:00:00'),
(54, 12, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(55, 12, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(91, 18, 'Monday', 'Open', '09:00:00', '20:00:00'),
(92, 18, 'Tuesday', 'Open', '09:00:00', '20:00:00'),
(93, 18, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(94, 18, 'Thursday', 'Open', '09:00:00', '20:00:00'),
(95, 18, 'Friday', 'Open', '09:00:00', '20:00:00'),
(96, 18, 'Saturday', 'Open', '12:00:00', '20:00:00'),
(97, 18, 'Sunday', 'Open', '12:00:00', '20:00:00'),
(98, 19, 'Monday', 'Open', '09:00:00', '20:00:00'),
(99, 19, 'Tuesday', 'Open', '09:00:00', '20:00:00'),
(100, 19, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(101, 19, 'Thursday', 'Open', '09:00:00', '20:00:00'),
(102, 19, 'Friday', 'Open', '09:00:00', '20:00:00'),
(103, 19, 'Saturday', 'Open', '12:00:00', '20:00:00'),
(104, 19, 'Sunday', 'Open', '12:00:00', '20:00:00'),
(112, 21, 'Monday', 'Open', '09:00:00', '18:00:00'),
(113, 21, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(114, 21, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(115, 21, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(116, 21, 'Friday', 'Open', '09:00:00', '18:00:00'),
(117, 21, 'Saturday', 'Close', '00:00:00', '00:00:00'),
(118, 21, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(119, 22, 'Monday', 'Open', '09:00:00', '19:00:00'),
(120, 22, 'Tuesday', 'Open', '09:00:00', '19:00:00'),
(121, 22, 'Wednesday', 'Open', '09:00:00', '18:00:00'),
(122, 22, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(123, 22, 'Friday', 'Open', '09:00:00', '19:00:00'),
(124, 22, 'Saturday', 'Open', '09:00:00', '18:00:00'),
(125, 22, 'Sunday', 'Close', '00:00:00', '00:00:00'),
(133, 24, 'Monday', 'Open', '09:00:00', '20:00:00'),
(134, 24, 'Tuesday', 'Open', '09:00:00', '18:00:00'),
(135, 24, 'Wednesday', 'Open', '09:00:00', '20:00:00'),
(136, 24, 'Thursday', 'Open', '09:00:00', '18:00:00'),
(137, 24, 'Friday', 'Open', '09:00:00', '18:00:00'),
(138, 24, 'Saturday', 'Open', '09:00:00', '18:00:00'),
(139, 24, 'Sunday', 'Close', '00:00:00', '00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `name`) VALUES
(1, 'Home'),
(6, 'Locations'),
(7, 'Doctors'),
(8, 'Insurance');

-- --------------------------------------------------------

--
-- Table structure for table `pointers`
--

CREATE TABLE `pointers` (
  `id` int(11) NOT NULL,
  `point` varchar(300) NOT NULL,
  `section` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pointers`
--

INSERT INTO `pointers` (`id`, `point`, `section`) VALUES
(17, 'Chest pain', 'Emergency Room'),
(18, 'Difficulty breathing or wheezing', 'Emergency Room'),
(19, 'Arm/leg weakness', 'Emergency Room'),
(20, 'Loss of consciousness or lethargy', 'Emergency Room'),
(21, 'Any sudden, severe pain', 'Emergency Room'),
(22, 'Falls with injury', 'Emergency Room'),
(23, 'Head or eye injuries and/or loss of vision', 'Emergency Room'),
(24, 'Deep cuts/large open wounds', 'Emergency Room'),
(25, 'Newborn baby less than three months old with a fever of 100.4 degrees or higher', 'Emergency Room'),
(26, 'Fever with a rash', 'Emergency Room'),
(27, 'Persistent bleeding', 'Emergency Room'),
(28, 'Repeated vomiting and/or diarrhea with concern of dehydration', 'Emergency Room'),
(29, 'Burns', 'Emergency Room'),
(30, 'Seizures without a diagnosis or epilepsy', 'Emergency Room'),
(31, 'Urgent Care is an option for care needed right away that you would normally go to your doctor, but cannot get in to see your doctor', 'Urgent Care'),
(32, 'Minor trauma such as a common sprain or shallow cut', 'Urgent Care'),
(33, 'Fever without rash', 'Urgent Care'),
(34, 'Newborn baby less than three months old with a temperature up to 100.4 degrees', 'Urgent Care'),
(35, 'Painful urination', 'Urgent Care'),
(36, 'Vomiting', 'Urgent Care'),
(37, 'Diarrhea', 'Urgent Care'),
(38, 'Sore throat', 'Urgent Care'),
(39, 'Ear Pain', 'Urgent Care'),
(40, 'Cough', 'Urgent Care'),
(41, 'You already know the diagnosis, but cannot get an appointment with a primary care physician', 'Urgent Care');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accordion`
--
ALTER TABLE `accordion`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comparison`
--
ALTER TABLE `comparison`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `description_box`
--
ALTER TABLE `description_box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feature_box`
--
ALTER TABLE `feature_box`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_has_doctors`
--
ALTER TABLE `location_has_doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location_schedule`
--
ALTER TABLE `location_schedule`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pointers`
--
ALTER TABLE `pointers`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accordion`
--
ALTER TABLE `accordion`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `comparison`
--
ALTER TABLE `comparison`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `description_box`
--
ALTER TABLE `description_box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=249;

--
-- AUTO_INCREMENT for table `feature_box`
--
ALTER TABLE `feature_box`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `location_has_doctors`
--
ALTER TABLE `location_has_doctors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;

--
-- AUTO_INCREMENT for table `location_schedule`
--
ALTER TABLE `location_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `pointers`
--
ALTER TABLE `pointers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
